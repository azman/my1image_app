/*----------------------------------------------------------------------------*/
#ifndef __MY1IVEC_FILE_H__
#define __MY1IVEC_FILE_H__
/*----------------------------------------------------------------------------*/
#include "ivec.h"
/*----------------------------------------------------------------------------*/
/* file access for my1ivec_t */
int ivec_load(my1ivec_t* ivec, char* name);
int ivec_save(my1ivec_t* ivec, char* name);
int ivec_show(my1ivec_t* ivec);
/*----------------------------------------------------------------------------*/
#ifndef __MY1_HEADER_ONLY__
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int flush2_next_line(FILE* pchk) {
	int done = 0;
	while (!feof(pchk)) {
		if (fgetc(pchk)=='\n') {
			done = 1;
			break;
		}
	}
	return done;
}
/*----------------------------------------------------------------------------*/
int count4_cols(FILE* pchk) {
	int buff, cols = 0, init = 0;
	while (1) {
		buff = fgetc(pchk);
		if (buff=='\n'||buff==EOF) {
			if (init) cols++;
			break;
		}
		else if (buff!=' '&&buff!='\t') {
			if (!init) init = 1;
		}
		else { /* whitespace */
			if (init) {
				cols++;
				init = 0;
			}
		}
	}
	return cols;
}
/*----------------------------------------------------------------------------*/
int ivec_load(my1ivec_t* ivec, char* name) {
	FILE* pfile;
	long tell;
	int cols, rows, size, loop;
	int test, data, done = 0;
	pfile = fopen(name,"rt");
	if (pfile) {
		do {
			/* skip first line */
			if (!flush2_next_line(pfile)) break;
			/* save position */
			tell = ftell(pfile);
			/* count data in first list - cols! */
			cols = count4_cols(pfile);
			if (!cols) break;
			rows = 1;
			/* count data line - rows! */
			while ((test=count4_cols(pfile))) {
				if (!test) break;
				rows++;
			}
			if (!rows) break;
			/* get data start position */
			fseek(pfile,tell,SEEK_SET);
			/* prep data */
			size = ivec_make(ivec,rows,cols);
			if (!size) break;
			/* read in data */
			for (loop=0;loop<size;loop++) {
				test = fscanf(pfile,"%d",&data);
				if (test!=1) break;
				ivec->data[loop] = data;
			}
			if (loop==size) done = 1;
		} while(0);
		fclose(pfile);
	}
	return done;
}
/*----------------------------------------------------------------------------*/
int ivec_save(my1ivec_t* ivec, char* name) {
	FILE* pfile;
	int irow, icol, loop = 0;
	pfile = fopen(name,"wt");
	if (pfile) {
		for (irow=0;irow<ivec->rows;irow++) {
			for (icol=0;icol<ivec->cols;icol++,loop++)
				fprintf (pfile,"%d ",ivec->data[loop]);
			fprintf (pfile,"\n");
		}
		fclose(pfile);
	}
	return loop; /* non-zero on success */
}
/*----------------------------------------------------------------------------*/
int ivec_show(my1ivec_t* ivec) {
	int irow, icol, loop;
	for (irow=0,loop=0;irow<ivec->rows;irow++) {
		printf ("|");
		for (icol=0;icol<ivec->cols;icol++,loop++)
			printf (" %d",ivec->data[loop]);
		printf (" |\n");
	}
	return loop;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1_HEADER_ONLY__ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1IVEC_FILE_H__ */
/*----------------------------------------------------------------------------*/
