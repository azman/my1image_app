/*----------------------------------------------------------------------------*/
#ifndef __MY1POINTS_H__
#define __MY1POINTS_H__
/*----------------------------------------------------------------------------*/
#include "point.h"
/*----------------------------------------------------------------------------*/
#define CCLIST_MIN_POINTS CONTOUR_MIN_POINTS
/*----------------------------------------------------------------------------*/
#define POINTS_SAMPLE_INIT 16
/*----------------------------------------------------------------------------*/
typedef struct _my1points_t {
	my1point_t *loop;
	int size, fill;
	int flag, pidx;
	int xmin, xmax;
	int ymin, ymax;
	struct _my1points_t *next; /* linked list */
	my1point_t orig; /* origin point */
} my1points_t;
/*----------------------------------------------------------------------------*/
void points_init(my1points_t* points) {
	points->loop = 0x0;
	points->size = 0;
	points->fill = 0;
	points->flag = 0; /* not used at the moment? */
	points->pidx = 0; /* used in points_scan_contour */
	points->xmin = -1;
	points->xmax = -1;
	points->ymin = -1;
	points->ymax = -1;
	points->next = 0x0;
	point_make(&points->orig,0,0);
}
/*----------------------------------------------------------------------------*/
void points_free(my1points_t* points) {
	if (points->loop) {
		free((void*)points->loop);
		points->loop = 0x0; /* just in case, avoid double-free */
		points->size = 0;
	}
}
/*----------------------------------------------------------------------------*/
void points_zero(my1points_t* points) {
	points->fill = 0;
	points->flag = 0;
	points->pidx = 0;
	points->xmin = -1;
	points->xmax = -1;
	points->ymin = -1;
	points->ymax = -1;
	points->next = 0x0;
	point_make(&points->orig,0,0);
}
/*----------------------------------------------------------------------------*/
my1point_t* points_make(my1points_t* points, int size) {
	my1point_t* temp;
	temp = (my1point_t*) realloc((void*)points->loop,
		(size)*sizeof(my1point_t));
	if (temp) {
		points->loop = temp;
		points->size = size;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
my1point_t* points_append(my1points_t* points, int x, int y) {
	my1point_t* curr = 0x0;
	if (points->fill<points->size) {
		if (!points->fill) {
			points->xmin = x;
			points->xmax = x;
			points->ymin = y;
			points->ymax = y;
		}
		else {
			if (x<points->xmin) points->xmin = x;
			else if (x>points->xmax) points->xmax = x;
			if (y<points->ymin) points->ymin = y;
			else if (y>points->ymax) points->ymax = y;
		}
		curr = &points->loop[points->fill++];
		point_make(curr,x,y);
	}
	return curr;
}
/*----------------------------------------------------------------------------*/
int points_clone(my1points_t* points, my1points_t* from) {
	int loop = 0;
	if (points_make(points,from->size)) {
		while (loop<from->fill) {
			point_copy(&points->loop[loop],&from->loop[loop]);
			loop++;
		}
		points->fill = loop;
		points->flag = from->flag;
		points->pidx = from->pidx; /* keep this info? */
		points->xmin = from->xmin;
		points->xmax = from->xmax;
		points->ymin = from->ymin;
		points->ymax = from->ymax;
		points->next = 0x0; /* no longer in list! */
		point_copy(&points->orig,&from->orig);
	}
	return loop;
}
/*----------------------------------------------------------------------------*/
int points_trace(my1points_t* points, my1ivec_t* ivec, int mark) {
	int test,loop,step;
	my1point_t* temp = points->loop;
	for (loop=0,step=0;loop<points->fill;loop++,temp++) {
		test = point_test_that(ivec,temp->x,temp->y);
		if (test<0) {
#ifdef MY1DEBUG_POINTS
			printf("** Invalid point! [%d](%d,%d)\n",loop,temp->x,temp->y);
#endif
			continue;
		}
		ivec->data[test] = mark;
		step++;
	}
	return step;
}
/*----------------------------------------------------------------------------*/
void points_sample_down(my1points_t* points, my1points_t* sample, int size) {
	int loop, test;
	float step;
	my1point_t *curr;
	points_make(sample,size); /* size < points->fill! */
	points_zero(sample);
	test = points->fill;
	step = (float)test/size;
#ifdef MY1DEBUG_POINTS
	printf("-- Curr:%d, Size:%d, Step:%d\n",test,size,(int)step);
#endif
	curr = points->loop;
	for (loop=0;loop<test;loop++,curr++) {
		if ((int)((float)loop/step)==sample->fill) {
#ifdef MY1DEBUG_POINTS
			printf("## Sample[%d]<-[%d]\n",sample->fill,loop);
#endif
			points_append(sample,curr->x,curr->y);
			if (sample->fill==sample->size) break;
		}
#ifdef MY1DEBUG_POINTS
		else printf("   Skipped[%d:%d]\n",loop,(int)(step*loop));
#endif
	}
#ifdef MY1DEBUG_POINTS
	printf("-- Loop:%d, Fill:%d\n",loop,sample->fill);
#endif
}
/*----------------------------------------------------------------------------*/
void points_sample_up(my1points_t* points, my1points_t* sample, int size) {
	int loop, test;
	float step;
	my1point_t *curr, *prev;
	points_make(sample,size); /* size > points->fill! */
	points_zero(sample);
	test = points->fill;
	step = (float)size/test;
#ifdef MY1DEBUG_POINTS
	printf("-- Curr:%d, Size:%d, Step:%d\n",test,size,(int)step);
#endif
	loop = 0;
	prev = curr = points->loop;
	while (sample->fill<sample->size) {
		if ((int)((float)sample->fill/step)!=loop) {
#ifdef MY1DEBUG_POINTS
			printf("   Repeat[%d]<-[%d]\n",sample->fill,loop-1);
#endif
			points_append(sample,prev->x,prev->y);
		}
		else {
#ifdef MY1DEBUG_POINTS
			printf("## Sample[%d]<-[%d]\n",sample->fill,loop);
#endif
			points_append(sample,curr->x,curr->y);
			if (loop<test) {
				loop++;
				prev = curr;
				curr++;
			}
#ifdef MY1DEBUG_POINTS
			else printf("** Overflow?[%d]<-[%d]\n",sample->fill,loop);
#endif
		}
	}
#ifdef MY1DEBUG_POINTS
	printf("-- Loop:%d, Fill:%d\n",loop,sample->fill);
#endif
}
/*----------------------------------------------------------------------------*/
void points_sample(my1points_t* points, my1points_t* sample, int size) {
	int loop, test;
	float step;
	my1point_t *curr;
	/* check if auto-size requested? */
	test = points->fill;
	if (size<=0) {
		size = POINTS_SAMPLE_INIT;
		while (size<test) size <<= 1; /* get next power-of-2 */
		size >>= 1; /* down-sample! */
	}
	/* size should be < points->fill! */
	points_make(sample,size);
	points_zero(sample);
	step = (float)test/size;
#ifdef MY1DEBUG_POINTS
	printf("-- Curr:%d, Size:%d, Step:%d\n",test,size,(int)step);
#endif
	curr = points->loop;
	for (loop=0;loop<test;loop++,curr++) {
		if ((int)((float)loop/step)==sample->fill) {
#ifdef MY1DEBUG_POINTS
			printf("## Sample[%d]<-[%d]\n",sample->fill,loop);
#endif
			points_append(sample,curr->x,curr->y);
			if (sample->fill==sample->size) break;
		}
#ifdef MY1DEBUG_POINTS
		else printf("   Skipped[%d:%d]\n",loop,(int)(step*loop));
#endif
	}
#ifdef MY1DEBUG_POINTS
	printf("-- Loop:%d, Fill:%d\n",loop,sample->fill);
#endif
}
/*----------------------------------------------------------------------------*/
typedef struct _my1moment_t {
	unsigned long m10, m01, m00;
} my1moment_t;
/*----------------------------------------------------------------------------*/
void points_moment(my1points_t* points) {
	my1point_t* temp;
	my1moment_t momt;
	int loop;
	temp = points->loop;
	momt.m10 = 0;
	momt.m01 = 0;
	momt.m00 = 0;
	for (loop=0;loop<points->size;loop++,temp++) {
		momt.m10 += temp->x;
		momt.m01 += temp->y;
		momt.m00++;
	}
	/* get center of gravity */
	points->orig.x = (int)((double)momt.m10 / momt.m00);
	points->orig.y = (int)((double)momt.m01 / momt.m00);
}
/*----------------------------------------------------------------------------*/
typedef struct _my1cclist_t {
	my1points_t *init, *last, *lmax;
	my1point_find_t find;
	int size, pmin, rsvd, draw;
	my1ivec_t buff;
} my1cclist_t;
/*----------------------------------------------------------------------------*/
#define CCLIST_DRAW_ENB 0x03
#define CCLIST_DRAW_ALL 0x01
#define CCLIST_DRAW_MAX 0x02
/*----------------------------------------------------------------------------*/
void cclist_init(my1cclist_t* cclist) {
	cclist->init = 0x0;
	cclist->last = 0x0;
	cclist->lmax = 0x0; /* pointer to most points  */
	cclist->size = 0;
	cclist->pmin = CCLIST_MIN_POINTS; /* minimum points to become a member */
	cclist->rsvd = 0; /* not used - reserved */
	cclist->draw = CCLIST_DRAW_ALL; /* option to draw points */
	ivec_init(&cclist->buff);
}
/*----------------------------------------------------------------------------*/
void cclist_redo(my1cclist_t* cclist) {
	my1points_t* temp;
	while (cclist->init) {
		temp = cclist->init->next;
		points_free(cclist->init);
		cclist->init = temp;
	}
	cclist->last = 0x0;
	cclist->lmax = 0x0;
	cclist->size = 0;
}
/*----------------------------------------------------------------------------*/
void cclist_free(my1cclist_t* cclist) {
	ivec_free(&cclist->buff);
	cclist_redo(cclist);
}
/*----------------------------------------------------------------------------*/
my1points_t* cclist_create(my1cclist_t* cclist) {
	my1points_t* list = (my1points_t*) malloc(sizeof(my1points_t));
	if (!list) return 0x0;
	points_init(list);
	if (!cclist->init) cclist->init = list;
	else cclist->last->next = list;
	cclist->last = list;
	cclist->size++;
	return list;
}
/*----------------------------------------------------------------------------*/
void points_check(my1ivec_t* ivec, void* data,int y,int x,int step,int size) {
	my1points_t* list;
	my1cclist_t* cclist;
	my1point_t *curr, temp;
	int loop, ichk, bdir;
	cclist = (my1cclist_t*) data;
	if (size<cclist->pmin) return;
#ifdef MY1DEBUG_POINTS
	/* if all is well, should NOT need this! */
	if ((ichk=point_test_that(ivec,x,y))<0||ivec->data[ichk]<0) {
		printf("** LOOP0[%d:%d]:(%d,%d){%dx%d}[%d/%d:%d]\n",
			step,size,x,y,ivec->cols,ivec->rows,
			ichk,ivec->size,ivec->data[ichk]);
		return;
	}
#endif
	list = cclist_create(cclist);
	if (!list) return;
	curr = points_make(list,size);
	if (!curr) return;
	points_append(list,x,y); /* fill curr */
	for (loop=1;loop<size;loop++) {
		ichk = curr->x + (curr->y * ivec->cols);
#ifdef MY1DEBUG_POINTS
		if (!point_test_comp(ivec,curr->x,curr->y)||ivec->data[ichk]<0) {
			printf("** LOOPX[%d:%d:%d]:(%d,%d)[%d@%d:%d]\n",
				step,loop,size,curr->x,curr->y,ichk,
				point_test_that(ivec,curr->x,curr->y),
				ivec->data[ichk]);
			break;
		}
#endif
		bdir = DIR_ODIR(ivec->data[ichk]);
#ifdef MY1DEBUG_POINTS
		if (bdir<0||bdir>DIR_CHK) {
			printf("** LOOPY[%d:%d:%d]:(%d,%d)[%d]\n",
				step,loop,size,curr->x,curr->y,bdir);
			break;
		}
#endif
		point_next(curr,&temp,bdir);
		curr = points_append(list,temp.x,temp.y);
	}
	list->pidx = step;
	if (!cclist->lmax||cclist->lmax->fill<list->fill)
		cclist->lmax = list;
}
/*----------------------------------------------------------------------------*/
void points_scan_contour(my1ivec_t *ivec, my1ivec_t *dest, my1cclist_t* pccs) {
	cclist_redo(pccs); /* must be in reset state... */
	point_find_init(&pccs->find,points_check,(void*)pccs);
	pccs->find.pmin = pccs->pmin;
	point_find_contour(&pccs->find,ivec,&pccs->buff);
	ivec_make(dest,ivec->rows,ivec->cols);
	ivec_fill(dest,0);
#ifdef MY1DEBUG_POINTS
	printf("@@ CCLIST {Main:%d,Draw:%d}\n",
		pccs->size,pccs->draw&CCLIST_DRAW_ENB);
#endif
	if (pccs->size>0&&(pccs->draw&CCLIST_DRAW_ENB)) {
		my1points_t *list;
		my1point_t* curr;
		int test,loop;
#ifdef MY1DEBUG_POINTS
		list = pccs->init;
		while (list) {
			curr = list->loop;
			printf("@@ CCLIST => {Size:%d,@(%d,%d)}\n",
				list->fill,curr->x,curr->y);
			list = list->next;
		}
#endif
		if (pccs->draw&CCLIST_DRAW_ALL) {
			list = pccs->init;
			while (list) {
				curr = list->loop;
				for (loop=0;loop<list->fill;loop++,curr++)
				{
					test = point_test_that(dest,curr->x,curr->y);
					dest->data[test] = pccs->buff.data[test];
				}
				list = list->next;
			}
		}
		else {
			list = pccs->lmax;
			curr = list->loop;
			for (loop=0;loop<list->fill;loop++,curr++) {
				test = point_test_that(dest,curr->x,curr->y);
				dest->data[test] = pccs->buff.data[test];
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void points_translate(my1points_t* points, int x, int y) {
	my1point_t *orig, *temp;
	int loop;
	orig = &points->orig;
	orig->x = x;
	orig->y = y;
	points->xmax += orig->x;
	points->ymax += orig->y;
	points->xmin += orig->x;
	points->ymin += orig->y;
	temp = points->loop;
	for (loop=0;loop<points->fill;loop++,temp++) {
		temp->x += orig->x;
		temp->y += orig->y;
	}
}
/*----------------------------------------------------------------------------*/
void points_prep4image(my1points_t* points, int border) {
	/* assume all stats are correct! place orig dead center! */
	/* make sure all points are all positive values */
	points_translate(points,border-points->xmin,border-points->ymin);
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
