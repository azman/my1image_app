/*----------------------------------------------------------------------------*/
/*
	if_origin.h
	- image_filter to redraw original image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_ORIGIN_H__
#define __MY1IF_ORIGIN_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_ORIGIN "if_origin"
/*----------------------------------------------------------------------------*/
my1image_t* filter_origin(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	if (pass->buffer->iref)
		image_copy(res,pass->buffer->iref);
	else res = img; /* use input as output - no processing */
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_ORIGIN_H__ */
/*----------------------------------------------------------------------------*/
