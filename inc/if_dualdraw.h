/*----------------------------------------------------------------------------*/
/*
	if_dualdraw.h
	- image_filter draw input & iref... side by side
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_DUALDRAW_H__
#define __MY1IF_DUALDRAW_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_DUALDRAW "if_dualdraw"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
my1image_t* filter_dualdraw(my1image_t* img, my1image_t* res,
		my1ipass_t* pass) {
	my1image_area_t tmp;
	my1image_t* ref = pass->buffer->iref;
	if (ref&&(ref->cols==img->cols)&&(ref->rows==img->rows)) {
		image_make(res,ref->rows,ref->cols<<1);
		image_fill(res,0);
		res->mask = ref->mask;
		image_area_make(&tmp,0,0,ref->rows,ref->cols);
		image_set_area(res,ref,&tmp);
		if (img->mask!=res->mask) {
			image_copy(&pass->buffer->xtra,img);
			img = &pass->buffer->xtra;
			if (res->mask==IMASK_COLOR) image_colormode(img);
			else image_grayscale(img);
		}
		tmp.xset = ref->cols;
		image_set_area(res,img,&tmp);
	}
	else res = img;
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_DUALDRAW_H__ */
/*----------------------------------------------------------------------------*/
