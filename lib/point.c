/*----------------------------------------------------------------------------*/
#include "ivec_file.h"
#include "point.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, test;
	my1ivec_t load, buff, *ivec;
	my1point_find_t find;
	ivec_init(&load);
	ivec_init(&buff);
	point_find_init(&find,0x0,0x0);
	do {
		stat = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		for (loop=2;loop<argc;loop++) {
			if (!strncmp(argv[loop],"--pmin",7))
				find.pmin = atoi(argv[++loop]);
			else
				printf("** Unknown param '%s'!\n",argv[loop]);
		}
		test = ivec_load(&load,argv[1]);
		if (!test) {
			printf("** Cannot load '%s'!\n",argv[1]);
			stat = -2;
			break;
		}
		ivec = &load;
		printf("-- Load:\n");
		ivec_show(ivec);
		test = point_find_contour(&find,ivec,&buff);
		if (test) {
			ivec = &buff;
			printf("-- Found: %d closed contour(s)\n",test);
			for (loop=0;loop<ivec->size;loop++) {
				if (ivec->data[loop]) {
					ivec->data[loop] &= DIR_MASK;
					ivec->data[loop]++; /* viewable data */
				}
			}
		}
		printf("-- Data:\n");
		ivec_show(ivec);
	} while (0);
	ivec_free(&buff);
	ivec_free(&load);
	return stat;
}
/*----------------------------------------------------------------------------*/
