/*----------------------------------------------------------------------------*/
/*
	if_return.h
	- image_filter to to reinsert image into original image (if larger)
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_RETURN_H__
#define __MY1IF_RETURN_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_RETURN "if_return"
/*----------------------------------------------------------------------------*/
my1image_t* filter_return(my1image_t* img, my1image_t* res,my1ipass_t* pass) {
	my1image_buffer_t* buff = pass->buffer;
	if (buff->iref&&buff->select.hval&&buff->select.wval) {
		/* assume it can fit if select is defined? */
		image_copy(res,buff->iref);
		image_set_area(res,img,&buff->select);
		/* restore region */
		buff->region = buff->select;
		image_area_make(&buff->select,0,0,0,0); /* reset select */
	}
	else res = img; /* maintain input image! */
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_RETURN_H__ */
/*----------------------------------------------------------------------------*/
