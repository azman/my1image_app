/*----------------------------------------------------------------------------*/
/*
	if_inspect.h
	- image_filter to run a custom inspection routine
	  = DOES NOT write output image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_INSPECT_H__
#define __MY1IF_INSPECT_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_INSPECT "if_inspect"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
typedef void (*imgchk_t)(my1image_t* view, my1ipass_t* pass);
/*----------------------------------------------------------------------------*/
typedef struct _my1if_inspect_t {
	void *data;
	imgchk_t proc;
} my1if_inspect_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_inspect(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	my1if_inspect_t* data = (my1if_inspect_t*) pass->data;
	if (data->proc) data->proc(img,pass);
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_inspect_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_inspect_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_inspect_t));
	curr = (my1if_inspect_t*) filter->data;
	if (!pclone) {
		curr->data = 0x0;
		curr->proc = 0x0;
	}
	else {
		pchk = (my1if_inspect_t*) pclone->data;
		curr->data = pchk->data;
		curr->proc = pchk->proc;
	}
}
/*----------------------------------------------------------------------------*/
void filter_inspect_free(my1ipass_t* filter) {
	if (filter->data) free(filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_INSPECT_H__ */
/*----------------------------------------------------------------------------*/
