/*----------------------------------------------------------------------------*/
#ifndef __MY1POINTS_FILE_H__
#define __MY1POINTS_FILE_H__
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "points.h"
/*----------------------------------------------------------------------------*/
#define POINTS_FILE_HEADER "MY1POINTS:"
#define POINTS_FILE_HEADER_SIZE 10
/*----------------------------------------------------------------------------*/
#define POINTS_FILE_OK 0
#define POINTS_FILE_ERROR_OPEN 1
#define POINTS_FILE_ERROR_FMT0 2
#define POINTS_FILE_ERROR_FMT1 3
#define POINTS_FILE_ERROR_FMT2 4
#define POINTS_FILE_ERROR_FMT3 5
#define POINTS_FILE_ERROR_MAKE 6
#define POINTS_FILE_ERROR_READ 7
#define POINTS_FILE_ERROR_FMT4 8
/*----------------------------------------------------------------------------*/
int points_load(my1points_t* data,char* name) {
	int test, size, fill, done, x, y;
	char buff[16];
	FILE* read = fopen(name,"rt");
	if (!read) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return POINTS_FILE_ERROR_OPEN;
	}
	do {
		done = POINTS_FILE_OK;
		if (!fgets(buff,16,read)) {
			done = POINTS_FILE_ERROR_FMT0;
			break;
		}
		test = strlen(buff);
		if (buff[test-1]!='\n') { /* assume size is no more than 4 digit */
			done = POINTS_FILE_ERROR_FMT1;
			break;
		}
		buff[test-1] = 0x0;
		if (strncmp(buff,POINTS_FILE_HEADER,POINTS_FILE_HEADER_SIZE)) {
			done = POINTS_FILE_ERROR_FMT2;
			break;
		}
		size = atoi(&buff[10]);
		if (size<=0) {
			done = POINTS_FILE_ERROR_FMT2;
			break;
		}
		if (!points_make(data,size)) {
			done = POINTS_FILE_ERROR_MAKE;
			break;
		}
		points_zero(data);
		fill = 0;
		while (fill<size) {
			fill++;
			if (!fgets(buff,16,read)) {
				fprintf(stderr,"** Cannot read data %d/%d!\n",fill,size);
				done = POINTS_FILE_ERROR_READ;
				break;
			}
			if (sscanf(buff,"%d,%d\n",&x,&y)!=2) {
				fprintf(stderr,"** Cannot get XY data (%d/%d)!\n",fill,size);
				done = POINTS_FILE_ERROR_FMT4;
				break;
			}
			points_append(data,x,y);
		}
	} while (0);
	fclose(read);
	return done;
}
/*----------------------------------------------------------------------------*/
int points_ostr(my1points_t* data,FILE* ostr) {
	my1point_t* curr;
	int loop;
	fprintf(ostr,"%s%d\n",POINTS_FILE_HEADER,data->fill);
	curr = data->loop;
	for (loop=0;loop<data->fill;loop++,curr++)
		fprintf(ostr,"%d,%d\n",curr->x,curr->y);
	return loop;
}
/*----------------------------------------------------------------------------*/
int points_save(my1points_t* data,char* name) {
	FILE *save = fopen(name,"wt");
	if (!save) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return POINTS_FILE_ERROR_OPEN;
	}
	points_ostr(data,save);
	fclose(save);
	return POINTS_FILE_OK;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1POINTS_FILE_H__ */
/*----------------------------------------------------------------------------*/
