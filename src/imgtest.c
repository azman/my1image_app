/*----------------------------------------------------------------------------*/
#include "my1image_what.h"
/* cannot have this in image_what... pure header! may fix this later... */
#include "ifdb_app0.h"
/*----------------------------------------------------------------------------*/
int init_more(void* data, void* that, void* xtra) {
	/*my1itask_t *task = (my1itask_t*)data;*/
	my1iwhat_t *what = (my1iwhat_t*)that;
	/*my1imain_t *mdat = (my1imain_t*)xtra;*/
	ifdb_generate_base(&what->ifdb);
	ifdb_generate_app0(&what->ifdb);
	return what->stat;
}
/*----------------------------------------------------------------------------*/
/* for gettimeofday, timersub */
#include <sys/time.h>
static int keep = -1;
static int fval,fprv = -1;
static double tval;
static struct timeval curr, prev, diff;
/*----------------------------------------------------------------------------*/
int post_more(void* data, void* that, void* xtra) {
	/*my1itask_t *task = (my1itask_t*)data;*/
	my1iwhat_t *what = (my1iwhat_t*)that;
	/*my1imain_t *mdat = (my1imain_t*)xtra;*/
	if (what->flag&WFLAG_IMAGE_PATH) {
		if (keep<0) {
			gettimeofday(&prev,0x0);
			keep = what->ptgrab.full.step;
		}
		else if (keep!=what->ptgrab.full.step) {
			gettimeofday(&curr,0x0);
			timersub(&curr,&prev,&diff);
			tval = (double)diff.tv_sec+(double)diff.tv_usec/1000000.0;
			fval = (int)round(1.0/tval);
			if (fprv<0||fval!=fprv) {
				printf("Rate:%dHz <%ld.%06lds>\n",fval,
					(long int)(diff.tv_sec),(long int)(diff.tv_usec));
				fprv = fval;
			}
			prev = curr;
			keep = what->ptgrab.full.step;
		}
	}
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1iwhat_t what;
	my1imain_t data;
	iwhat_main(&what,&data);
	what.init.task = init_more;
	what.post.task = post_more;
	imain_init(&data);
	imain_args(&data,argc,argv);
	imain_pass(&data,0x0); /* pass all filters :p */
	imain_prep(&data);
	imain_proc(&data);
	imain_show(&data,argc,argv);
	imain_free(&data);
	return 0;
}
/*----------------------------------------------------------------------------*/
