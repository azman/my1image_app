/*----------------------------------------------------------------------------*/
/*
	if_keepthis.h
	- image_filter to copy current image into keepshow filter
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_KEEPTHIS_H__
#define __MY1IF_KEEPTHIS_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "if_keepshow.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_KEEPTHIS "if_keepthis"
/*----------------------------------------------------------------------------*/
my1image_t* filter_keepthis(my1image_t* img, my1image_t* res,
		my1ipass_t* pass) {
	my1ipass_t* find;
	find = pass->next;
	while (find) {
		if (!strncmp(find->name,IFNAME_KEEPSHOW,FILTER_NAMESIZE)) {
			filter_keepshow_keep(find,img);
			break;
		}
		find = find->next;
	}
	return img;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_KEEPTHIS_H__ */
/*----------------------------------------------------------------------------*/
