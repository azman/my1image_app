# makefile for applications based on my1imgpro

EXETOOL = imgtest
OBJTOOL = my1image.o my1image_what.o my1ipath_grab.o
# these are required by my1ipath_grab.o
OBJTOOL += my1path.o my1path_list.o my1thread.o
OBJTOOL += $(EXETOOL).o
EXEPLAY = imgplay
OBJPLAY = my1image_01full.o $(EXEPLAY).o
EXELOGO = imglogo
OBJLOGO = $(EXELOGO).o

NEED_GTKX = $(EXETOOL)
NEED_PTHR = $(EXETOOL)
NEED_MATH = $(EXETOOL) $(EXEPLAY) $(EXELOGO)
NEED_MATH += points_file vector_file vector_points vector_dist shape
NEED_MATH += shaped shaped_clustering
NEED_LIBV = $(EXETOOL)

SOURCES = $(subst .c,,$(subst src/,,$(sort $(wildcard src/*.c))))
SOURCES += $(subst .c,,$(sort $(wildcard *.c)))
# sourceswith headers (in src) are libraries!make
HEADERS = $(subst .h,,$(subst src/,,$(sort $(wildcard src/*.h))))
TARGETS = $(filter-out $(HEADERS),$(SOURCES))
MODULES = $(filter $(HEADERS),$(SOURCES))
# lib tests
LIBSRCS = $(subst .c,,$(subst lib/,,$(sort $(wildcard lib/*.c))))
LIBHDRS = $(subst .h,,$(subst lib/,,$(sort $(wildcard lib/*.h))))
MODULES += $(filter-out $(LIBSRCS),$(LIBHDRS))

TOPPATH = $(shell cd $(PWD)/.. ; pwd)
EXTPATH = $(TOPPATH)/my1imgpro/src
EX1PATH = $(TOPPATH)/my1codebase/lib
EX2PATH = $(TOPPATH)/my1codelib/src
MYCFLAG = -Wall -I$(EXTPATH) -I$(EX1PATH) -I$(EX2PATH) -Ilib/ -Iinc/ -Isrc/
MYLFLAG = $(LDFLAGS)
ifneq ($(DOFLAG),)
	MYCFLAG += $(DOFLAG)
endif
ifneq ($(DOLINK),)
	MYCFLAG += $(DOLINK)
endif
CFLAGS = $(MYCFLAG)
LFLAGS = $(MYLFLAG)
DFLAGS = -D__MY1IMAGE_DO_DEMO__

# gtk-related flags
GTKVER = 3.0
GFLAGC = $(shell pkg-config --cflags gtk+-$(GTKVER))
GFLAGL = $(shell pkg-config --libs gtk+-$(GTKVER))

$(NEED_GTKX): CFLAGS += $(GFLAGC)
$(NEED_GTKX): LFLAGS += $(GFLAGL)
$(NEED_MATH): LFLAGS += -lm
$(NEED_PTHR): LFLAGS += -lpthread
$(NEED_LIBV): LFLAGS += -lavcodec -lavutil -lavformat -lswscale -lavdevice
$(EXETOOL): CFLAGS += -DMY1APP_VERS=\"$(shell date +%Y%m%d)\"

RM = rm -f
CC = gcc
LD = gcc

debug: CFLAGS += -g -DMY1_DEBUG

.PHONY: all new debug info clean app tool play logo

app:
	@make --no-print-directory -C app/

tool: ${EXETOOL}

play: ${EXEPLAY}

logo: ${EXELOGO}

all: $(TARGETS) $(LIBSRCS)

new: clean tool

debug: new

info:
	@echo
	@echo "Run 'make <bin|lib>'"
	@echo
	@echo "  <lib> = { $(LIBSRCS) }"
	@echo "  <bin> = { $(TARGETS) }"
	@echo
	@echo "  [mod] = { $(MODULES) }"
	@echo
	@echo "To add compiler switch (e.g. -Wall), do 'make <bin> DOFLAG=-Wall'"
	@echo "To link a library (e.g. math), do 'make <bin> DOLINK=-lm'"
	@echo

${EXETOOL}: $(OBJTOOL)
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

${EXEPLAY}: $(OBJPLAY)
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

${EXELOGO}: $(OBJLOGO)
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

# simple single-source-file app
%: src/%.c
	$(CC) $(MYCFLAG) -o $@ $< $(MYLFLAG)

# simple single-source-file app
%: %.c
	$(CC) $(MYCFLAG) -o $@ $< $(MYLFLAG)

# most probably test codes (single-source-file)
%: lib/%.c lib/%.h
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS)

%.o: src/%.c src/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) -c $(CFLAGS) $(DFLAGS) -o $@ $<

%.o: $(EX2PATH)/%.c $(EX2PATH)/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-$(RM) $(EXETOOL) $(EXEPLAY) $(EXELOGO) $(TARGETS) $(LIBSRCS) *.o
