/*----------------------------------------------------------------------------*/
/*
	if_box.h
	- image_filter to box blob
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_BOX_H__
#define __MY1IF_BOX_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
#include "my1image_box.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_BOX "if_box"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
#define MARK_COLOR_RED 0x00FF0000
/*----------------------------------------------------------------------------*/
void ibox_draw(my1ibox_t* rect, my1image_t* draw, int cval) {
	int step, init, ends, loop, chk1, chk2;
	/* prepare cols */
	step = draw->cols;
	init = rect->yt * step;
	ends = rect->yb * step;
	chk1 = rect->xl + init;
	chk2 = rect->xr + init;
	/* left side */
	for (loop=chk1;loop<ends;loop+=step)
		draw->data[loop] = cval;
	/* right side */
	for (loop=chk2;loop<ends;loop+=step)
		draw->data[loop] = cval;
	/* top side */
	for (loop=chk1;loop<chk2;loop++)
		draw->data[loop] = cval;
	/* bottom side */
	chk1 = rect->xl + ends;
	chk2 = rect->xr + ends;
	for (loop=chk1;loop<chk2;loop++)
		draw->data[loop] = cval;
}
/*----------------------------------------------------------------------------*/
typedef struct _my1if_my1ibox_t {
	my1image_scan_t scan;
	my1ibox_t tbox, view;
	int mask, find, rows, cols;
	int mpad, wchk;
} my1if_my1ibox_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_box(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1image_buffer_t* buff = filter->buffer;
	my1image_t* init = buff->iref;
	my1if_my1ibox_t* data = (my1if_my1ibox_t*) filter->data;
	my1image_scan_t* scan = &data->scan;
	my1ibox_t* tbox = &data->tbox;
	/* find boundaries */
	ibox_init(tbox);
	iscan_init(scan,img,BORDER_WIDTH);
	iscan_prep(scan);
	while (iscan_next(scan)) {
		if (iscan_skip(scan)||!scan->data[scan->loop])
			continue;
		/* update box */
		if (tbox->yt<0) tbox->yt = scan->irow;
		else if (tbox->yb<scan->irow) tbox->yb = scan->irow;
		if (tbox->xl<0||tbox->xl>scan->icol) tbox->xl = scan->icol;
		else if (tbox->xr<scan->icol) tbox->xr = scan->icol;
	}
	/* only if valid */
	data->find = 0;
	image_area_make(&buff->region,0,0,0,0);
	if (ibox_valid(tbox)) {
		/* set viewport border */
		ibox_fill(&data->view,scan->skip,scan->bcol,scan->skip,scan->brow);
		/* add margin */
		tbox->yb += data->mpad;
		tbox->yt -= data->mpad;
		tbox->xr += data->mpad;
		tbox->xl -= data->mpad;
		/* size! */
		data->rows = tbox->yb-tbox->yt+1;
		data->cols = tbox->xr-tbox->xl+1;
		if (data->cols<data->wchk) data->cols = 0;
		if (data->cols&&ibox_within(tbox,&data->view)) {
			data->find = 1;
			image_area_make(&buff->region,
				tbox->yt,tbox->xl,data->rows,data->cols);
		}
	}
	/* prepare output */
	if (!init) init = img;
	if (!data->mask) {
		image_copy(res,init);
		if (data->find)
			ibox_draw(tbox,res,MARK_COLOR_RED);
	}
	else {
		image_make(res,init->rows,init->cols);
		res->mask = init->mask;
		if (!data->find)
			image_fill(res,0);
		else {
			iscan_init(scan,res,BORDER_WIDTH);
			iscan_prep(scan);
			while (iscan_next(scan)) {
				if (!ibox_contain(tbox,scan->icol,scan->irow))
					res->data[scan->loop] = 0;
				else
					res->data[scan->loop] = init->data[scan->loop];
			}
		}
	}
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_box_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_my1ibox_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_my1ibox_t));
	curr = (my1if_my1ibox_t*) filter->data;
	if (!pclone) {
		curr->mask = 0;
		curr->mpad = MARGIN_WIDTH;
		curr->wchk = curr->mpad << 2;
	}
	else {
		pchk = (my1if_my1ibox_t*) pclone->data;
		curr->mask = pchk->mask;
		curr->mpad = pchk->mpad;
		curr->wchk = pchk->wchk;
	}
}
/*----------------------------------------------------------------------------*/
void filter_box_free(my1ipass_t* filter) {
	if (filter->data) free(filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_BOX_H__ */
/*----------------------------------------------------------------------------*/
