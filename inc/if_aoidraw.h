/*----------------------------------------------------------------------------*/
/*
	if_aoidraw.h
	- image_filter to draw area-of-interest
	  = by default, use buffer defined region
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_AOIDRAW_H__
#define __MY1IF_AOIDRAW_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_box.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_AOIDRAW "if_aoidraw"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#define MARK_COLOR_RED 0x00FF0000
/*----------------------------------------------------------------------------*/
/* indicates valid aoi found/drawn */
#define FLAG_AOI_DRAW 0x01
/* request to draw on original image (buff->iref) [DEFAULT] */
#define FLAG_AOI_ORIG 0x02
/* flag to draw buffer region [DEFAULT] */
#define FLAG_AOI_BUFF 0x04
/*----------------------------------------------------------------------------*/
typedef struct _my1if_aoidraw_t {
	my1rect_t *pbox;
	my1ibox_t tbox;
	int flag, cval;
} my1if_aoidraw_t;
/*----------------------------------------------------------------------------*/
void draw_aoi(my1ibox_t* rect, my1image_t* draw, int cval) {
	int step, init, ends, loop, chk1, chk2;
	/* prepare cols */
	step = draw->cols;
	init = rect->yt * step;
	ends = rect->yb * step;
	chk1 = rect->xl + init;
	chk2 = rect->xr + init;
	/* left side */
	for (loop=chk1;loop<ends;loop+=step)
		draw->data[loop] = cval;
	/* right side */
	for (loop=chk2;loop<ends;loop+=step)
		draw->data[loop] = cval;
	/* top side */
	for (loop=chk1;loop<chk2;loop++)
		draw->data[loop] = cval;
	/* bottom side */
	chk1 = rect->xl + ends;
	chk2 = rect->xr + ends;
	for (loop=chk1;loop<chk2;loop++)
		draw->data[loop] = cval;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_aoidraw(my1image_t* img, my1image_t* res,
		my1ipass_t* filter) {
	my1if_aoidraw_t* data = (my1if_aoidraw_t*) filter->data;
	if (data->flag&FLAG_AOI_BUFF) {
		data->pbox = (my1rect_t*) &filter->buffer->region;
		ibox_from_rect(&data->tbox,data->pbox);
	}
	if (ibox_valid(&data->tbox)) {
		if ((data->flag&FLAG_AOI_ORIG)&&filter->buffer->iref)
			img = filter->buffer->iref;
		data->flag |= FLAG_AOI_DRAW;
	}
	else data->flag &= ~FLAG_AOI_DRAW;
	image_copy(res,img);
	if (data->flag&FLAG_AOI_DRAW)
		draw_aoi(&data->tbox,res,data->cval);
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_aoidraw_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_aoidraw_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_aoidraw_t));
	curr = (my1if_aoidraw_t*) filter->data;
	if (!pclone) {
		curr->flag = FLAG_AOI_ORIG|FLAG_AOI_BUFF;
		curr->cval = MARK_COLOR_RED;
	}
	else {
		pchk = (my1if_aoidraw_t*) pclone->data;
		curr->flag = pchk->flag;
		curr->cval = pchk->cval;
	}
}
/*----------------------------------------------------------------------------*/
void filter_aoidraw_free(my1ipass_t* filter) {
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_AOIDRAW_H__ */
/*----------------------------------------------------------------------------*/
