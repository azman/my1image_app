/*----------------------------------------------------------------------------*/
/*
	if_blue.h
	- image_filter to suppress blue background in an image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_BLUE_H__
#define __MY1IF_BLUE_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_BLUE "if_blue"
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
#define CHK_DIFF_BR 32
/*----------------------------------------------------------------------------*/
int mask_blue(int *that) {
	my1rgb_t* rgb = (my1rgb_t*) that;
	if (rgb->b>rgb->g&&rgb->r<CHK_DIFF_BR)
		return RGB_WHITE;
	else return *that;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_blue(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	image_make(res,img->rows,img->cols);
	if (img->mask==IMASK_COLOR) {
		my1image_scan_t scan;
		iscan_init(&scan,img,BORDER_BASIC);
		iscan_prep(&scan);
		while (iscan_next(&scan)) {
			if (iscan_skip(&scan)) {
				res->data[scan.loop] = RGB_WHITE;
				continue;
			}
			res->data[scan.loop] = mask_blue(&img->data[scan.loop]);
		}
		res->mask = img->mask;
	}
	else image_copy(res,img);
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_BLUE_H__ */
/*----------------------------------------------------------------------------*/
