/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_CLUSTERING_FILE_H__
#define __MY1SHAPED_CLUSTERING_FILE_H__
/*----------------------------------------------------------------------------*/
#include "shaped_clustering.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void shaped_clustering_show(my1shaped_clustering_t* that, FILE* what) {
	double dmin, davg, dmax, dchk;
	that->clus.curr = 0x0;
	while (list_scan_item(&that->clus)) {
		that->pool = (my1shaped_pool_t*)that->clus.curr->data;
		fprintf(what,"   Cluster %d[%d]",
			that->clus.step,that->pool->pool.size);
		if (that->flag&SC_FLAG_SHOW_STAT) {
			/* get stats */
			that->pool->pool.curr = 0x0;
			while (list_scan_item(&that->pool->pool)) {
				that->pick = (my1shaped_dist_t*)that->pool->pool.curr->data;
				/** assert: that->pick-Llink->from!=(void*)that->pool */
				dchk = that->pick->link->dist;
				if (!that->pool->pool.step) {
					dmin = dmax = davg = dchk;
				}
				else {
					davg += dchk;
					if (dmin>dchk) dmin = dchk;
					if (dmax<dchk) dmax = dchk;
				}
			}
			davg /= that->pool->pool.size;
			fprintf(what,"<Avg:%.10lf,Min:%.10lf,Max:%.10lf>",davg,dmin,dmax);
		}
		if (that->flag&SC_FLAG_SHOW_LIST) {
			/* print cluster items */
			fputc('{',what);
			that->pool->pool.curr = 0x0;
			while (list_scan_item(&that->pool->pool)) {
				that->pick = (my1shaped_dist_t*)that->pool->pool.curr->data;
				if (that->pool->pool.step) fputc(',',what);
				fprintf(what,"%s",that->pick->data.name.buff);
			}
			fputc('}',what);
		}
		fputc('\n',what);
		if (that->flag&SC_FLAG_SHOW_ITEM) {
			that->pool->pool.curr = 0x0;
			while (list_scan_item(&that->pool->pool)) {
				that->pick = (my1shaped_dist_t*)that->pool->pool.curr->data;
				/* link should still hold nearest distance */
				that->link = that->pick->link;
				if (that->link->from!=(void*)that->pool) {
					fprintf(what,"** Invalid pool for '%s'??\n",
						that->pick->data.name.buff);
				}
				else {
					fprintf(what,"@@ %s => Dist:%.10lf\n",
						that->pick->data.name.buff,that->link->dist);
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_CLUSTERING_FILE_H__ */
/*----------------------------------------------------------------------------*/
