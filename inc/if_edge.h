/*----------------------------------------------------------------------------*/
/*
	if_edge.h
	- image_filter to detect edge within ROI
	- input should be binary image (zero/non-zero)
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_EDGE_H__
#define __MY1IF_EDGE_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
#include "my1image_ifdb.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_EDGE "if_edge"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1if_edge_t {
	my1image_buffer_t buff;
	my1image_filter_t pass;
	my1if_threshold_t keep;
} my1if_edge_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_edge(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1image_area_t *area = &filter->buffer->region;
	my1if_edge_t* data = (my1if_edge_t*) filter->data;
	image_copy(res,img);
	if (area->hval&&area->wval) {
		buffer_size_all(&data->buff,area->hval,area->wval);
		image_get_area(img,data->buff.curr,area);
		if (img->mask)
			image_grayscale(data->buff.curr);
		filter_threshold(data->buff.curr,data->buff.next,&data->pass);
		buffer_swap(&data->buff);
		filter_laplace(data->buff.curr,data->buff.next,0x0);
		buffer_swap(&data->buff);
		if (img->mask)
			image_colormode(data->buff.curr);
		image_set_area(res,data->buff.curr,area);
	}
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_edge_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_edge_t* data;
	filter->data = malloc(sizeof(my1if_edge_t));
	data = (my1if_edge_t*) filter->data;
	data->pass.data = (void*) &data->keep;
	buffer_init(&data->buff);
}
/*----------------------------------------------------------------------------*/
void filter_edge_free(my1ipass_t* filter) {
	my1if_edge_t* data = (my1if_edge_t*) filter->data;
	if (data) {
		buffer_free(&data->buff);
		free(filter->data);
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_EDGE_H__ */
/*----------------------------------------------------------------------------*/
