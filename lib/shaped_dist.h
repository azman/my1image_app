/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_DIST_H__
#define __MY1SHAPED_DIST_H__
/*----------------------------------------------------------------------------*/
#include "shaped_item.h"
#include "shaped_link.h"
#include "vector_dist.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_dist_t {
	my1shaped_t data; /* my1shaped_dist_t* == my1shaped_t* !! */
	my1list_t list; /* list of my1shaped_link_t */
	my1shaped_link_t* link;
	my1vector_dist_t* calc;
	my1item_t* iref; /* for dist_calc_full */
	my1list_t* pool; /* current pool that holds iref */
	/* debug stuffs */
	my1list_t* prev;
} my1shaped_dist_t;
/*----------------------------------------------------------------------------*/
void shaped_dist_init(my1shaped_dist_t* that, my1shaped_t* data,
		my1vector_dist_t* calc) {
	shaped_init(&that->data);
	shaped_copy(&that->data,data);
	/* we use only this for clustering */
	vector_magnitude(&that->data.desc);
	list_init(&that->list,free_shaped_link_item);
	/*that->link = 0x0;*/
	that->calc = calc;
	that->iref = 0x0;
	that->pool = 0x0;
}
/*----------------------------------------------------------------------------*/
void shaped_dist_free(my1shaped_dist_t* that) {
	list_free(&that->list);
	shaped_free(&that->data);
}
/*----------------------------------------------------------------------------*/
void shaped_dist_prep_calc(my1shaped_dist_t* that) {
	list_redo(&that->list);
	that->link = 0x0;
}
/*----------------------------------------------------------------------------*/
my1item_t* shaped_dist_find_link(my1shaped_dist_t* that, void* from) {
	my1shaped_link_t* link;
	my1item_t* item;
	item = that->list.init;
	while (item) {
		link = (my1shaped_link_t*)item->data;
		if (link->from==from)
			break;
		item = item->next;
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* shaped_dist_make_link(my1shaped_dist_t* that,
		my1shaped_dist_t* from, double dist) {
	my1shaped_link_t *link;
	my1item_t* item;
	item = make_shaped_link_item((void*)from,dist);
	if (item) {
		list_push_item(&that->list,item);
		link = (my1shaped_link_t*)item->data;
		/* pick a link with largest distance */
		if (!that->link||that->link->dist<dist)
			that->link = link;
	}
	return item;
}
/*----------------------------------------------------------------------------*/
void shaped_dist_calc_full(my1shaped_dist_t* that, my1list_t* data) {
	/* data is the global list of my1shaped_dist_t* */
	my1shaped_dist_t *temp;
	my1item_t *item;
	double dval;
	/* should not use data->curr! calling function may be scanning! */
	item = data->init;
	while (item) {
		temp = (my1shaped_dist_t*)item->data;
		if (temp!=that) {
			if (!shaped_dist_find_link(that,(void*)temp)) {
				dval = vector_dist_calc(that->calc,SVEC(that),SVEC(temp));
				shaped_dist_make_link(that,temp,dval);
				shaped_dist_make_link(temp,that,dval);
			}
		}
		item = item->next;
	}
}
/*----------------------------------------------------------------------------*/
void shaped_dist_calc_pool(my1shaped_dist_t* that, my1list_t* data) {
	/* data is the list of my1shaped_pool_t* (my1shaped_t*) */
	my1shaped_t *temp;
	my1item_t *item, *it3m;
	double dval;
	/* should already call shaped_dist_prep_calc */
	item = data->init;
	while (item) {
		temp = (my1shaped_t*)item->data; /* actually my1shaped_pool_t* */
		dval = vector_dist_calc(that->calc,SVEC(that),SVEC(temp));
		/* cannot use shaped_dist_make_link coz need to pick min */
		it3m = make_shaped_link_item((void*)temp,dval);
		if (it3m) {
			list_push_item(&that->list,it3m);
			if (!that->link||dval<that->link->dist)
				that->link = (my1shaped_link_t*)it3m->data;
		}
		item = item->next;
	}
}
/*----------------------------------------------------------------------------*/
my1item_t* shaped_dist_move(my1shaped_dist_t* that,
		my1list_t* curr, my1list_t* next) {
	my1item_t* item;
	item = (my1item_t*)that->iref;
	if (item) {
		/* make sure valid parent and between pools */
		if (curr!=that->pool||curr->done||next->done)
			item = 0x0;
		else {
			list_hack_item(curr,item);
			list_push_item(next,item);
			that->pool = next;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* make_shaped_dist_item(my1shaped_t* that, my1vector_dist_t* calc) {
	my1shaped_dist_t* temp;
	my1item_t* item;
	item = make_item();
	if (item) {
		temp = (my1shaped_dist_t*)malloc(sizeof(my1shaped_dist_t));
		if (temp) {
			shaped_dist_init(temp,that,calc);
			item->data = (void*)temp;
		}
		else {
			free((void*)item);
			item = 0x0;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
void free_shaped_dist_item(void* that) {
	my1item_t* item = (my1item_t*)that;
	shaped_dist_free((my1shaped_dist_t*)item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_DIST_H__ */
/*----------------------------------------------------------------------------*/
