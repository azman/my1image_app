/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_CLUSTERING_H__
#define __MY1SHAPED_CLUSTERING_H__
/*----------------------------------------------------------------------------*/
#include "shaped_pool.h"
#include "shaped_db.h"
/*----------------------------------------------------------------------------*/
#define SC_FLAG_SHOW_ITEM 0x0001
#define SC_FLAG_SHOW_STAT 0x0002
#define SC_FLAG_SHOW_LIST 0x0004
/*----------------------------------------------------------------------------*/
typedef void (*event0_t)(void*);
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_clustering_t /* shaped clustering object */ {
	my1list_t main; /* main data (my1shaped_dist_t objects) */
	my1list_t clus; /* list of my1shaped_pool_t */
	my1list_t temp; /* temp placement (my1shaped_dist_t pointers) */
	my1vector_dist_t* calc; /* distance calc function */
	int size, flag, move, loop;
	/* event handlers */
	event0_t kmeans_prep_done;
	event0_t kmeans_init_done;
	event0_t kmeans_loop_next;
	event0_t kmeans_init_done0;
	event0_t kmeans_init_done1;
	event0_t kmeans_init_done2;
	/* temp stuff */
	double dval, dmax;
	my1shaped_dist_t *pick, *chk1, *chk2, *pdst;
	my1shaped_pool_t *pool, *poo1;
	my1shaped_link_t *link, *plnk;
	my1shaped_t* cont;
	my1list_t *fifo;
	my1item_t *item, *it3m;
	void *gdat; /* user data - just in case */
} my1shaped_clustering_t;
/*----------------------------------------------------------------------------*/
void shaped_clustering_init(my1shaped_clustering_t* that,
		my1vector_dist_t* calc) {
	list_init(&that->main,free_shaped_dist_item);
	list_init(&that->clus,free_shaped_pool_item);
	list_init(&that->temp,0x0); /* item(s) will be passed around */
	that->calc = calc;
	that->size = 0;
	that->flag = 0;
	that->kmeans_prep_done = 0x0;
	that->kmeans_init_done = 0x0;
	that->kmeans_loop_next = 0x0;
	that->kmeans_init_done0 = 0x0;
	that->kmeans_init_done1 = 0x0;
	that->kmeans_init_done2 = 0x0;
}
/*----------------------------------------------------------------------------*/
void shaped_clustering_free(my1shaped_clustering_t* that) {
	that->temp.done = list_free_item; /* just to make sure */
	list_free(&that->temp);
	list_free(&that->clus);
	list_free(&that->main);
}
/*----------------------------------------------------------------------------*/
void shaped_clustering_data(my1shaped_clustering_t* that,
		my1shaped_db_t* data) {
	/* create shaped_dist for all shaped in data! */
	data->list.curr = 0x0;
	while (list_scan_item(&data->list)) {
		that->cont = (my1shaped_t*)data->list.curr->data;
		that->item = make_shaped_dist_item(that->cont,that->calc);
		if (that->item) list_push_item(&that->main,that->item);
	}
	/* assert that->main.size = data->list.size */
}
/*----------------------------------------------------------------------------*/
int shaped_clustering_prep(my1shaped_clustering_t* that, int size) {
	int loop;
	/* clear previous calc results (@links) */
	that->main.curr = 0x0;
	while (list_scan_item(&that->main)) {
		that->pick = (my1shaped_dist_t*)that->main.curr->data;
		shaped_dist_prep_calc(that->pick);
	}
	/* clear all current clusters */
	/* - all pools will delete items because of free_shaped_pool_item */
	list_redo(&that->clus);
	/* clear temp - just in case */
	that->temp.done = list_free_item; /* make sure prev items are freed! */
	list_redo(&that->temp);
	that->temp.done = 0x0;
	/* create temp pool */
	that->main.curr = 0x0;
	while (list_scan_item(&that->main)) {
		that->item = make_item();
		if (that->item) {
			list_push_item(&that->temp,that->item);
			that->item->data = that->main.curr->data;
			that->pick = (my1shaped_dist_t*)that->item->data;
			that->pick->iref = that->item; /* this item has mobility! */
			that->pick->pool = &that->temp; /* current container */
		}
	}
	/* must have all to create clusters */
	if (that->temp.size==that->main.size) {
		for (loop=0;loop<size;loop++) {
			that->item = make_shaped_pool_item();
			if (that->item) list_push_item(&that->clus,that->item);
		}
	}
	return that->clus.size;
}
/*----------------------------------------------------------------------------*/
int shaped_clustering_kmeans_init(my1shaped_clustering_t* that) {
	/* calculate distance between all nodes */
	that->link = 0x0; that->chk1 = 0x0; that->chk2 = 0x0;
	that->temp.curr = 0x0;
	while (list_scan_item(&that->temp)) {
		that->pick = (my1shaped_dist_t*)that->temp.curr->data;
		shaped_dist_calc_full(that->pick,&that->temp);
		if (!that->link||that->link->dist<that->pick->link->dist) {
			that->link = that->pick->link;
			that->chk1 = that->pick;
			that->chk2 = (my1shaped_dist_t*)that->link->from;
		}
	}
	/* keep these for top level reference */
	that->pdst = that->chk1;
	that->plnk = that->link;
	if (that->kmeans_init_done0)
		that->kmeans_init_done0(that);
	if (!that->chk1||!that->chk2) return -1;
	/* assign initial item for each cluster (pool) */
	/* - for first two clusters, take 2 items with max dist */
	that->item = that->clus.init;
	that->pool = (my1shaped_pool_t*)that->item->data;
	shaped_dist_move(that->chk1,&that->temp,&that->pool->pool);
	that->item = that->item->next;
	that->pool = (my1shaped_pool_t*)that->item->data;
	shaped_dist_move(that->chk2,&that->temp,&that->pool->pool);
	/* - for additional clusters */
	for (that->loop=2;that->loop<that->clus.size;that->loop++) {
		/* pick a shaped_dist with largest average dist from all clusters */
		that->pick = 0x0;
		that->temp.curr = 0x0;
		while (list_scan_item(&that->temp)) {
			that->chk1 = (my1shaped_dist_t*)that->temp.curr->data;
			that->dval = 0.0;
			that->clus.curr = 0x0;
			while (list_scan_item(&that->clus)) {
				if (that->clus.step==that->loop) break;
				that->pool = (my1shaped_pool_t*)that->clus.curr->data;
				/* assume first data valid in these clusters */
				that->chk2 = (my1shaped_dist_t*)that->pool->pool.init->data;
				that->it3m = shaped_dist_find_link(that->chk1,that->chk2);
				if (that->it3m) {
					that->link = (my1shaped_link_t*)that->it3m->data;
					that->dval += that->link->dist;
				}
			}
			that->dval /= that->loop;
			if (!that->pick||that->dval>that->dmax) {
				that->pick = that->chk1;
				that->dmax = that->dval;
			}
		}
		if (!that->pick) break; /* cannot find suitable candidate */
		that->item = that->item->next;
		that->pool = (my1shaped_pool_t*)that->item->data;
		shaped_dist_move(that->pick,&that->temp,&that->pool->pool);
	}
	if (that->kmeans_init_done1)
		that->kmeans_init_done1(that);
	if (that->loop!=that->clus.size) return -2;
	/* start placing all shaped_dist in temp into clusters */
	that->item = that->temp.init;
	while (that->item) {
		that->chk1 = (my1shaped_dist_t*)that->item->data;
		/* find nearest cluster for that->chk1 */
		that->poo1 = 0x0;
		that->clus.curr = 0x0;
		while (list_scan_item(&that->clus)) {
			/* assume first data valid in these clusters */
			that->pool = (my1shaped_pool_t*)that->clus.curr->data;
			that->chk2 = (my1shaped_dist_t*)that->pool->pool.init->data;
			that->it3m = shaped_dist_find_link(that->chk1,that->chk2);
			if (that->it3m) {
				that->link = (my1shaped_link_t*)that->it3m->data;
				if (!that->poo1||that->link->dist<that->dval) {
					that->poo1 = that->pool;
					that->dval = that->link->dist;
				}
			}
		}
		/* should find 1 - so, move! */
		that->item = that->item->next;
		shaped_dist_move(that->chk1,&that->temp,&that->poo1->pool);
	}
	if (that->kmeans_init_done2)
		that->kmeans_init_done2(that);
	if (that->temp.size) return -3;
	return that->clus.size;
}
/*----------------------------------------------------------------------------*/
int shaped_clustering_kmeans_next(my1shaped_clustering_t* that) {
	/* calculate cluster average */
	that->clus.curr = 0x0;
	while (list_scan_item(&that->clus)) {
		that->pool = (my1shaped_pool_t*)that->clus.curr->data;
		shaped_pool_average(that->pool);
	}
	/* calculate distance to all clusters */
	that->move = 0; /* init 'moved' flag */
	that->main.curr = 0x0;
	while (list_scan_item(&that->main)) {
		that->pick = (my1shaped_dist_t*)that->main.curr->data;
		shaped_dist_prep_calc(that->pick);
		shaped_dist_calc_pool(that->pick,&that->clus);
		/* move if current pool is NOT nearest pool */
		that->link = (my1shaped_link_t*)that->pick->link;
		that->pool = (my1shaped_pool_t*)that->link->from;
		that->fifo = &that->pool->pool;
		if (that->pick->pool!=that->fifo) {
			shaped_dist_move(that->pick,that->pick->pool,&that->pool->pool);
			that->move++;
		}
	}
	return that->move;
}
/*----------------------------------------------------------------------------*/
int shaped_clustering_kmeans(my1shaped_clustering_t* that, int size) {
	int done;
	if (size<2||size>that->main.size) return -1;
	done = shaped_clustering_prep(that,size);
	if (that->kmeans_prep_done)
		that->kmeans_prep_done(that);
	if (done!=size) return -2;
	done = shaped_clustering_kmeans_init(that);
	if (that->kmeans_init_done)
		that->kmeans_init_done(that);
	if (done!=size) return -3;
	that->loop = 0;
	do {
		that->loop++;
		shaped_clustering_kmeans_next(that);
		if (that->kmeans_loop_next)
			that->kmeans_loop_next(that);
	} while (that->move);
	return that->loop;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_CLUSTERING_H__ */
/*----------------------------------------------------------------------------*/
