/*----------------------------------------------------------------------------*/
#ifndef __IFDB_APP0_H__
#define __IFDB_APP0_H__
/*----------------------------------------------------------------------------*/
/**
 * Sample application-specific image filter DB build
**/
/*----------------------------------------------------------------------------*/
#include "my1image_ifdb_base.h"
/*----------------------------------------------------------------------------*/
#include "if_aoiblob.h"
#include "if_aoidraw.h"
#include "if_aoimake.h"
#include "if_area.h"
#include "if_blue.h"
#include "if_box.h"
#include "if_branch.h"
#include "if_branch_points.h"
#include "if_branch_select.h"
#include "if_ccl.h"
#include "if_dofind.h"
#include "if_dualdraw.h"
#include "if_edge.h"
#include "if_green.h"
#include "if_green_hsv.h"
#include "if_inspect.h"
#include "if_keepshow.h"
#include "if_keepthis.h"
#include "if_mango.h"
#include "if_origin.h"
#include "if_points.h"
#include "if_return.h"
#include "if_select.h"
#include "if_shaped.h"
#include "if_stem.h"
#include "if_wipe.h"
/*----------------------------------------------------------------------------*/
void ifdb_generate_app0(my1ifdb_t* ifdb) {
	ifdb_append_full(ifdb,IFNAME_AOIBLOB,0,0,filter_aoiblob,
		filter_aoiblob_init,filter_aoiblob_free,filter_aoiblob_conf);
	ifdb_append(ifdb,IFNAME_AOIDRAW,0,0,
		filter_aoidraw,filter_aoidraw_init,filter_aoidraw_free);
	ifdb_append(ifdb,IFNAME_AOIMAKE,0,0,
		filter_aoimake,filter_aoimake_init,filter_aoimake_free);
	ifdb_append(ifdb,IFNAME_AREA,0,0,
		filter_area,filter_area_init,filter_area_free);
	ifdb_append(ifdb,IFNAME_BLUE,0,0,filter_blue,0x0,0x0);
	ifdb_append(ifdb,IFNAME_BOX,0,0,
		filter_box,filter_box_init,filter_box_free);
	ifdb_append_full(ifdb,IFNAME_BRANCH,0,0,filter_branch,
		filter_branch_init,filter_branch_free,filter_branch_conf);
	ifdb_append_full(ifdb,IFNAME_BRANCH_POINTS,0,0,filter_branch,
		filter_branch_points_init,filter_branch_free,filter_branch_conf);
	ifdb_append_full(ifdb,IFNAME_BRANCH_SELECT,0,0,filter_branch,
		filter_branch_select_init,filter_branch_free,filter_branch_conf);
	ifdb_append_full(ifdb,IFNAME_CCL,0,0,filter_ccl,
		filter_ccl_init,filter_ccl_free,filter_ccl_conf);
	ifdb_append(ifdb,IFNAME_DOFIND,0,0,
		filter_dofind,filter_dofind_init,filter_dofind_free);
	ifdb_append(ifdb,IFNAME_DUALDRAW,0,0,filter_dualdraw,0x0,0x0);
	ifdb_append(ifdb,IFNAME_EDGE,0,0,
		filter_edge,filter_edge_init,filter_edge_free);
	ifdb_append(ifdb,IFNAME_GREEN,0,0,filter_green,0x0,0x0);
	ifdb_append(ifdb,IFNAME_GREEN_HSV,0,0,filter_green_hsv,0x0,0x0);
	ifdb_append(ifdb,IFNAME_INSPECT,0,0,
		filter_inspect,filter_inspect_init,filter_inspect_free);
	ifdb_append(ifdb,IFNAME_KEEPSHOW,0,0,
		filter_keepshow,filter_keepshow_init,filter_keepshow_free);
	ifdb_append(ifdb,IFNAME_KEEPTHIS,0,0,filter_keepthis,0x0,0x0);
	ifdb_append(ifdb,IFNAME_MANGO,0,0,filter_mango,0x0,0x0);
	ifdb_append(ifdb,IFNAME_ORIGIN,0,0,filter_origin,0x0,0x0);
	ifdb_append_full(ifdb,IFNAME_POINTS,0,0,filter_points,
		filter_points_init,filter_points_free,filter_points_conf);
	ifdb_append(ifdb,IFNAME_RETURN,0,0,filter_return,0x0,0x0);
	ifdb_append(ifdb,IFNAME_SELECT,0,0,filter_select,0x0,0x0);
	ifdb_append(ifdb,IFNAME_SHAPED,0,0,
		filter_shaped,filter_shaped_init,filter_shaped_free);
	ifdb_append(ifdb,IFNAME_STEM,0,0,
		filter_stem,filter_stem_init,filter_stem_free);
	ifdb_append(ifdb,IFNAME_WIPE,0,0,filter_wipe,0x0,0x0);
}
/*----------------------------------------------------------------------------*/
#endif /** __IFDB_APP0_H__ */
/*----------------------------------------------------------------------------*/
