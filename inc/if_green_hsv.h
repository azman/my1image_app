/*----------------------------------------------------------------------------*/
/*
	if_green_hsv.h
	- image_filter to find green blob in an image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_GREEN_HSV_H__
#define __MY1IF_GREEN_HSV_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
#include "my1image_chsv.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_GREEN_HSV "if_green_hsv"
/*----------------------------------------------------------------------------*/
#define HSV_THRESH_S 15
/*----------------------------------------------------------------------------*/
int pick_green_hsv(int *that) {
	my1hsv_t test = rgb2hsv(*((my1rgb_t*)that));
	if ((test.h<=(HUE_INIT_G+(HUE_DIFF>>1)))&&
		(test.h>=(HUE_INIT_G-(HUE_DIFF>>1)))&&
		(test.s>HSV_THRESH_S)) return *that;
	else return 0;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_green_hsv(my1image_t* img, my1image_t* res,
		my1ipass_t* pass) {
	if (img->mask==IMASK_COLOR) {
		my1image_scan_t scan;
		image_make(res,img->rows,img->cols);
		iscan_init(&scan,img,BORDER_WIDTH);
		iscan_prep(&scan);
		while (iscan_next(&scan)) {
			if (iscan_skip(&scan)) {
				res->data[scan.loop] = 0;
				continue;
			}
			res->data[scan.loop] = pick_green_hsv(&img->data[scan.loop]);
		}
		res->mask = img->mask;
	}
	else image_copy(res,img);
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_GREEN_HSV_H__ */
/*----------------------------------------------------------------------------*/
