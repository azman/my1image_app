/*----------------------------------------------------------------------------*/
#include "my1image_what.h"
#include "my1image_file.h"
/*----------------------------------------------------------------------------*/
int init_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	libav1_init(&what->avgrab,&mdat->load);
	path_grab_init(&what->ptgrab);
	image_appw_init(&what->awin);
	image_init(&what->buff);
	ifdb_init(&what->ifdb);
	what->orig = 0x0;
	what->xtra = 0x0;
	what->list = 0x0;
	what->load = 0x0;
	what->text = 0x0;
	what->tcol = 20;
	what->trow = 20;
	what->txsz = 12;
	what->trgb = 0x000000ff;
	what->flag = 0;
	what->stat = 0;
	itask_exec(&what->init,task->data,that);
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int free_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	if (what->text) {
		free((void*)what->text);
		what->text = 0x0;
	}
	ifdb_free(&what->ifdb);
	image_free(&what->buff);
	image_appw_free(&what->awin);
	path_grab_free(&what->ptgrab);
	libav1_free(&what->avgrab);
	itask_exec(&what->free,task->data,that);
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int igrab_grab_video(void* data, void* that, void* xtra) {
	my1itask_t* ptemp = (my1itask_t*) data;
	my1image_grab_t* igrab = (my1image_grab_t*) that;
	my1iwhat_t *what = (my1iwhat_t*)ptemp->data;
	my1libav_grab_t* vgrab = (my1libav_grab_t*)&what->avgrab;
	if (!vgrab->flag) {
		if ((what->flag&WFLAG_VIDEO_LIVE)==WFLAG_VIDEO_LIVE)
			libav1_live(vgrab,igrab->pick);
		else
			libav1_file(vgrab,igrab->pick);
		if (!vgrab->flag)
			igrab->flag |= IGRAB_FLAG_LOAD_ERROR;
	}
	libav1_grab(vgrab,igrab->grab);
	return igrab->flag;
}
/*----------------------------------------------------------------------------*/
int igrab_grab_path(void* data, void* that, void* xtra) {
	my1itask_t* ptemp = (my1itask_t*) data;
	my1image_grab_t* igrab = (my1image_grab_t*) that;
	my1iwhat_t *what = (my1iwhat_t*)ptemp->data;
	my1ipath_grab_t* pgrab = (my1ipath_grab_t*)&what->ptgrab;
	if (pgrab->flag==PGRAB_FLAG_INIT) {
		path_grab_path(pgrab,igrab->pick);
		if (pgrab->flag&PGRAB_FLAG_ERROR)
			igrab->flag |= IGRAB_FLAG_LOAD_ERROR;
	}
	else path_grab_next(pgrab);
	image_copy(igrab->grab,pgrab->show);
	return igrab->flag;
}
/*----------------------------------------------------------------------------*/
int args_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)task->temp;
	my1igrab_t *grab = (my1igrab_t*)&mdat->grab;
	int loop, argc;
	char** argv;
	argv = (char**) xtra;
	argc = *(int*)that;
	if (mdat->flag&IFLAG_ERROR) return mdat->flag;
	/* re-check parameter for video option */
	for (loop=(argc>1&&argv[1][0]=='-')?1:2;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--video",8)) {
			if (grab->pick)
				printf("** Multiple source? (%s|%s)\n",grab->pick,argv[loop+1]);
			else {
				if (++loop<argc) {
					grab->pick = argv[loop];
					what->flag |= WFLAG_VIDEO_MODE;
				}
				else {
					printf("** No source for '%s'!\n",argv[loop-1]);
					mdat->flag |= IFLAG_ERROR_ARGS;
				}
			}
		}
		else if (!strncmp(argv[loop],"--live",7)) {
			if (grab->pick)
				printf("** Multiple source? (%s|%s)\n",grab->pick,argv[loop+1]);
			else {
				if (++loop<argc) {
					grab->pick = argv[loop];
					what->flag |= WFLAG_VIDEO_LIVE;
				}
				else {
					printf("** No source for '%s'!\n",argv[loop-1]);
					mdat->flag |= IFLAG_ERROR_ARGS;
				}
			}
		}
		else if (!strncmp(argv[loop],"--path",7)) {
			if (grab->pick)
				printf("** Multiple source? (%s|%s)\n",grab->pick,argv[loop+1]);
			else {
				if (++loop<argc) {
					grab->pick = argv[loop];
					what->flag |= WFLAG_IMAGE_PATH;
				}
				else {
					printf("** No source for '%s'!\n",argv[loop-1]);
					mdat->flag |= IFLAG_ERROR_ARGS;
				}
			}
		}
		else if (!strncmp(argv[loop],"--original",10))
			what->flag |= WFLAG_SHOW_ORIGINAL;
		else if (!strncmp(argv[loop],"--filter",9))
			what->list = ++loop<argc?argv[loop]:0x0;
		else if (!strncmp(argv[loop],"--load",7))
			what->load = ++loop<argc?argv[loop]:0x0;
		else if (!strncmp(argv[loop],"--pause",8))
			mdat->flag |= IFLAG_VIDEO_HOLD;
		else if (!strncmp(argv[loop],"--xfilter",10))
			mdat->flag |= IFLAG_FILTER_EXE;
		else
			printf("** Unknown param '%s'!\n",argv[loop]);
	}
	/* setup grabber */
	if (what->flag&WFLAG_VIDEO_MODE) {
		grab->do_grab.task = igrab_grab_video;
		grab->do_grab.data = (void*)what;
		grab->grab = &mdat->load;
	}
	else if(what->flag&WFLAG_IMAGE_PATH) {
		grab->do_grab.task = igrab_grab_path;
		grab->do_grab.data = (void*)what;
		grab->grab = &mdat->load;
	}
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int prep_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	if (what->flag&WFLAG_VIDEO_MODE||what->flag&WFLAG_IMAGE_PATH)
		mdat->flag |= IFLAG_VIDEO_MODE;
	/* mdat->show should point to grabbed image! */
	if (what->flag&WFLAG_SHOW_ORIGINAL)
		what->orig = mdat->view;
	/** check requested filters */
	if (what->load) {
		/**imain_filter_doload_file(mdat,(char*)what->load);*/
		/* we want full control on filter loading */
		char fname[FILTER_NAMESIZE*2], *pname;
		FILE* pfile;
		pfile = fopen((char*)what->load,"rt");
		if (pfile) {
			printf("-- Loading filter list file '%s'\n",what->load);
			while (fgets(fname,FILTER_NAMESIZE*2,pfile)) {
				pname = fname;
				while (*pname) {
					if (pname[0]=='\n'||pname[0]==' '||
							pname[0]=='\t'||pname[0]=='\r') {
						pname[0] = 0x0;
					}
					pname++;
				}
				prep_pass(mdat,fname);
			}
			fclose(pfile);
		}
		else
			printf("** Cannot load filter list file '%s'!\n",what->load);
	}
	if (what->list) {
		/**imain_filter_doload_list(mdat,what->list);*/
		/* we want full control on filter loading */
		char *pchk = what->list;
		int loop = 0, curr = 0, stop = 0;
		while (!stop) {
			switch (pchk[loop]) {
				case 0x0: stop = 1;
				case ',':
					pchk[loop] = 0x0;
					prep_pass(mdat,&pchk[curr]);
					if (!stop) pchk[loop] = ',';
					curr = ++loop;
					break;
				default: loop++;
			}
		}
	}
	itask_exec(&what->prep,task->data,that);
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int exec_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	itask_exec(&what->post,task->data,that);
	image_copy(&what->buff,mdat->view);
	image_colormode(&what->buff); /* must be in color for display */
	mdat->view = &what->buff;
	return what->stat;
}
/*----------------------------------------------------------------------------*/
int what_on_keychk(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)task->temp;
	GdkEventKey *event = (GdkEventKey*) xtra;
	if (event->keyval == GDK_KEY_z)
		imain_menu_filter_enable(mdat,!(mdat->flag&IFLAG_FILTER_EXE));
	else if (event->keyval == GDK_KEY_space)
		mdat->flag ^= IFLAG_VIDEO_HOLD;
	else if (event->keyval == GDK_KEY_r) {
		if (what->ptgrab.tchk&PGRAB_TCHK_ENDS) {
			path_grab_noends(&what->ptgrab);
			printf("@@ Reset PGRAB!(%08x)\n",what->ptgrab.tchk);
		}
	}
	else if (event->keyval == GDK_KEY_plus) {
		if (what->ptgrab.dostep<what->ptgrab.show->cols/5) {
			what->ptgrab.dostep += DIFF_COLSKIP;
			printf("@@ dostep=%d\n",what->ptgrab.dostep);
		}
	}
	else if (event->keyval == GDK_KEY_minus) {
		if (what->ptgrab.dostep>DIFF_COLSKIP) {
			what->ptgrab.dostep -= DIFF_COLSKIP;
			printf("@@ dostep=%d\n",what->ptgrab.dostep);
		}
	}
	else if (event->keyval == GDK_KEY_g) {
		/* allow grab ONLY if on hold */
		if (mdat->flag&IFLAG_VIDEO_HOLD) {
			GtkWidget *dosave;
			my1image_appw_t *appw = &what->awin;
			dosave = gtk_file_chooser_dialog_new("Save Image File",
				GTK_WINDOW(appw->window),GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Open", GTK_RESPONSE_ACCEPT, NULL);
			gtk_file_chooser_set_do_overwrite_confirmation(
				GTK_FILE_CHOOSER(dosave),TRUE);
			if (gtk_dialog_run(GTK_DIALOG(dosave))==GTK_RESPONSE_ACCEPT) {
				gchar *filename;
				filename = gtk_file_chooser_get_filename(
					GTK_FILE_CHOOSER(dosave));
				if (image_save(mdat->view,filename))
					printf("** Grab failed!\n");
				else
					printf("-- Frame saved to '%s'!\n",filename);
				g_free(filename);
			}
			gtk_widget_destroy(dosave);
		}
	}
	return what->stat;
}
/*----------------------------------------------------------------------------*/
void what_text_show(my1iwhat_t* what, char* text) {
	if (what->text) {
		free((void*)what->text);
		what->text = 0x0;
	}
	if (text) {
		what->text = strdup(text);
		if (what->tcol<0) what->tcol = 0;
		if (what->trow<0) what->trow = 0;
	}
}
/*----------------------------------------------------------------------------*/
int what_text_draw(void* data, void* that, void* xtra) {
	my1image_view_t* view = (my1image_view_t*) that;
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	if (what->text) {
		cairo_set_source_rgb(view->dodraw,(float)((what->trgb>>16)&0xff)/255,
			(float)((what->trgb>>8)&0xff)/255,(float)(what->trgb&0xff)/255);
		cairo_move_to(view->dodraw,what->tcol,what->trow);
		cairo_set_font_size(view->dodraw,what->txsz);
		cairo_show_text(view->dodraw,what->text);
		cairo_stroke(view->dodraw);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int show_what(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1iwhat_t *what = (my1iwhat_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	if (what->orig) {
		my1image_show_t show = { what->orig, "Source Image", 0, 0};
		/* show original */
		image_appw_show(&what->awin,&show);
		/* modify name for main win */
		image_appw_name(&mdat->iwin,"Processed Image");
	}
	if (!(mdat->flag&IFLAG_VIDEO_MODE))
		image_appw_domenu(&mdat->iwin);
	imain_domenu_filters(mdat);
	image_appw_domenu_quit(&mdat->iwin);
	itask_make(&mdat->iwin.view.domore,what_text_draw,(void*)what);
	if (what->flag&WFLAG_VIDEO_MODE||what->flag&WFLAG_IMAGE_PATH) {
		itask_make(&mdat->iwin.keychk,what_on_keychk,(void*)what);
		mdat->iwin.keychk.temp = (void*) mdat;
		imain_loop(mdat,0);
	}
	return what->stat;
}
/*----------------------------------------------------------------------------*/
void iwhat_main(my1iwhat_t* what, my1imain_t* that) {
	imain_data(that,what);
	that->init.task = init_what;
	that->free.task = free_what;
	that->args.task = args_what;
	that->prep.task = prep_what;
	that->proc.task = exec_what;
	that->show.task = show_what;
	/* extra tasks for what */
	itask_make(&what->init,0x0,0x0);
	itask_make(&what->free,0x0,0x0);
	itask_make(&what->prep,0x0,0x0);
	itask_make(&what->post,0x0,0x0);
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_pass(my1imain_t* idat, char *name) {
	my1ipass_t *pass;
	my1iwhat_t* what;
	what = (my1iwhat_t*)idat->data;
	if (!name) {
		pass = ifdb_make_all(&what->ifdb);
		/* remove all existing */
		if (idat->list)
			filter_free_clones(idat->list);
		/* assume all is ok */
		idat->list = pass;
	}
	else {
		pass = ifdb_find(&what->ifdb,name);
		if (pass) imain_filter_dolist(idat,pass);
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* prep_pass(my1imain_t *mdat, char *name) {
	my1ipass_t* test;
	char *conf;
	conf = strchr(name,(int)':');
	if (conf) conf[0] = 0x0;
	printf("## Loading filter '%s'... ",name);
	if (conf) conf[0] = ':';
	if ((test=imain_filter_doload(mdat,name))) printf("OK!\n");
	else printf("not found?!\n");
	return test;
}
/*----------------------------------------------------------------------------*/
