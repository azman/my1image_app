/*----------------------------------------------------------------------------*/
/*
	if_aoiblob.h
	- image_filter to set area-of-interest to contain blob
	  = input assumed to contain single continuous blob
	  = DOES NOT write output image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_AOIBLOB_H__
#define __MY1IF_AOIBLOB_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_scan.h"
#include "my1image_box.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_AOIBLOB "if_aoiblob"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define BLOB_PADDING_DEFAULT 10
#define BORDER_WIDTH_DEFAULT 1
/*----------------------------------------------------------------------------*/
/* indicates valid blob marked */
#define FLAG_AOI_BLOB 0x01
/* flag to ignore rule to be within border view */
#define FLAG_AOI_NO_BORDER 0x0100
/*----------------------------------------------------------------------------*/
typedef struct _my1if_aoiblob_t {
	my1iscan_t scan;
	my1ibox_t tbox, view;
	int flag, temp; /* bitwise status flag & temp storage */
	int rows, cols; /* temporary storage for blob size calculation */
	int mpad, bpad; /* blob padding and border padding */
	int padx, pady; /* separate blob side paddings */
	int wchk, hchk; /* minimum blob size to mark */
	int arow, acol; /* target aspect ratio */
	float hrat, wrat;
} my1if_aoiblob_t;
/*----------------------------------------------------------------------------*/
int filter_aoiblob_flag(my1ipass_t* filter) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	return data->flag;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_padding(my1ipass_t* filter, int mpad, int bpad) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	if (mpad>=0) data->mpad = mpad;
	if (bpad>=0) data->bpad = bpad;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_xtrapad(my1ipass_t* filter, int padx, int pady) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	if (padx>=0) data->padx = padx;
	if (pady>=0) data->pady = pady;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_minsize(my1ipass_t* filter, int hchk, int wchk) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	data->wchk = wchk;
	data->hchk = hchk;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_noborder(my1ipass_t* filter, int none) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	if (none) data->flag |= FLAG_AOI_NO_BORDER;
	else data->flag &= ~FLAG_AOI_NO_BORDER;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_aspect_none(my1ipass_t* filter) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	data->arow = 0;
	data->acol = 0;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf_aspect_ratio(my1ipass_t* filter, int arow, int acol) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	data->arow = arow;
	data->acol = acol;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_aoiblob(my1image_t* img, my1image_t* res,
		my1ipass_t* filter) {
	my1if_aoiblob_t* data = (my1if_aoiblob_t*) filter->data;
	my1image_scan_t* scan = &data->scan;
	data->flag &= ~FLAG_AOI_BLOB;
	/* find boundaries */
	ibox_init(&data->tbox);
	iscan_init(scan,img,data->bpad);
	iscan_prep(scan);
	while (iscan_next(scan)) {
		if (iscan_skip(scan)||!scan->data[scan->loop]) continue;
		/* update box */
		if (data->tbox.yt<0) data->tbox.yt = scan->irow;
		else if (data->tbox.yb<scan->irow) data->tbox.yb = scan->irow;
		if (data->tbox.xl<0||data->tbox.xl>scan->icol)
			data->tbox.xl = scan->icol;
		else if (data->tbox.xr<scan->icol)
			data->tbox.xr = scan->icol;
	}
	/* reset region, mark if valid */
	image_area_make(&filter->buffer->region,0,0,0,0);
	if (ibox_valid(&data->tbox)) {
		/* add margin */
		if (data->mpad)
			ibox_padding(&data->tbox,data->mpad);
		if (data->padx)
			ibox_padx(&data->tbox,data->padx);
		if (data->pady)
			ibox_pady(&data->tbox,data->pady);
		/* get size */
		data->rows = data->tbox.yb-data->tbox.yt+1;
		data->cols = data->tbox.xr-data->tbox.xl+1;
		/* option to ignore border view rule */
		if (data->flag&FLAG_AOI_NO_BORDER) {
			ibox_fill(&data->view,0,scan->cols-1,0,scan->rows-1);
			if (data->tbox.xl<data->view.xl) {
				data->cols -= data->view.xl - data->tbox.xl; /* less cols */
				data->tbox.xl = data->view.xl;
			}
			if (data->tbox.yt<data->view.yt) {
				data->rows -= data->view.yt - data->tbox.yt; /* less rows */
				data->tbox.yt = data->view.yt;
			}
			if (data->tbox.xr>data->view.xr) {
				data->cols -= data->tbox.xr - data->view.xr; /* less cols */
				data->tbox.xr = data->view.xr;
			}
			if (data->tbox.yb>data->view.yb) {
				data->rows -= data->tbox.yb - data->view.yb; /* less rows */
				data->tbox.yb = data->view.yb;
			}
		}
		else {
			/* set viewport border */
			ibox_fill(&data->view,scan->skip,scan->bcol,scan->skip,scan->brow);
		}
		/* validate box! */
		if (!ibox_within(&data->tbox,&data->view))
			data->rows = 0;
		/* check aspect ratio if requested */
		if (data->arow>0&&data->acol>0) {
			data->hrat = (float)data->rows/data->arow;
			data->wrat = (float)data->cols/data->acol;
			if (data->hrat>data->wrat) {
				/* increase data->cols */
				data->temp = data->hrat * data->acol;
				data->temp -= data->cols; /* difference we need to add */
				if (data->temp%2) data->temp++; /* make sure it is even */
				data->cols += data->temp;
				if (data->cols<scan->cols) {
					/* increase r/l borders */
					data->temp >>= 1;
					data->tbox.xl -= data->temp;
					data->tbox.xr += data->temp;
					if (data->tbox.xl<0) {
						data->tbox.xr -= data->tbox.xl;
						data->tbox.xl = 0;
					}
					else if (data->tbox.xr>=scan->cols) {
						data->temp = scan->cols-1;
						data->tbox.xl -= data->tbox.xr-data->temp;
						data->tbox.xr = data->temp;
					}
				}
				else data->cols = 0;
			}
			else {
				/* increase data->rows */
				data->temp = data->wrat * data->arow;
				data->temp -= data->rows;
				if (data->temp%2) data->temp++;
				data->rows += data->temp;
				if (data->rows<scan->rows) {
					/* increase t/b borders */
					data->temp >>= 1;
					data->tbox.yt -= data->temp;
					data->tbox.yb += data->temp;
					if (data->tbox.yt<0) {
						data->tbox.yb -= data->tbox.yt;
						data->tbox.yt = 0;
					}
					else if (data->tbox.yb>=scan->rows) {
						data->temp = scan->rows-1;
						data->tbox.yt -= data->tbox.yb-data->temp;
						data->tbox.yb = data->temp;
					}
				}
				else data->rows = 0;
			}
		}
		/* check minimum blob size to mark */
		if (data->rows<data->hchk) data->rows = 0;
		if (data->cols<data->wchk) data->cols = 0;
		/* mark if still valid */
		if (data->rows&&data->cols) {
			image_area_make(&filter->buffer->region,
				data->tbox.yt,data->tbox.xl,data->rows,data->cols);
			data->flag |= FLAG_AOI_BLOB;
		}
	}
	if (!(data->flag&FLAG_AOI_BLOB))
		ibox_init(&data->tbox); /* invalidate box! */
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_aoiblob_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_aoiblob_t));
	curr = (my1if_aoiblob_t*) filter->data;
	if (!pclone) {
		curr->flag = 0;
		curr->mpad = BLOB_PADDING_DEFAULT;
		curr->bpad = BORDER_WIDTH_DEFAULT;
		curr->padx = 0;
		curr->pady = 0;
		curr->wchk = curr->mpad << 2;
		curr->hchk = curr->mpad << 2;
		curr->arow = 0;
		curr->acol = 0;
	}
	else {
		pchk = (my1if_aoiblob_t*) pclone->data;
		curr->flag = pchk->flag;
		curr->mpad = pchk->mpad;
		curr->bpad = pchk->bpad;
		curr->padx = pchk->padx;
		curr->pady = pchk->pady;
		curr->wchk = pchk->wchk;
		curr->hchk = pchk->hchk;
		curr->arow = pchk->arow;
		curr->acol = pchk->acol;
	}
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_free(my1ipass_t* filter) {
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
void filter_aoiblob_conf(my1ipass_t* filter, char *pkey, char *pval) {
	my1if_aoiblob_t* data;
	char *pchk;
	int temp, tmp1;
	data = (my1if_aoiblob_t*) filter->data;
	if (!strncmp(pkey,"mpad",5)) {
		temp = atoi(pval);
		if (temp>=0) data->mpad = temp;
	}
	else if (!strncmp(pkey,"bpad",5)) {
		temp = atoi(pval);
		if (temp>=0) data->bpad = temp;
	}
	else if (!strncmp(pkey,"padx",5)) {
		temp = atoi(pval);
		if (temp>=0) data->padx = temp;
	}
	else if (!strncmp(pkey,"pady",5)) {
		temp = atoi(pval);
		if (temp>=0) data->pady = temp;
	}
	else if (!strncmp(pkey,"aspect",7)) {
		pchk = strchr(pval,(int)'/');
		if (pchk) {
			pchk[0] = 0x0;
			temp = atoi(pval);
			tmp1 = atoi(&pchk[1]);
			if (temp>0&&tmp1>0) {
				data->acol = temp;
				data->arow = tmp1;
			}
		}
	}
	else if (!strncmp(pkey,"border",7)) {
		if ((pval[0]==0x31||pval[0]==0x30)&&pval[1]==0x0) {
			if (pval[0]==0x31) data->flag &= ~FLAG_AOI_NO_BORDER;
			else data->flag |= FLAG_AOI_NO_BORDER;
		}
	}
	else if (!strncmp(pkey,"minsize",8)) {
		pchk = strchr(pval,(int)'x');
		if (pchk) {
			pchk[0] = 0x0;
			temp = atoi(pval);
			tmp1 = atoi(&pchk[1]);
			if (temp>=0&&tmp1>=0) {
				data->wchk = temp;
				data->hchk = tmp1;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_AOIBLOB_H__ */
/*----------------------------------------------------------------------------*/
