/*----------------------------------------------------------------------------*/
/**
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <string.h>
#include <libgen.h>
/*----------------------------------------------------------------------------*/
#include "shaped_clustering_file.h"
#include "vector_file.h"
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR (~(~0U>>1))
#define STAT_ERROR_ARGS (STAT_ERROR|0x01)
#define STAT_ERROR_LOAD (STAT_ERROR|0x02)
#define STAT_ERROR_WORK (STAT_ERROR|0x04)
#define STAT_ERROR_SAVE (STAT_ERROR|0x08)
/*----------------------------------------------------------------------------*/
#define FLAG_DO_VERBOSE 0x00000001
/*----------------------------------------------------------------------------*/
#define REAL_FORMAT ",%.12lf"
#define CLUSTERSIZE 2
#define CLUSTERDBNAME "shaped_clusters.db"
/*----------------------------------------------------------------------------*/
#define FLAG_SHOW_STEPS 0x0002
#define FLAG_SAVE_CLUST 0x0004
#define FLAG_RECALC_FFD 0x0008
#define FLAG_DO_CLUSTDB 0x0010
#define FLAG_SC_SHOW_ITEM 0x0100
#define FLAG_SC_SHOW_STAT 0x0200
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	unsigned int flag, stat; /* control/status bits */
	int loop, test; /* general purpose vars - used in data_args */
	int tmp1, tmp2; /* temp storage */
	char* ptmp; /* pointer to argv used in data_args */
	char *pdat, *pdis, *pcdb, *path;
	my1cstr_t name;
	my1vector_dist_t calc;
	my1shaped_db_t that; /* main db */
	my1shaped_db_t what; /* db for cluster fd */
	my1shaped_clustering_t work;
	my1shaped_dist_t *pick;
	my1shaped_pool_t *pool;
} data_t;
/*----------------------------------------------------------------------------*/
int data_args(data_t *data, int argc, char* argv[]) {
	/* get options */
	data->flag = 0; data->stat = 0; data->pcdb = CLUSTERDBNAME;
	data->tmp1 = CLUSTERSIZE; /* using tmp1 as cluster size */
	data->pdat = 0x0; data->pdis = 0x0; data->path = 0x0;
	vector_dist_default(&data->calc);
	for (data->loop=1;data->loop<argc;data->loop++) {
		if (argv[data->loop][0]=='-'&&argv[data->loop][1]=='-') {
			data->ptmp = &argv[data->loop][2];
			if (!strncmp(data->ptmp,"size",5)) {
				if (++data->loop<argc) {
					data->tmp1 = atoi(argv[data->loop]);
					if (data->tmp1<2) {
						fprintf(stderr,"** Invalid size! (%d)\n",data->tmp1);
						data->stat |= STAT_ERROR_ARGS;
					}
				}
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"dist-offs",10)) {
				if (++data->loop<argc)
					data->calc.offs = atoi(argv[data->loop]);
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"dist-stop",10)) {
				if (++data->loop<argc)
					data->calc.stop = atoi(argv[data->loop]);
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"dist-file",10)) {
				if (++data->loop<argc)
					data->pdis = argv[data->loop];
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"dbname",7)) {
				if (++data->loop<argc)
					data->pcdb = argv[data->loop];
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"dbpath",7)) {
				if (++data->loop<argc)
					data->path = argv[data->loop];
				else {
					fprintf(stderr,"** No value for '%s' given!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->ptmp,"stat",5))
				data->flag |= FLAG_SC_SHOW_STAT;
			else if (!strncmp(data->ptmp,"itemize",8))
				data->flag |= FLAG_SC_SHOW_ITEM;
			else if (!strncmp(data->ptmp,"verbose",8))
				data->flag |= FLAG_DO_VERBOSE;
			else if (!strncmp(data->ptmp,"steps",6))
				data->flag |= FLAG_SHOW_STEPS;
			else if (!strncmp(data->ptmp,"save",5))
				data->flag |= FLAG_SAVE_CLUST;
			else if (!strncmp(data->ptmp,"ffdcalc",8))
				data->flag |= FLAG_RECALC_FFD;
			else if (!strncmp(data->ptmp,"clustdb",8))
				data->flag |= FLAG_DO_CLUSTDB;
			else if (!strncmp(data->ptmp,"dist-avg",9)) {
				data->calc.calc = vector_distance_avg;
				fprintf(stderr,"@@ Using vector_distance_avg!\n");
			}
			else if (!strncmp(data->ptmp,"dist-rms",9)) {
				/* this is the default now! */
				data->calc.calc = vector_distance_rms;
				fprintf(stderr,"@@ Using vector_distance_rms!\n");
			}
			else {
				fprintf(stderr,"** Invalid option '%s'!\n",argv[data->loop]);
				data->stat |= STAT_ERROR_ARGS;
			}
		}
		else {
			/* non-parameter */
			if (!data->pdat) data->pdat = argv[data->loop];
			else {
				fprintf(stderr,"** Invalid argument '%s'!\n",argv[data->loop]);
				data->stat |= STAT_ERROR_ARGS;
			}
		}
	}
	/* check input */
	if (!data->pdat) {
		fprintf(stderr,"** No shape database file given!\n");
		data->stat |= STAT_ERROR_ARGS;
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
void data_post_init(void* temp) {
	FILE* pchk;
	data_t *data;
	my1item_t *item;
	my1shaped_link_t *link;
	my1shaped_clustering_t* that = (my1shaped_clustering_t*)temp;
	/* show info */
	printf("-- Clusters:{%d}\n",that->clus.size);
	printf("-- Shape count:{%d}\n",that->main.size);
	printf("-- Maximum distance:{%.12lf|%s:%s}\n",
		that->plnk->dist,that->pdst->data.name.buff,
		((my1shaped_dist_t*)that->plnk->from)->data.name.buff);
	that->loop = 1;
	that->item = that->clus.init;
	while (that->item) {
		that->pool = (my1shaped_pool_t*)that->item->data;
		that->pick = (my1shaped_dist_t*)that->pool->pool.init->data;
		printf("   Shape %d:{%p:%s}\n",that->loop,that->pool,
			that->pick->data.name.buff);
		that->item = that->item->next;
		that->loop++;
	}
	printf("-- Initial Size:{Temp=%d}\n",that->temp.size);
	shaped_clustering_show(that,stdout);
	fflush(stdout);
	data = (data_t*) that->gdat;
	if (data->pdis) {
		pchk = fopen(data->pdis,"w");
		if (!pchk) {
			printf("** Error opening file '%s' for writing!\n",data->pdis);
			fflush(stdout);
			return;
		}
		printf("@@ Writing distance matrix to '%s'!\n",data->pdis);
		fflush(stdout);
		/* get from main list - print header line */
		fprintf(pchk,"D-MAT");
		that->item = that->main.init;
		while (that->item) {
			that->cont = (my1shaped_t*) that->item->data;
			fprintf(pchk,",%s",that->cont->name.buff);
			that->item = that->item->next;
		}
		fprintf(pchk,"\n");
		/* print distance rows */
		that->item = that->main.init;
		while (that->item) {
			that->chk1 = (my1shaped_dist_t*) that->item->data;
			fprintf(pchk,"%s",that->chk1->data.name.buff);
			that->it3m = that->main.init;
			while (that->it3m) {
				if (that->it3m==that->item) {
					fprintf(pchk,",%.12lf",0.0);
				}
				else {
					that->chk2 = (my1shaped_dist_t*) that->it3m->data;
					item = shaped_dist_find_link(that->chk1,(void*)that->chk2);
					if (item) {
						link = (my1shaped_link_t*) item->data;
						fprintf(pchk,",%.12lf",link->dist);
					}
					else fprintf(pchk,",");
				}
				that->it3m = that->it3m->next;
			}
			that->item = that->item->next;
			fprintf(pchk,"\n");
		}
		fclose(pchk);
	}
}
/*----------------------------------------------------------------------------*/
void data_post_loop(void* temp) {
	my1shaped_clustering_t* that = (my1shaped_clustering_t*)temp;
	if (that->move) {
		printf("-- Re-clustering done. (%d|%d)\n",that->loop,that->move);
		shaped_clustering_show(that,stdout);
		fflush(stdout);
	}
}
/*----------------------------------------------------------------------------*/
int data_init(data_t *data) {
	/* initialize stuff */
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Initializing data...\n");
	cstr_init(&data->name);
	shaped_db_init(&data->that);
	shaped_db_init(&data->what);
	shaped_clustering_init(&data->work,&data->calc);
	data->work.kmeans_init_done = data_post_init;
	data->work.gdat = (void*) data;
	if (data->flag&FLAG_SHOW_STEPS)
		data->work.kmeans_loop_next = data_post_loop;
	if (data->flag&FLAG_SC_SHOW_ITEM)
		data->work.flag |= SC_FLAG_SHOW_ITEM;
	if (data->flag&FLAG_SC_SHOW_STAT)
		data->work.flag |= SC_FLAG_SHOW_STAT;
	else
		data->work.flag |= SC_FLAG_SHOW_LIST;
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_free(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Cleaning up data...\n");
	shaped_clustering_free(&data->work);
	shaped_db_free(&data->what);
	shaped_db_free(&data->that);
	cstr_free(&data->name);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_load(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Loading data...\n");
	if (shaped_db_load(&data->that,data->pdat)<0) {
		fprintf(stderr,"** Error loading shape db '%s'!\n",data->pdat);
		data->stat |= STAT_ERROR_LOAD;
	}
	else {
		shaped_clustering_data(&data->work,&data->that);
		if (data->work.main.size!=data->that.list.size) {
			fprintf(stderr,"** Error making cluster data! (%d/%d))\n",
				data->work.main.size,data->that.list.size);
			data->stat |= STAT_ERROR_LOAD;
		}
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_proc(data_t *data) {
	my1list_t *list;
	my1shaped_t *that;
	int temp;
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Processing data...\n");
	data->test = shaped_clustering_kmeans(&data->work,data->tmp1);
	if (data->test<0) {
		fprintf(stderr,"** Error clustering data! (%d)\n",data->test);
		data->stat |= STAT_ERROR_WORK;
	}
	else {
		printf("## Clustering done. (%d)\n",data->test);
		shaped_clustering_show(&data->work,stdout);
		temp = data->work.flag&SC_FLAG_SHOW_STAT&SC_FLAG_SHOW_LIST;
		data->work.flag &= ~SC_FLAG_SHOW_STAT;
		data->work.flag |= SC_FLAG_SHOW_LIST;
		fprintf(stderr,"## Clustering Completed! {Iterations:%d}\n",data->test);
		shaped_clustering_show(&data->work,stderr);
		fflush(stderr);
		if (temp&SC_FLAG_SHOW_STAT)
			data->work.flag |= SC_FLAG_SHOW_STAT;
		if (!(temp&SC_FLAG_SHOW_LIST))
			data->work.flag &= ~SC_FLAG_SHOW_LIST;
		if (data->flag&FLAG_SAVE_CLUST) {
			list = &data->work.clus;
			list->curr = 0x0;
			while (list_scan_item(list)) {
				that = (my1shaped_t*) list->curr->data; /* davg/shaped_pool */
				if(data->flag&FLAG_RECALC_FFD) {
					printf("-- Recalculating FD (cluster%d)\n",list->step);
					shaped_pool_average((my1shaped_pool_t*)list->curr->data);
				}
				cstr_resize(&that->name,32);
				data->loop =
					snprintf(that->name.buff,32,"cluster%d",list->step);
				that->name.fill = data->loop;
				if (shaped_db_append(&data->what,that))
					printf("-- Added cluster data! (%s)\n",that->name.buff);
				else {
					fprintf(stderr,"** Error adding cluster data! (%s)\n",
						that->name.buff);
					data->stat |= STAT_ERROR_WORK;
					break;
				}
			}
		}
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
void shaped_clustering_make_clusterdb(my1shaped_clustering_t* that,
		char *path) {
	my1shaped_pool_t *pool;
	my1shaped_db_t temp;
	my1cstr_t name;
	char buff[32];
	FILE* show;
	show = stdout;
	cstr_init(&name);
	that->clus.curr = 0x0;
	while (list_scan_item(&that->clus)) {
		snprintf(buff,32,"exe_cluster%d",that->clus.step);
		fprintf(show,"@@ Building db for %s... ",buff);
		if (path) {
			cstr_setstr(&name,path);
			cstr_append(&name,"/");
		}
		else cstr_null(&name);
		cstr_append(&name,buff);
		cstr_append(&name,".db");
		shaped_db_init(&temp);
		pool = (my1shaped_pool_t*)that->clus.curr->data;
		/** NEED TO SORT THIS POOL! */
		fprintf(show,"sorting... ");
		shaped_pool_sort(pool);
		fprintf(show,"picking... ");
		pool->pool.curr = 0x0;
		while (list_scan_item(&pool->pool)) {
			that->pick = (my1shaped_dist_t*)pool->pool.curr->data;
			shaped_db_append(&temp,&that->pick->data);
		}
		fprintf(show,"done.\n");
		if (shaped_db_save(&temp,name.buff))
			fprintf(show,"** Error saving '%s'\n",name.buff);
		else
			fprintf(show,"@@ Saved '%s'\n",name.buff);
		shaped_db_free(&temp);
	}
	cstr_free(&name);
}
/*----------------------------------------------------------------------------*/
int data_save(data_t *data) {
	my1cstr_t name;
	cstr_init(&name);
	if (data->flag&FLAG_SAVE_CLUST) {
		if (data->path) {
			cstr_setstr(&name,data->path);
			cstr_append(&name,"/");
		}
		cstr_append(&name,data->pcdb);
		printf("-- Saving (%d) %s... ",data->what.list.size,name.buff);
		if (shaped_db_save(&data->what,name.buff)) {
			printf("** error?! **\n");
			data->stat |= STAT_ERROR_SAVE;
		}
		else printf("done.\n");
	}
	cstr_free(&name);
	if (data->flag&FLAG_DO_CLUSTDB)
		shaped_clustering_make_clusterdb(&data->work,data->path);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	data_args(&data,argc,argv);
	if (!data.stat) {
		do {
			if (data_init(&data)) break;
			if (data_load(&data)) break;
			if (data_proc(&data)) break;
			if (data_save(&data)) break;
		} while (0);
		data_free(&data);
	}
	return data.stat;
}
/*----------------------------------------------------------------------------*/
