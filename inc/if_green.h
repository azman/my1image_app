/*----------------------------------------------------------------------------*/
/*
	if_green.h
	- image_filter to find green blob in an image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_GREEN_H__
#define __MY1IF_GREEN_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_GREEN "if_green"
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
#define DIFF_COLOR_CHECK 10
#define THRESH_COLOR_MANGO 245
/*----------------------------------------------------------------------------*/
int pick_green(int *that) {
	my1rgb_t* rgb = (my1rgb_t*) that;
	if ((rgb->g<THRESH_COLOR_MANGO)&&(rgb->g>rgb->b)&&
		((rgb->g-rgb->r)>DIFF_COLOR_CHECK)) return *that;
	else return 0;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_green(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	if (img->mask==IMASK_COLOR) {
		my1image_scan_t scan;
		image_make(res,img->rows,img->cols);
		iscan_init(&scan,img,BORDER_WIDTH);
		iscan_prep(&scan);
		while (iscan_next(&scan)) {
			if (iscan_skip(&scan)) {
				res->data[scan.loop] = 0;
				continue;
			}
			res->data[scan.loop] = pick_green(&img->data[scan.loop]);
		}
		res->mask = img->mask;
	}
	else image_copy(res,img);
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_GREEN_H__ */
/*----------------------------------------------------------------------------*/
