/*----------------------------------------------------------------------------*/
/*
	if_points.h
	- image_filter to detect contour
	- input should be contour edge image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_POINTS_H__
#define __MY1IF_POINTS_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
#include "my1image_ifdb.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_POINTS "if_points"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "common.h"
#include "points.h"
/*----------------------------------------------------------------------------*/
#define POINTS_FOUND 0x10000
#define POINTS_NEW 0x80000
#define POINTS_FLAGS (POINTS_FOUND|POINTS_NEW)
/*----------------------------------------------------------------------------*/
typedef struct _my1if_points_t {
	unsigned int flag, stat;
	int xchk, ychk, zchk, rsv0;
	my1image_buffer_t buff;
	my1moment_t imom;
	my1cclist_t that;
} my1if_points_t;
/*----------------------------------------------------------------------------*/
unsigned int filter_points_stat(my1ipass_t* filter) {
	my1if_points_t* data = (my1if_points_t*) filter->data;
	return data->stat;
}
/*----------------------------------------------------------------------------*/
my1cclist_t* filter_points_list(my1ipass_t* filter) {
	my1if_points_t* data = (my1if_points_t*) filter->data;
	return &data->that;
}
/*----------------------------------------------------------------------------*/
void filter_points_conf_pmin(my1ipass_t* filter, int pmin) {
	my1if_points_t* data = (my1if_points_t*) filter->data;
	data->that.pmin = pmin;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_points(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	my1image_area_t *area = &pass->buffer->region;
	my1if_points_t* data = (my1if_points_t*) pass->data;
	unsigned int prev, curr;
	int xchk, ychk, zchk;
	prev = data->stat&POINTS_FOUND;
	data->stat &= ~POINTS_FLAGS;
	if (area->hval&&area->wval) {
		buffer_size_all(&data->buff,area->hval,area->wval);
		image_get_area(img,data->buff.curr,area);
		if (img->mask)
			image_grayscale(data->buff.curr);
		points_scan_contour((my1ivec_t*)data->buff.curr,
			(my1ivec_t*)data->buff.next,&data->that);
		if (data->that.lmax) {
			ivec_val1((my1ivec_t*)data->buff.next,WHITE);
			buffer_swap(&data->buff);
			if (img->mask)
				image_colormode(data->buff.curr);
			image_set_area(res,data->buff.curr,area);
			xchk = data->that.lmax->xmax-data->that.lmax->xmin;
			ychk = data->that.lmax->ymax-data->that.lmax->ymin;
			zchk = data->that.lmax->fill;
			data->stat |= POINTS_FOUND;
		}
	}
	curr = data->stat&POINTS_FOUND;
	if (curr) {
		if (data->zchk>0) { /* if points previous found */
			if (zchk!=data->zchk||ychk!=data->ychk||xchk!=data->xchk) {
				/* must be new? */
				prev = 0;
			}
		}
		if (curr!=prev) {
			data->stat |= POINTS_NEW;
			/**printf("-- Points found!\n");*/
		}
		/* keep info on prev */
		data->xchk = xchk;
		data->ychk = ychk;
		data->zchk = zchk;
	}
	else data->zchk = 0; /* no points found in image */
	/* update buffer flags */
	pass->buffer->flag &= ~POINTS_FLAGS;
	pass->buffer->flag |= (data->stat&POINTS_FLAGS);
	return img; /* pass through */
}
/*----------------------------------------------------------------------------*/
void filter_points_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_points_t* curr;
	filter->data = malloc(sizeof(my1if_points_t));
	curr = (my1if_points_t*) filter->data;
	curr->flag = 0;
	curr->stat = 0;
	curr->xchk = 0;
	curr->ychk = 0;
	curr->zchk = 0;
	/*curr->rsv0 = 0;*/
	buffer_init(&curr->buff);
	cclist_init(&curr->that);
	curr->that.pmin = MINIMUM_POINTS;
}
/*----------------------------------------------------------------------------*/
void filter_points_free(my1ipass_t* filter) {
	my1if_points_t* temp = (my1if_points_t*) filter->data;
	if (temp) {
		cclist_free(&temp->that);
		buffer_free(&temp->buff);
		free((void*)filter->data);
	}
}
/*----------------------------------------------------------------------------*/
void filter_points_conf(my1ipass_t* filter, char *pkey, char *pval) {
	my1if_points_t* data;
	int test;
	data = (my1if_points_t*) filter->data;
	if (!strncmp(pkey,"pmin",5)) {
		test = atoi(pval);
		if (test>=0) data->that.pmin = test;
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_POINTS_H__ */
/*----------------------------------------------------------------------------*/
