/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_DIST_H__
#define __MY1VECTOR_DIST_H__
/*----------------------------------------------------------------------------*/
/**
 * vector_dist.h
 * - vector extension for distance calculation
**/
/*----------------------------------------------------------------------------*/
#include "vector.h"
/*----------------------------------------------------------------------------*/
typedef double (*vector_distcalc)(my1vector_t*,my1vector_t*,int,int);
/*----------------------------------------------------------------------------*/
typedef struct  _my1vector_dist_t {
	vector_distcalc calc;
	int offs, stop;
} my1vector_dist_t;
/*----------------------------------------------------------------------------*/
#define vector_distance vector_distance_rms
/*----------------------------------------------------------------------------*/
/* NOT REALLY USEFUL? MAY REMOVE THIS! */
double vector_distance_2d(my1vector_t* vec1, my1vector_t* vec2,
		int offs, int stop) {
	double dsum, keep;
	my1complex_t temp;
	dsum = 0.0;
	if (stop<0) stop = vec1->fill>>1; /* half length */
	else if (!stop||stop>vec1->fill) stop = vec1->fill;
	for (;offs<stop;offs++) {
		/* diff */
		temp.real = vec1->data[offs].real-vec2->data[offs].real;
		temp.imag = vec1->data[offs].imag-vec2->data[offs].imag;
		/* squared */
		keep = temp.real;
		temp.real = (keep*keep)-(temp.imag*temp.imag);
		temp.imag = temp.imag*keep*2;
		/* now, get magnitude */
		dsum += complex_magnitude(&temp);
	}
	return sqrt(dsum);
}
/*----------------------------------------------------------------------------*/
double vector_distance_rms(my1vector_t* vec1, my1vector_t* vec2,
		int offs, int stop) {
	double dsum, diff;
	dsum = 0.0;
	if (stop<0) stop = vec1->fill>>1;
	else if (!stop||stop>vec1->fill) stop = vec1->fill;
	for (;offs<stop;offs++) {
		diff = fabs(complex_magnitude(&vec1->data[offs]) -
			complex_magnitude(&vec2->data[offs]));
		dsum += (diff * diff);
	}
	return sqrt(dsum);
}
/*----------------------------------------------------------------------------*/
double vector_distance_avg(my1vector_t* vec1, my1vector_t* vec2,
		int offs, int stop) {
	double dsum, diff;
	dsum = 0.0;
	if (stop<0) stop = vec1->fill>>1;
	else if (!stop||stop>vec1->fill) stop = vec1->fill;
	for (;offs<stop;offs++) {
		diff = fabs(complex_magnitude(&vec1->data[offs]) -
			complex_magnitude(&vec2->data[offs]));
		dsum += diff;
	}
	return dsum/offs; /* assume vec1->fill>0 */
}
/*----------------------------------------------------------------------------*/
void vector_dist_default(my1vector_dist_t* dist) {
	dist->calc = vector_distance;
	/* by default use all terms */
	dist->offs = 0;
	dist->stop = 0;
}
/*----------------------------------------------------------------------------*/
void vector_dist_prep(my1vector_dist_t* dist, vector_distcalc calc,
		int offs, int stop) {
	dist->calc = calc;
	dist->offs = offs;
	dist->stop = stop;
}
/*----------------------------------------------------------------------------*/
double vector_dist_calc(my1vector_dist_t* dist,
		my1vector_t* vec1, my1vector_t* vec2) {
	return dist->calc(vec1,vec2,dist->offs,dist->stop);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_DIST_H__ */
/*----------------------------------------------------------------------------*/
