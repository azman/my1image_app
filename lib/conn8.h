/*----------------------------------------------------------------------------*/
#ifndef __MY1CONN8_H__
#define __MY1CONN8_H__
/*----------------------------------------------------------------------------*/
#include "ivec.h"
#include "ivec_scan.h"
/*----------------------------------------------------------------------------*/
void conn8_scan(my1ivec_t *ivec, my1ivec_t *from, int filter) {
	int temp, mask;
	my1ivec_scan_t scan;
	mask = filter;
	if (mask<0) mask = -mask;
	ivec_make(ivec,from->rows,from->cols);
	scan_init(&scan,from->data,from->rows,from->cols,1);
	scan_prep(&scan);
	while (scan_next(&scan)) {
		if (scan_skip(&scan)) temp = 0;
		else {
			temp = scan_8connected(&scan);
			if (mask) {
				if (filter<0) { if (mask<temp) temp = 0; }
				else { if (temp<mask) temp = 0; }
			}
		}
		ivec->data[scan.loop] = temp;
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
