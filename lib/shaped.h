/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_H__
#define __MY1SHAPED_H__
/*----------------------------------------------------------------------------*/
/**
 * shaped.h
 * - basic structure for named shape
 * - vector contains descriptor values
**/
/*----------------------------------------------------------------------------*/
#include "vector.h"
#include "cstr.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_t {
	my1vector_t desc; /* shape descriptor */
	my1cstr_t name;
} my1shaped_t;
/*----------------------------------------------------------------------------*/
#define SVEC(that) ((my1vector_t*)that)
/*----------------------------------------------------------------------------*/
void shaped_init(my1shaped_t* cont) {
	vector_init(&cont->desc);
	cstr_init(&cont->name);
}
/*----------------------------------------------------------------------------*/
void shaped_free(my1shaped_t* cont) {
	cstr_free(&cont->name);
	vector_free(&cont->desc);
}
/*----------------------------------------------------------------------------*/
int shaped_make(my1shaped_t* cont, char* name, int size) {
	int test = 0;
	vector_make(SVEC(cont),size);
	if (cont->desc.size==size)
		test = size;
	cstr_assign(&cont->name,name);
	return test;
}
/*----------------------------------------------------------------------------*/
int shaped_copy(my1shaped_t* cont, my1shaped_t* from) {
	vector_copy(SVEC(cont),SVEC(from));
	if (cont->desc.fill!=from->desc.fill)
		return 0;
	cstr_assign(&cont->name,from->name.buff);
	return cont->desc.fill;
}
/*----------------------------------------------------------------------------*/
int shaped_copy_some(my1shaped_t* cont, my1shaped_t* from, int skip, int size) {
	vector_copy_some(SVEC(cont),SVEC(from),skip,size);
	if (cont->desc.fill!=size)
		return 0;
	cstr_assign(&cont->name,from->name.buff);
	return cont->desc.fill;
}
/*----------------------------------------------------------------------------*/
int shaped_copy_band(my1shaped_t* cont, my1shaped_t* from, int size) {
	vector_copy_band(SVEC(cont),SVEC(from),size);
	if (cont->desc.fill!=size)
		return 0;
	cstr_assign(&cont->name,from->name.buff);
	return cont->desc.fill;
}
/*----------------------------------------------------------------------------*/
int shaped_copy_append(my1shaped_t* cont, my1shaped_t* from) {
	vector_copy_append(SVEC(cont),SVEC(from));
	cstr_assign(&cont->name,from->name.buff);
	return cont->desc.fill;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_H__ */
/*----------------------------------------------------------------------------*/
