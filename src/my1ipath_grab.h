/*----------------------------------------------------------------------------*/
/**
 * my1ipath_grab - by azman@my1matrix.net
 *   - conveyor simulator library
 *   - using codes from my1video_csim (from my own my1video_app project)
 *   - rewritten to be a grabber for my1image_grab
**/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IPATH_GRAB_H__
#define __MY1IPATH_GRAB_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1list.h"
#include "my1path.h"
#include "my1thread.h"
/*----------------------------------------------------------------------------*/
#define DEFAULT_COLSKIP 10
#define DIFF_COLSKIP 5
/*----------------------------------------------------------------------------*/
#define MAX_WIDTH 640
#define MAX_HEIGHT 480
/*----------------------------------------------------------------------------*/
#define PGRAB_FLAG_OK 0
#define PGRAB_FLAG_INIT 0x01
#define PGRAB_FLAG_ERROR (~(~0U>>1))
#define PGRAB_FLAG_PATH_ERROR (PGRAB_FLAG_ERROR|0x010)
#define PGRAB_FLAG_SIZE_ERROR (PGRAB_FLAG_ERROR|0x020)
#define PGRAB_TCHK_NONE 0x00
#define PGRAB_TCHK_1R 0x01
#define PGRAB_TCHK_ENDS 0x10
/*----------------------------------------------------------------------------*/
typedef struct _my1ipath_grab_t {
	my1image_t *wait, *next, *show, view;
	my1image_buffer_t work;
	my1list_t full;
	my1path_t path;
	my1thread_t loader;
	int maxrow, maxcol;
	int donext; /* sliding index in next image */
	int dostep; /* sliding step size */
	unsigned int flag; /* not ready if non-zero */
	unsigned int tchk; /* process flag */
} my1ipath_grab_t;
/*----------------------------------------------------------------------------*/
typedef my1ipath_grab_t my1pgrab_t;
/*----------------------------------------------------------------------------*/
#define path_grab_noends(pg) ((my1pgrab_t*)pg)->tchk &= ~PGRAB_TCHK_ENDS
#define path_grab_1round(pg) ((my1pgrab_t*)pg)->tchk |= PGRAB_TCHK_1R
/*----------------------------------------------------------------------------*/
void path_grab_init(my1ipath_grab_t* grab);
void path_grab_free(my1ipath_grab_t* grab);
void path_grab_next(my1ipath_grab_t* grab);
void path_grab_path(my1ipath_grab_t* grab, char* path);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IPATH_GRAB_H__ */
/*----------------------------------------------------------------------------*/
