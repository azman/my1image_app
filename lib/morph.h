/*----------------------------------------------------------------------------*/
#ifndef __MY1MORPH_H__
#define __MY1MORPH_H__
/*----------------------------------------------------------------------------*/
#include "ivec.h"
#include "ivec_scan.h"
/*----------------------------------------------------------------------------*/
int morph_is_fit(my1ivec_scan_t* scan,  int *elem) {
	int loop, step;
	int *ele1 = &elem[4];
	int *ele2 = &elem[6];
	for (loop=0,step=scan->icol-1;loop<3;loop++,step++) {
		if (elem[loop]&&!scan->prev[step]) return 0;
		if (ele1[loop]&&!scan->curr[step]) return 0;
		if (ele2[loop]&&!scan->next[step]) return 0;
	}
	return 1; /* fit */
}
/*----------------------------------------------------------------------------*/
int morph_is_hit(my1ivec_scan_t* scan,  int *elem) {
	int loop, step;
	int *ele1 = &elem[4];
	int *ele2 = &elem[6];
	for (loop=0,step=scan->icol-1;loop<3;loop++,step++) {
		if (elem[loop]&&scan->prev[step]) return 1;
		if (ele1[loop]&&scan->curr[step]) return 1;
		if (ele2[loop]&&scan->next[step]) return 1;
	}
	return 0; /* no hit */
}
/*----------------------------------------------------------------------------*/
/* elem is always assumed 9-element structure (3x3) */
/*----------------------------------------------------------------------------*/
void morph_erode(my1ivec_t *ivec, my1ivec_t *dest, int* elem) {
	my1ivec_scan_t scan;
	ivec_make(dest,ivec->rows,ivec->cols);
	scan_init(&scan,ivec->data,ivec->rows,ivec->cols,1);
	scan_prep(&scan);
	while (scan_next(&scan)) {
		if (scan_skip(&scan)) dest->data[scan.loop] = 0;
		else dest->data[scan.loop] = morph_is_fit(&scan,elem);
	}
}
/*----------------------------------------------------------------------------*/
void morph_dilate(my1ivec_t *ivec, my1ivec_t *dest, int* elem) {
	my1ivec_scan_t scan;
	ivec_make(dest,ivec->rows,ivec->cols);
	scan_init(&scan,ivec->data,ivec->rows,ivec->cols,1);
	scan_prep(&scan);
	while (scan_next(&scan)) {
		if (scan_skip(&scan)) dest->data[scan.loop] = 0;
		else dest->data[scan.loop] = morph_is_hit(&scan,elem);
	}
}
/*----------------------------------------------------------------------------*/
void morph_opening(my1ivec_t *ivec, my1ivec_t *dest, int* elem) {
	my1ivec_t chk;
	ivec_init(&chk);
	morph_erode(ivec,&chk,elem);
	morph_dilate(&chk,dest,elem);
	ivec_free(&chk);
}
/*----------------------------------------------------------------------------*/
void morph_closing(my1ivec_t *ivec, my1ivec_t *dest, int* elem) {
	my1ivec_t chk;
	ivec_init(&chk);
	morph_dilate(ivec,&chk,elem);
	morph_erode(&chk,dest,elem);
	ivec_free(&chk);
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
