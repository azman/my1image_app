/*----------------------------------------------------------------------------*/
/*
	if_aoimake.h
	- image_filter to explicitly set area-of-interest (buffer defined region)
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_AOIMAKE_H__
#define __MY1IF_AOIMAKE_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_AOIMAKE "if_aoimake"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
typedef struct _my1if_aoimake_t {
	my1image_area_t make;
} my1if_aoimake_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_aoimake(my1image_t* img, my1image_t* res,
		my1ipass_t* filter) {
	my1if_aoimake_t* data = (my1if_aoimake_t*) filter->data;
	filter->buffer->region = data->make;
	if (!filter->buffer->region.wval)
		filter->buffer->region.wval = img->cols;
	if (!filter->buffer->region.hval)
		filter->buffer->region.hval = img->rows;
	/* no need write new image! */
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_aoimake_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_aoimake_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_aoimake_t));
	curr = (my1if_aoimake_t*) filter->data;
	if (!pclone)
		image_area_make(&curr->make,0,0,0,0);
	else {
		pchk = (my1if_aoimake_t*) pclone->data;
		curr->make = pchk->make;
	}
}
/*----------------------------------------------------------------------------*/
void filter_aoimake_free(my1ipass_t* filter) {
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_AOIMAKE_H__ */
/*----------------------------------------------------------------------------*/
