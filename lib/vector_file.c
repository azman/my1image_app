/*----------------------------------------------------------------------------*/
/**
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "vector_file.h"
/*----------------------------------------------------------------------------*/
int file_exists(char* name) {
	FILE* test = fopen(name,"r");
	if (test) fclose(test);
	return test ? 1:0;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, temp;
	double dval, imag;
	my1vector_t inp, pro, *pick, *next, *swap;
	vector_init(&inp);
	vector_init(&pro);
	stat = 0;
	do {
		if (argc<2)
		{
			printf("** No input file!\n");
			stat = -1;
			break;
		}
		if (strncmp(argv[1],"--new",6))
		{
			printf("## Loading vector from '%s'... ",argv[1]);
			if (vector_load(&inp,argv[1])&&vector_load_bin(&inp,argv[1]))
			{
				printf("failed!\n");
				stat = -2;
				break;
			}
			printf("done. [%d/%d]\n",inp.fill,inp.size);
		}
		pick = &inp; next = &pro;
		for (loop=2;loop<argc;loop++)
		{
			if (!strncmp(argv[loop],"--make",7))
			{
				if (++loop<argc)
				{
					temp = atoi(argv[loop]);
					if (temp>0)
					{
						printf("## Making vector (%d->%d)... ",pick->size,temp);
						vector_make(pick,temp);
						printf("done. (%d)\n",pick->size);
					}
				}
				else
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
				}
			}
			else if (!strncmp(argv[loop],"--save",7))
			{
				if (++loop<argc)
				{
					printf("## Saving vector to '%s'... ",argv[loop]);
					temp = vector_save(pick,argv[loop]);
					if (temp)
					{
						printf("error?! (%d)\n",temp);
						stat = -3;
					}
					else printf("done.\n");
				}
				else
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
				}
			}
			else if (!strncmp(argv[loop],"--bsave",8))
			{
				if (++loop<argc)
				{
					printf("## Saving vector (bin) to '%s'... ",argv[loop]);
					temp = vector_save_bin(pick,argv[loop]);
					if (temp)
					{
						printf("error?! (%d)\n",temp);
						stat = -3;
					}
					else printf("done.\n");

				}
				else
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
				}
			}
			else if (!strncmp(argv[loop],"--add",6))
			{
				if (++loop<argc)
				{
					printf("## Adding vector from '%s'... ",argv[loop]);
					if (vector_load(next,argv[loop])&&
						vector_load_bin(next,argv[loop]))
					{
						printf("failed!\n");
						stat = -3;
						continue;
					}
					if (next->fill!=pick->fill)
					{
						printf("size mismatched! [%d|%d]\n",
							next->fill,pick->fill);
						stat = -3;
						continue;
					}
					vector_summation(pick,next);
					printf("done. [%d/%d]\n",next->fill,next->size);
				}
				else
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
				}
			}
			else if (!strncmp(argv[loop],"--append",9))
			{
				if (++loop>=argc)
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
					continue;
				}
				dval = atof(argv[loop]);
				if (++loop>=argc)
				{
					printf("** No value for '%s' given!\n",argv[loop-2]);
					stat = -1;
					continue;
				}
				imag = atof(argv[loop]);
				printf("## Append value to vector {%lf,%lf}\n",dval,imag);
				vector_append(pick,dval,imag);
			}
			else if (!strncmp(argv[loop],"--scale",8))
			{
				if (++loop<argc)
				{
					dval = atof(argv[loop]);
					printf("## Scaling vector by {%lf}\n",dval);
					vector_scale(pick,dval);
				}
				else
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
				}
			}
			else if (!strncmp(argv[loop],"--fill",7))
			{
				printf("## Filling vector with zeroes (%d->%d)... ",
					pick->fill,pick->size);
				vector_zero_pad(pick);
				printf("done. (%d)\n",pick->fill);
			}
			else if (!strncmp(argv[loop],"--magnitude",12))
			{
				printf("## Taking vector magnitude.\n");
				vector_magnitude(pick);
			}
			else if (!strncmp(argv[loop],"--average",10))
			{
				printf("## Taking vector average.\n");
				vector_average(pick);
			}
			else if (!strncmp(argv[loop],"--fft",6))
			{
				temp = pick->fill;
				if (temp&(temp-1))
				{
					printf("## Padding (%d->",temp);
					/** need to pad to nearest 2^n */
					if (temp&0x7fff0000) temp = 0x10000;
					else temp=1;
					while (temp<pick->fill)
						temp <<= 1; /* get next power-of-2 */
					printf("%d)... ",temp);
					vector_make(pick,temp);
					vector_fill(pick,0.0,0.0);
				}
				printf("## Running FFT!\n");
				vector_fft(pick,next);
				swap = pick;
				pick = next;
				next = swap;
			}
			else if (!strncmp(argv[loop],"--ifft",7))
			{
				temp = pick->fill;
				if (temp&(temp-1))
				{
					printf("## Padding (%d->",temp);
					/** need to pad to nearest 2^n */
					if (temp&0x7fff0000) temp = 0x10000;
					else temp=1;
					while (temp<pick->fill)
						temp <<= 1; /* get next power-of-2 */
					printf("%d)... ",temp);
					vector_make(pick,temp);
					vector_fill(pick,0.0,0.0);
				}
				printf("## Running IFFT!\n");
				vector_fft_inv(pick,next);
				swap = pick;
				pick = next;
				next = swap;
			}
			else if (!strncmp(argv[loop],"--dft",6))
			{
				printf("## Running DFT!\n");
				vector_make(next,pick->fill);
				vector_dft(pick,next);
				swap = pick;
				pick = next;
				next = swap;
			}
			else if (!strncmp(argv[loop],"--idft",7))
			{
				printf("## Running IDFT!\n");
				vector_make(next,pick->fill);
				vector_dft_inv(pick,next);
				swap = pick;
				pick = next;
				next = swap;
			}
			else if (!strncmp(argv[loop],"--ores",7))
			{
				if (++loop>=argc)
				{
					printf("** No value for '%s' given!\n",argv[loop-1]);
					stat = -1;
					continue;
				}
				temp = atoi(argv[loop]);
				if (temp>0)
				{
					vector_ostr_resolution(temp);
					printf("## Output resolution is now %d!\n",temp);
				}
			}
			else
			{
				printf("** Invalid option '%s'?\n",argv[loop]);
				stat = -1;
			}
		}
		if (pick->fill)
			vector_ostr(pick,stderr);
	}
	while (0);
	vector_free(&pro);
	vector_free(&inp);
	return stat;
}
/*----------------------------------------------------------------------------*/
