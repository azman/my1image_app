/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1image_base.h"
#include "my1image_mask.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int *temp;
	my1image_t data;
	my1image_mask_t mask;
	/* initialize stuff */
	image_init(&data);
	/* do stuff here */
	image_mask_init(&mask,3);
	image_mask_prep(&mask);
	printf("[INIT] Scanning mask\n");
	while ((temp=image_mask_scan(&mask))) {
		printf("-- R:%-2d,C:%-2d,S:%d,P:%p\n",
			mask.orow,mask.ocol,mask.step,temp);
	}
	printf("[DONE] Scanning mask\n");
	/* cleanup stuff */
	image_free(&data);
	/* done! */
	return 0;
}
/*----------------------------------------------------------------------------*/
