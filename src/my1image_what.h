/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_WHAT_H__
#define __MY1IMAGE_WHAT_H__
/*----------------------------------------------------------------------------*/
/**
 * my1image_what : A generic data structure for my1image_main
 * - replaces my1image_data in my1imgpro library
 * - also provides event handlers
 **/
/*----------------------------------------------------------------------------*/
#include "my1image_main.h"
#include "my1libav_grab.h"
#include "my1ipath_grab.h"
#include "my1image_ifdb.h"
/*----------------------------------------------------------------------------*/
#define WFLAG_SHOW_ORIGINAL 0x01
#define WFLAG_VIDEO_MODE 0x10
#define WFLAG_VIDEO_LIVE (WFLAG_VIDEO_MODE|0x20)
#define WFLAG_IMAGE_PATH 0x40
/*----------------------------------------------------------------------------*/
typedef struct _my1iwhat_t {
	my1libav_grab_t avgrab;
	my1ipath_grab_t ptgrab;
	my1image_appw_t awin;
	my1image_t buff, *orig;
	my1ifdb_t ifdb;
	/* extra work tasks - init should be set BETWEEN iwhat_main & imain_init */
	my1itask_t init, free, prep, post;
	void* xtra; /* xtra stuff */
	/* list for requested user filter (csv) */
	char *list, *load;
	/* text to place on image */
	char *text;
	int trow, tcol;
	int txsz, trgb;
	/* what flag&stat */
	unsigned int flag, stat;
} my1iwhat_t;
/*----------------------------------------------------------------------------*/
#define what_text_ypos(pw,yp) ((my1iwhat_t*)pw)->trow = yp
#define what_text_xpos(pw,xp) ((my1iwhat_t*)pw)->tcol = xp
#define what_text_size(pw,is) ((my1iwhat_t*)pw)->txsz = is
#define what_text_make(pw,vv,xp,yp) \
	{ what_text_xpos(pw,xp); what_text_ypos(pw,yp); what_text_show(pw,vv); }
#define what_text_color(pw,br,bg,bb) \
	((my1iwhat_t*)pw)->trgb = (br<<16)|(bg<<8)|bb
/*----------------------------------------------------------------------------*/
int init_what(void* data, void* that, void* xtra);
int free_what(void* data, void* that, void* xtra);
int args_what(void* data, void* that, void* xtra);
int prep_what(void* data, void* that, void* xtra);
int exec_what(void* data, void* that, void* xtra);
int show_what(void* data, void* that, void* xtra);
/*----------------------------------------------------------------------------*/
/* helper function to setup my1iwhat_t - call this BEFORE imain_init */
void iwhat_main(my1iwhat_t* what, my1imain_t* that);
/* helper function to setup text display */
void what_text_show(my1iwhat_t* what, char* text);
/* helper function to setup filter list in image_main */
my1ipass_t* imain_pass(my1imain_t* idat, char *name);
/* helper function to setup filter for processing */
my1ipass_t* prep_pass(my1imain_t *mdat, char *name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_WHAT_H__ */
/*----------------------------------------------------------------------------*/
