/*----------------------------------------------------------------------------*/
#include "ivec_file.h"
#include "points.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, test;
	my1ivec_t load, buff, *ivec;
	my1cclist_t list;
	ivec_init(&load);
	ivec_init(&buff);
	cclist_init(&list);
	do {
		stat = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		for (loop=2;loop<argc;loop++) {
			if (!strncmp(argv[loop],"--pmin",6))
				list.pmin = atoi(argv[++loop]);
			else if (!strncmp(argv[loop],"--max-only",11)) {
				list.draw &= ~CCLIST_DRAW_ENB;
				list.draw |= CCLIST_DRAW_MAX;
			}
			else printf("** Unknown param '%s'!\n",argv[loop]);
		}
		test = ivec_load(&load,argv[1]);
		if (!test) {
			printf("** Cannot load '%s'!\n",argv[1]);
			stat = -2;
			break;
		}
		ivec = &load;
		printf("-- Load:\n");
		ivec_show(ivec);
		printf("-- Looking for loop points... ");
		points_scan_contour(ivec,&buff,&list);
		printf("done. (%d:%x)\n",list.size,list.draw);
		if (list.size>0) {
			ivec = &buff;
			printf("-- Found:%d (Min:%d)\n",list.size,list.pmin);
			printf("-- Draw: ");
			switch (list.draw&CCLIST_DRAW_ENB) {
				case CCLIST_DRAW_ALL: printf("ALL\n"); break;
				case CCLIST_DRAW_MAX: printf("MAX\n"); break;
				default: printf("None\n"); break;
			}
			for (loop=0;loop<ivec->size;loop++) {
				if (ivec->data[loop]) {
					ivec->data[loop] &= DIR_MASK;
					ivec->data[loop]++; /* viewable data */
				}
			}
		}
		printf("-- Data:\n");
		ivec_show(ivec);
	} while (0);
	cclist_free(&list);
	ivec_free(&buff);
	ivec_free(&load);
	return stat;
}
/*----------------------------------------------------------------------------*/
