/*----------------------------------------------------------------------------*/
#ifndef __MY1CCL_RUN_H__
#define __MY1CCL_RUN_H__
/*----------------------------------------------------------------------------*/
#include "ccl_stat.h"
/*----------------------------------------------------------------------------*/
void mark_run(my1ccl_t* pccl, int curr, int size, int mark) {
	int loop;
	for (loop=0;loop<size;loop++) {
		pccl->curr[curr++] = mark;
		ccl_more(pccl,mark);
	}
}
/*----------------------------------------------------------------------------*/
int find_run_mark(my1ccl_t* pccl, int test, int mark) {
	if (test) {
		if (!mark||test<mark)
			mark = test;
		else if (mark<test)
			pccl->stat[test].mark = mark;
	}
	return mark;
}
/*----------------------------------------------------------------------------*/
int find_run(my1ccl_t* pccl, int curr, int *mark) {
	int size, temp, chk1, chk2;
	size = 0; temp = 0;
	/* should not find non-zero other than 1? */
	while (pccl->curr[curr]==1) {
		if (!size) {
			chk1 = pccl->temp[curr-1];
			chk2 = pccl->temp[curr];
			temp = find_run_mark(pccl,chk1,temp);
			temp = find_run_mark(pccl,chk2,temp);
		}
		temp = find_run_mark(pccl,pccl->temp[++curr],temp);
		++size;
	}
	if (temp>1) {
		if (chk1>temp) {
			while (pccl->stat[chk1].mark!=chk1) {
				chk2 = pccl->stat[chk1].mark;
				pccl->stat[chk1].mark = temp;
				chk1 = chk2;
			}
			pccl->stat[chk1].mark = temp;
		}
	}
	if (mark) *mark = temp;
	return size;
}
/*----------------------------------------------------------------------------*/
void exec_run_1(my1ccl_t* pccl) {
	int irow, icol, temp, mark;
	int rows, cols;
	/* border pixels are assumed cleared */
	rows = pccl->rows;
	cols = pccl->cols;
	pccl->curr = pccl->data; pccl->temp = pccl->data;
	for (irow=0;irow<rows;irow++) {
		for (icol=0;icol<cols;icol++) {
			temp = find_run(pccl,icol,&mark);
			if (temp) {
				if (!mark) {
					mark = pccl->step++;
					/* keep record of new marks? */
					ccl_mark(pccl,mark);
				}
				mark_run(pccl,icol,temp,mark);
				icol += temp; icol--;
			}
		}
		pccl->temp = pccl->curr;
		pccl->curr += cols;
	}
}
/*----------------------------------------------------------------------------*/
void exec_run_2(my1ccl_t* pccl) {
	int irow, icol, mark, temp;
	int rows, cols;
	rows = pccl->rows-1;
	cols = pccl->cols;
	pccl->curr = &pccl->data[rows*cols];
	pccl->temp = pccl->curr - (cols--);
	for (irow=rows;irow>=0;irow--) {
		for (icol=cols;icol>=0;icol--) {
			mark = pccl->curr[icol];
			if (mark) {
				/* remark if mark re-assigned */
				temp = pccl->stat[mark].mark;
				if (temp!=mark) {
					pccl->curr[icol] = temp;
					ccl_less(pccl,mark);
					ccl_more(pccl,temp);
					if (!pccl->stat[mark].size) {
						pccl->stat[mark].mark = 0;
						pccl->size--;
					}
					mark = temp;
				}
				/* check all above */
				if (irow>0) {
					if (icol<cols) {
						temp = pccl->temp[icol+1];
						if (temp&&temp!=mark)
							pccl->stat[temp].mark = mark;
					}
					if (icol>0) {
						temp = pccl->temp[icol-1];
						if (temp&&temp!=mark)
							pccl->stat[temp].mark = mark;
					}
					temp = pccl->temp[icol];
					if (temp&&temp!=mark)
						pccl->stat[temp].mark = mark;
				}
			}
		}
		pccl->curr = pccl->temp;
		pccl->temp -= pccl->cols;
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
