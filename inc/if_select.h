/*----------------------------------------------------------------------------*/
/*
	if_select.h
	- image_filter to use buffer area (if defined) as output image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_SELECT_H__
#define __MY1IF_SELECT_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_SELECT "if_select"
/*----------------------------------------------------------------------------*/
#define IFSELECT_ACTIVE 0x100000
#define IFSELECT_NEW 0x200000
#define IFSELECT_FLAGS (IFSELECT_ACTIVE|IFSELECT_NEW)
/*----------------------------------------------------------------------------*/
my1image_t* filter_select(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	my1image_buffer_t* buff = pass->buffer;
	unsigned int prev, curr;
	prev = (pass->buffer->flag&IFSELECT_ACTIVE);
	pass->buffer->flag &= IFSELECT_FLAGS;
	if (buff->region.hval&&buff->region.wval) {
		image_make(res,buff->region.hval,buff->region.wval);
		image_get_area(img,res,&buff->region);
		buff->select = buff->region;
		/* redefine region */
		buff->region.xset = 0;
		buff->region.yset = 0;
		filter_multi1(pass);
		pass->buffer->flag |= IFSELECT_ACTIVE;
	}
	else {
		res = img; /* maintain input image! */
		filter_multi0(pass);
	}
	curr = (pass->buffer->flag&IFSELECT_ACTIVE);
	if (curr&&curr!=prev)
		pass->buffer->flag |= IFSELECT_NEW;
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_SELECT_H__ */
/*----------------------------------------------------------------------------*/
