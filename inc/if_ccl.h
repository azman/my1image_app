/*----------------------------------------------------------------------------*/
/*
	if_ccl.h
	- image_filter to execute connected component labeling
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_CCL_H__
#define __MY1IF_CCL_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_CCL "if_ccl"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "ccl_run.h"
#include "common.h"
/*----------------------------------------------------------------------------*/
#define IFCCL_FLAG_NOBDR 0x01
#define IFCCL_FLAG_KEEP1 0x02
#define IFCCL_STAT_FOUND 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1if_ccl_t {
	my1image_t buff;
	my1image_scan_t scan;
	my1ccl_t _ccl;
	int pmin, rsv0;
	unsigned int flag, stat;
} my1if_ccl_t;
/*----------------------------------------------------------------------------*/
void filter_ccl_conf_noborder(my1ipass_t* filter, int none) {
	my1if_ccl_t* data = (my1if_ccl_t*) filter->data;
	if (none) data->flag |= IFCCL_FLAG_NOBDR;
	else data->flag &= ~IFCCL_FLAG_NOBDR;
}
/*----------------------------------------------------------------------------*/
void filter_ccl_conf_keep1(my1ipass_t* filter, int keep) {
	my1if_ccl_t* data = (my1if_ccl_t*) filter->data;
	if (keep) data->flag |= IFCCL_FLAG_KEEP1;
	else data->flag &= ~IFCCL_FLAG_KEEP1;
}
/*----------------------------------------------------------------------------*/
void filter_ccl_conf_pmin(my1ipass_t* filter, int pmin) {
	my1if_ccl_t* data = (my1if_ccl_t*) filter->data;
	if (pmin>=0) data->pmin = pmin;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_ccl(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1if_ccl_t* temp = (my1if_ccl_t*) filter->data;
	my1iscan_t* scan = &temp->scan;
	my1image_t* fbuf = &temp->buff;
	temp->stat &= ~IFCCL_STAT_FOUND;
	/* create binary image */
	image_make(fbuf,img->rows,img->cols);
	iscan_init(scan,img,BORDER_WIDTH);
	iscan_prep(scan);
	while (iscan_next(scan)) {
		if (iscan_skip(scan)||!img->data[scan->loop])
			fbuf->data[scan->loop] = 0;
		else
			fbuf->data[scan->loop] = 1;
	}
	/* ccl algo */
	ccl_init(&temp->_ccl,fbuf->data,fbuf->rows,fbuf->cols);
	exec_run_1(&temp->_ccl);
	exec_run_2(&temp->_ccl);
	/* get biggest blob */
	ccl_stat(&temp->_ccl);
	ccl_min_suppress(&temp->_ccl,temp->pmin);
	/* sweep border */
	ccl_flag_border(&temp->_ccl,scan->skip,scan->bcol,scan->skip,scan->brow);
	if (temp->flag&IFCCL_FLAG_NOBDR) {
		int curr, prev;
		curr = temp->_ccl.stat[0].stat; prev= 0;
		while (curr&&temp->_ccl.stat[curr].flag&CCLFLAG_ONBORDER) {
			prev = curr;
			curr = temp->_ccl.stat[curr].stat;
		}
		if (temp->flag&IFCCL_FLAG_KEEP1) {
			if (!curr) curr = prev;
		}
		if (curr&&curr!=temp->_ccl.ilab) {
			temp->_ccl.imax = temp->_ccl.stat[curr].size;
			temp->_ccl.ilab = temp->_ccl.stat[curr].mark;
			temp->_ccl.ichk = curr;
		}
	}
	ccl_max_suppress(&temp->_ccl);
	/* prepare output */
	image_make(res,img->rows,img->cols);
	if (temp->_ccl.ilab>1) { /* valid label! */
		iscan_init(scan,fbuf,BORDER_WIDTH);
		iscan_prep(scan);
		while (iscan_next(scan)) {
			if (!fbuf->data[scan->loop])
				res->data[scan->loop] = 0;
			else
				res->data[scan->loop] = img->data[scan->loop];
		}
		temp->stat |= IFCCL_STAT_FOUND;
	}
	else image_fill(res,0);
	res->mask = img->mask;
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_ccl_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_ccl_t* temp;
	filter->data = malloc(sizeof(my1if_ccl_t));
	temp = (my1if_ccl_t*) filter->data;
	image_init(&temp->buff);
	temp->pmin = 0;
	temp->flag = 0;
	temp->stat = 0;
}
/*----------------------------------------------------------------------------*/
void filter_ccl_free(my1ipass_t* filter) {
	my1if_ccl_t* temp = (my1if_ccl_t*) filter->data;
	image_free(&temp->buff);
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
void filter_ccl_conf(my1ipass_t* filter, char *pkey, char *pval) {
	my1if_ccl_t* data;
	int test;
	data = (my1if_ccl_t*) filter->data;
	if (!strncmp(pkey,"pmin",5)) {
		test = atoi(pval);
		if (test>=0) data->pmin = test;
	}
	else if (!strncmp(pkey,"keep1",6)) {
		if ((pval[0]==0x31||pval[0]==0x30)&&pval[1]==0x0) {
			if (pval[0]==0x31) data->flag |= IFCCL_FLAG_KEEP1;
			else data->flag &= ~IFCCL_FLAG_KEEP1;
		}
	}
	else if (!strncmp(pkey,"nobdr",6)) {
		if ((pval[0]==0x31||pval[0]==0x30)&&pval[1]==0x0) {
			if (pval[0]==0x31) data->flag |= IFCCL_FLAG_NOBDR;
			else data->flag &= ~IFCCL_FLAG_NOBDR;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_CCL_H__ */
/*----------------------------------------------------------------------------*/
