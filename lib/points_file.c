/*----------------------------------------------------------------------------*/
/**
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include "points_file.h"
#include "vector_file.h"
#include "vector_points.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
#define FLAG_DO_VERBOSE 0x0001
#define FLAG_FINDMOMENT 0x0002
#define FLAG_MAKEVECTOR 0x0004
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR ~((~0L)>>1)
#define STAT_ERROR_ARGS (STAT_ERROR|0x00000001)
#define STAT_ERROR_LOAD (STAT_ERROR|0x00000002)
#define STAT_ERROR_SAVE (STAT_ERROR|0x00000004)
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	int loop, test; /** general purpose vars */
	int flag, stat; /** control/status bits */
	int tmp1, tmp2; /** temp storage */
	int size, tpad;
	char *psrc, *pdst, *look;
	my1points_t inpp, prop, *curr, *next;
	my1vector_t prov, *pvec;
} data_t;
/*----------------------------------------------------------------------------*/
int data_args(data_t *data, int argc, char* argv[]) {
	/* get options */
	data->flag = 0; data->stat = 0;
	data->psrc = 0; data->pdst = 0;
	data->size = 0; /* by default, do not sample */
	for (data->loop=1;data->loop<argc;data->loop++) {
		if (argv[data->loop][0]=='-'&&argv[data->loop][1]=='-') {
			data->look = &argv[data->loop][2];
			if (!strncmp(data->look,"save",5)) {
				if (++data->loop<argc)
					data->pdst = argv[data->loop];
				else {
					fprintf(stderr,"** No arg for '%s'!\n",argv[data->loop-1]);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->look,"sample",7)) {
				if (++data->loop<argc) data->size = atoi(argv[data->loop]);
				else {
					fprintf(stderr,"** No arg for '%s'!\n",argv[data->loop-1]);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else if (!strncmp(data->look,"moment",7))
				data->flag |= FLAG_FINDMOMENT;
			else if (!strncmp(data->look,"vector",7))
				data->flag |= FLAG_MAKEVECTOR;
			else if (!strncmp(data->look,"verbose",8))
				data->flag |= FLAG_DO_VERBOSE;
			else {
				fprintf(stderr,"** Invalid option '%s'!\n",argv[data->loop]);
				data->stat |= STAT_ERROR_ARGS;
			}
		}
		else {
			/* non-parameter */
			if (!data->psrc) data->psrc = argv[data->loop];
			else {
				fprintf(stderr,"** Invalid argument '%s'!\n",argv[data->loop]);
				data->stat |= STAT_ERROR_ARGS;
			}
		}
	}
	if (!data->psrc) {
		fprintf(stderr,"** No input file given!\n");
		data->stat |= STAT_ERROR_ARGS;
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_init(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Initializing data...\n");
	points_init(&data->inpp);
	points_init(&data->prop);
	vector_init(&data->prov);
	data->curr = &data->inpp;
	data->next = &data->prop;
	data->pvec = &data->prov;
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_free(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Cleaning up data...\n");
	vector_free(&data->prov);
	points_free(&data->inpp);
	points_free(&data->prop);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_load(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Loading points... ");
	if (points_load(&data->inpp,data->psrc)) {
		data->stat |= STAT_ERROR_LOAD;
		if (data->flag&FLAG_DO_VERBOSE)
			printf("error?\n");
		return data->stat;
	}
	if (data->flag&FLAG_DO_VERBOSE) {
		printf("done. [%d/%d](%08x)\n  ",
			data->inpp.fill,data->inpp.size,data->inpp.flag);
		printf("Xmin:%d, Xmax:%d, ",data->inpp.xmin,data->inpp.xmax);
		printf("Ymin:%d, Ymax:%d\n",data->inpp.ymin,data->inpp.ymax);
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_proc(data_t *data) {
	if (data->flag&FLAG_FINDMOMENT) {
		if (data->flag&FLAG_DO_VERBOSE)
			printf("-- Calculating moment... ");
		points_moment(data->curr);
		if (data->flag&FLAG_DO_VERBOSE)
			printf("done. (%d,%d)\n",data->curr->orig.x,data->curr->orig.y);
	}
	if (data->size>0) {
		if (data->size<data->curr->fill) {
			if (data->flag&FLAG_DO_VERBOSE)
				printf("-- Sampling %d points... ",data->size);
			points_sample(data->curr,data->next,data->size);
			if (data->flag&FLAG_DO_VERBOSE)
				printf("done. (%d<%d)\n",data->next->fill,data->curr->fill);
			data->curr = data->next;
		}
		else {
			fprintf(stderr,"** Invalid sample size (%d>%d)\n",
				data->size,data->curr->fill);
		}
	}
	if (data->flag&FLAG_MAKEVECTOR) {
		if (data->flag&FLAG_DO_VERBOSE)
			printf("-- Convert to vector... ");
		points_make_vector_cc(data->curr,data->pvec);
		if (data->flag&FLAG_DO_VERBOSE)
			printf("done. [%d/%d]\n",data->pvec->fill,data->pvec->size);
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_save(data_t *data) {
	if (data->pdst) {
		if (data->pvec->fill) {
			if (data->flag&FLAG_DO_VERBOSE)
				printf("-- Saving vector... ");
			if (vector_save(data->pvec,data->pdst)) {
				data->stat |= STAT_ERROR_SAVE;
				if (data->flag&FLAG_DO_VERBOSE)
					printf("error?\n");
			}
			else {
				if (data->flag&FLAG_DO_VERBOSE)
					printf("done.\n");
			}
		}
		else {
			if (data->flag&FLAG_DO_VERBOSE)
				printf("-- Saving points... ");
			if (points_save(data->curr,data->pdst)) {
				data->stat |= STAT_ERROR_SAVE;
				if (data->flag&FLAG_DO_VERBOSE)
					printf("error?\n");
			}
			else {
				if (data->flag&FLAG_DO_VERBOSE)
					printf("done.\n");
			}
		}
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	data_args(&data,argc,argv);
	if (data.stat) return data.stat;
	do {
		if (data_init(&data)) break;
		if (data_load(&data)) break;
		if (data_proc(&data)) break;
		if (data_save(&data)) break;
	} while (0);
	data_free(&data);
	return data.stat;
}
/*----------------------------------------------------------------------------*/
