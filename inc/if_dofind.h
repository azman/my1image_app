/*----------------------------------------------------------------------------*/
/*
	if_dofind.h
	- multi-pass image_filter to find single contour and create fd
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_DOFIND_H__
#define __MY1IF_DOFIND_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_DOFIND "if_dofind"
/*----------------------------------------------------------------------------*/
#define DOFIND_STAT_ERROR_LOAD 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1if_dofind_t {
	my1ipass_t *list, *curr;
	unsigned int flag, stat;
} my1if_dofind_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_dofind(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	image_copy(res,img);
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_dofind_init(my1ipass_t* pass, my1ipass_t* from) {
	my1if_dofind_t* temp;
	my1ipass_t *work;
	pass->data = malloc(sizeof(my1if_dofind_t));
	if (pass->data) {
		temp = (my1if_dofind_t*) pass->data;
		temp->list = 0x0;
		temp->curr = 0x0;
		temp->flag = 0;
		temp->stat = 0;
		/* add what we need here */
		work = filter_create(IFNAME_SELECT,0,0,filter_select,0x0,0x0);
		if (work) temp->list = filter_insert(temp->list,work);
		else temp->stat |= DOFIND_STAT_ERROR_LOAD;
		/* work in progress... */
	}
}
/*----------------------------------------------------------------------------*/
void filter_dofind_free(my1ipass_t* pass) {
	my1if_dofind_t* temp;
	if (pass->data) {
		temp = (my1if_dofind_t*) pass->data;
		filter_free_clones(temp->list);
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_DOFIND_H__ */
/*----------------------------------------------------------------------------*/
