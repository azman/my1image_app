/*----------------------------------------------------------------------------*/
/*
	if_branch_points.h
	- extension for if_branch
	  = provides conditional function based on if_points results
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_BRANCH_POINTS_H__
#define __MY1IF_BRANCH_POINTS_H__
/*----------------------------------------------------------------------------*/
#include "if_branch.h"
#include "if_points.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_BRANCH_POINTS IFNAME_BRANCH"_points"
/*----------------------------------------------------------------------------*/
typedef my1if_branch_t my1if_branch_points_t;
/*----------------------------------------------------------------------------*/
int ifpoints_detect(my1image_t* curr, my1ipass_t* pass) {
	int test = 0; /* enable next filter by default */
	if (pass->buffer->flag&POINTS_FOUND) test = 1;
	return test;
}
/*----------------------------------------------------------------------------*/
void filter_branch_points_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_branch_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_branch_t));
	curr = (my1if_branch_t*) filter->data;
	curr->flag = 0;
	if (!pclone) curr->cond = ifpoints_detect;
	else {
		pchk = (my1if_branch_t*) pclone->data;
		curr->cond = pchk->cond;
	}
}
/*----------------------------------------------------------------------------*/
#define filter_branch_points_free(p) filter_branch_free(p)
#define filter_branch_points_conf(p,k,v) filter_branch_conf(p,k,v)
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_BRANCH_POINTS_H__ */
/*----------------------------------------------------------------------------*/
