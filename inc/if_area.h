/*----------------------------------------------------------------------------*/
/*
	if_area.h
	- image_filter to set buffer area
	- by default, use whole image
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_AREA_H__
#define __MY1IF_AREA_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_AREA "if_area"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1if_area_t {
	my1image_area_t region;
} my1if_area_t;
/*----------------------------------------------------------------------------*/
my1image_t* filter_area(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1image_buffer_t* buff = filter->buffer;
	my1if_area_t* area = (my1if_area_t*) filter->data;
	buff->region = area->region;
	if (!buff->region.wval)
		buff->region.wval = img->cols;
	if (!buff->region.hval)
		buff->region.hval = img->rows;
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_area_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_area_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_area_t));
	curr = (my1if_area_t*) filter->data;
	if (!pclone)
		image_area_make(&curr->region,0,0,0,0);
	else {
		pchk = (my1if_area_t*) pclone->data;
		curr->region = pchk->region;
	}
}
/*----------------------------------------------------------------------------*/
void filter_area_free(my1ipass_t* filter) {
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_AREA_H__ */
/*----------------------------------------------------------------------------*/
