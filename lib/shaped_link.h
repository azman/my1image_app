/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_LINK_H__
#define __MY1SHAPED_LINK_H__
/*----------------------------------------------------------------------------*/
#include "item.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_link_t {
	void* from; /* my1shaped_dist_t* */
	double dist;
} my1shaped_link_t;
/*----------------------------------------------------------------------------*/
my1item_t* make_shaped_link_item(void* from, double dist) {
	my1shaped_link_t* link;
	my1item_t* item;
	item = make_item();
	if (item) {
		link = (my1shaped_link_t*)malloc(sizeof(my1shaped_link_t));
		if (link) {
			link->from = from;
			link->dist = dist;
			item->data = (void*)link;
		}
		else {
			free((void*)item);
			item = 0x0;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
#define free_shaped_link_item list_free_item_data 
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_LINK_H__ */
/*----------------------------------------------------------------------------*/
