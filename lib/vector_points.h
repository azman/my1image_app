/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_POINTS_H__
#define __MY1VECTOR_POINTS_H__
/*----------------------------------------------------------------------------*/
/**
 * vector_points.h
 * - functions to create vector representation of cartesian points
 *   = e.g. complex-coordinates (direct) or centroid distance
 * - and, or course, the reverse functions
**/
/*----------------------------------------------------------------------------*/
#include "points.h"
#include "vector.h"
/*----------------------------------------------------------------------------*/
typedef void (*pmakev_t)(my1points_t*,my1vector_t*);
typedef int (*pfromv_t)(my1points_t*,my1vector_t*);
/*----------------------------------------------------------------------------*/
#define points_make_vector points_make_vector_cc
#define points_from_vector points_from_vector_cc
/*----------------------------------------------------------------------------*/
/* create vector of complex coordinates */
void points_make_vector_cc(my1points_t* points, my1vector_t* vector) {
	double real, imag;
	int loop, size = points->fill;
	my1point_t* orig = &points->orig;
	my1point_t* temp = points->loop;
	vector_make(vector,size);
	vector->fill = 0;
	for (loop=0;loop<size;loop++,temp++) {
		real = (double)temp->x-orig->x;
		imag = (double)temp->y-orig->y;
		vector_append(vector,real,imag);
	}
}
/*----------------------------------------------------------------------------*/
/* create points from vector of complex coordinates */
int points_from_vector_cc(my1points_t* points, my1vector_t* vector) {
	int loop, skip, size, x ,y;
	size = vector->size;
	points_make(points,size);
	points->fill = 0; /* just in case */
	for (loop=0,skip=0;loop<size;loop++) {
		x = (int) round(vector->data[loop].real);
		y = (int) round(vector->data[loop].imag);
		/* cannot have point at origin! */
		if (!x&&!y) {
			skip++;
			continue;
		}
		points_append(points,x,y);
	}
	return skip; /* zero is good! */
}
/*----------------------------------------------------------------------------*/
/* create vector of centroid distance */
void points_make_vector_cd(my1points_t* points, my1vector_t* vector) {
	double real, imag;
	int loop, size = points->fill;
	my1point_t* orig = &points->orig;
	my1point_t* temp = points->loop;
	vector_make(vector,size);
	vector->fill = 0;
	for (loop=0;loop<size;loop++,temp++) {
		real = (double)temp->x-orig->x;
		imag = (double)temp->y-orig->y;
		real = sqrt(real*real+imag*imag);
		vector_append(vector,real,0.0);
	}
}
/*----------------------------------------------------------------------------*/
/* create points from vector of centroid distance */
int points_from_vector_cd(my1points_t* points, my1vector_t* vector) {
	double angl, magn;
	int loop, skip, size, quad, temp, x ,y;
	size = vector->size;
	points_make(points,size);
	points->fill = 0; /* just in case */
	for (loop=0,skip=0;loop<size;loop++) {
		angl = (double)loop*360.0/size;
		if (angl<=90.0) quad = 0;
		else if (angl<=180.0) {
			quad = 1;
			angl -= 90.0;
		}
		else if (angl<=270.0) {
			quad = 2;
			angl -= 180.0;
		}
		else {
			quad = 3;
			angl -= 270.0;
		}
		magn = complex_magnitude(&vector->data[loop]);
		/*printf("@@ Point:(%.4lf@%03.2lf|%d)->",magn,angl,quad);*/
		angl = CONSTANT_PI_*angl/180.0;
		x = (int) round(magn*cos(angl));
		y = (int) round(magn*sin(angl));
		/* cannot have point at origin! */
		if (!x&&!y) {
			skip++;
			continue;
		}
		/*printf("(%d,%d)->",x,y);*/
		switch (quad) {
			case 0: x = -x; break;
			case 1: temp = x; x = y; y = temp; break;
			case 2: y = -y; break;
			case 3: temp = x; x = -y; y = -temp; break;
		}
		/*printf("(%d,%d)\n",x,y);*/
		points_append(points,x,y);
	}
	return skip; /* zero is good! */
}
/*----------------------------------------------------------------------------*/
/* create vector of centroid distance AND phase */
void points_make_vector_dp(my1points_t* points, my1vector_t* vector) {
	double real, imag;
	int loop, size = points->fill;
	my1point_t* orig = &points->orig;
	my1point_t* temp = points->loop;
	vector_make(vector,size);
	vector->fill = 0;
	for (loop=0;loop<size;loop++,temp++) {
		real = (double)temp->x-orig->x;
		imag = (double)temp->y-orig->y;
		vector_append(vector,sqrt(real*real+imag*imag),atan2(imag,real));
	}
}
/*----------------------------------------------------------------------------*/
/* create points from vector of centroid distance AND phase */
int points_from_vector_dp(my1points_t* points, my1vector_t* vector) {
	int loop, skip, size, x ,y;
	double temp;
	size = vector->size;
	points_make(points,size);
	points->fill = 0; /* just in case */
	for (loop=0,skip=0;loop<size;loop++) {
		temp = vector->data[loop].imag;
		x = (int) (vector->data[loop].real*cos(temp));
		y = (int) (vector->data[loop].real*sin(temp));
		if (temp<0) y = -y;
		/* cannot have point at origin! */
		if (!x&&!y) {
			skip++;
			continue;
		}
		points_append(points,x,y); // coordinates around origin
	}
	return skip; /* zero is good! */
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_POINTS_H__ */
/*----------------------------------------------------------------------------*/
