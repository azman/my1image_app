/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPE_H__
#define __MY1SHAPE_H__
/*----------------------------------------------------------------------------*/
/**
 * shape.h
 * - alternative for vector_shape.h
 * - allows using fft instead of dft only
**/
/*----------------------------------------------------------------------------*/
#include "vector_points.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1shape_t {
	my1vector_t keep, buff, xtra; /* internal storage */
	my1points_t pnts;
	int flag, size; /* flag & fd size */
	int iscl, pcnt; /* image scale and point count */
	my1vector_t *desc; /* can use external vector */
} my1shape_t;
/*----------------------------------------------------------------------------*/
#define DEFAULT_POINTCOUNT -1
#define DEFAULT_IMAGESCALE 120
/*----------------------------------------------------------------------------*/
#define SHAPE_FLAG_DOSAMPLE 0x0001
#define SHAPE_FLAG_PICK_FFT 0x0002
#define SHAPE_FLAG_BAND_DFT 0x0004
/* options to use complex coordinates or centroid distance/phase */
#define SHAPE_FLAG_POINTSCC 0x0100
#define SHAPE_FLAG_POINTSDP 0x0200
#define SHAPE_FLAG_POINTS   (SHAPE_FLAG_POINTSCC|SHAPE_FLAG_POINTSDP)
/*----------------------------------------------------------------------------*/
/* by default use dft, full input size, centroid distance */
#define SHAPE_FLAG_DEFAULT 0
/*----------------------------------------------------------------------------*/
#define SHAPE(ps) ((my1shape_t*)ps)
#define shape_flag(ps) SHAPE(ps)->flag
#define shape_use_none(ps) SHAPE(ps)->flag&=~SHAPE_FLAG_POINTS
#define shape_use_flag(ps,pf) shape_use_none(ps); SHAPE(ps)->flag|=pf
#define shape_use_cd(ps) shape_use_none(ps)
#define shape_use_cc(ps) shape_use_flag(ps,SHAPE_FLAG_POINTSCC)
#define shape_use_dp(ps) shape_use_flag(ps,SHAPE_FLAG_POINTSDP)
/*----------------------------------------------------------------------------*/
void shape_init(my1shape_t* cont, my1vector_t* pvec) {
	vector_init(&cont->keep);
	vector_init(&cont->buff);
	vector_init(&cont->xtra);
	points_init(&cont->pnts);
	cont->flag = SHAPE_FLAG_DEFAULT;
	cont->size = 0; /* target size for FD points */
	cont->iscl = DEFAULT_IMAGESCALE;
	cont->pcnt = DEFAULT_POINTCOUNT;
	if (!pvec) pvec = &cont->keep;
	cont->desc = pvec;
}
/*----------------------------------------------------------------------------*/
void shape_free(my1shape_t* cont) {
	points_free(&cont->pnts);
	vector_free(&cont->xtra);
	vector_free(&cont->buff);
	vector_free(&cont->keep);
}
/*----------------------------------------------------------------------------*/
void shape_from_vector(my1shape_t* cont, my1vector_t* pvec) {
	/** SHAPE_FLAG_DOSAMPLE ignored here */
	if (cont->flag&SHAPE_FLAG_BAND_DFT) {
		vector_make(cont->desc,cont->size?cont->size:pvec->fill);
		vector_dft_base(pvec,cont->desc,DFT_EXP_NEGATIVE|DFT_CALC_FRQBAND);
	}
	else {
		if (cont->flag&SHAPE_FLAG_PICK_FFT)
			vector_fft(pvec,cont->desc);
		else {
			vector_make(cont->desc,pvec->fill);
			vector_dft(pvec,cont->desc);
		}
		/* windowing function to reduce */
		if (cont->size>0&&cont->size<pvec->fill)
			vector_freq_window(cont->desc,cont->size>>1);
	}
	/* scale and normalize! */
	vector_scale(cont->desc,(double)1/pvec->fill); /* average from full size */
	vector_norm(cont->desc,(cont->flag&SHAPE_FLAG_POINTSCC)?1:0);
}
/*----------------------------------------------------------------------------*/
void shape_prep_points(my1shape_t* cont, my1points_t* ppnt) {
	if (cont->flag&SHAPE_FLAG_DOSAMPLE||cont->flag&SHAPE_FLAG_PICK_FFT) {
		points_sample(ppnt,&cont->pnts,-1);
		ppnt = &cont->pnts;
	}
	points_moment(ppnt);
	if (cont->flag&SHAPE_FLAG_POINTSCC)
		points_make_vector_cc(ppnt,&cont->buff);
	else if (cont->flag&SHAPE_FLAG_POINTSDP)
		points_make_vector_dp(ppnt,&cont->buff);
	else points_make_vector_cd(ppnt,&cont->buff);
}
/*----------------------------------------------------------------------------*/
void shape_from_points(my1shape_t* cont, my1points_t* ppnt) {
	shape_prep_points(cont,ppnt);
	shape_from_vector(cont,&cont->buff);
}
/*----------------------------------------------------------------------------*/
void shape_make_vector(my1shape_t* cont, my1vector_t* pvec) {
	my1vector_t *pick;
	int size, test;
	size = cont->pcnt;
	if (size<=0) size = cont->desc->fill;
	if (cont->flag&SHAPE_FLAG_PICK_FFT) {
		pick = cont->desc;
		/* increase to nearest 2^n */
		test = size;
		while (test&(test-1)) test++;
		if (test!=size) {
			vector_copy(&cont->xtra,pick);
			vector_freq_insert(&cont->xtra,test);
			pick = &cont->xtra;
		}
		vector_fft_inv(pick,pvec);
	}
	else {
		vector_make(pvec,size);
		vector_dft_base(cont->desc,pvec,0);
	}
	vector_scale(pvec,(double)cont->iscl);
}
/*----------------------------------------------------------------------------*/
int shape_make_points(my1shape_t* cont, my1points_t* ppnt) {
	pfromv_t conv;
	shape_make_vector(cont,&cont->buff);
	if (cont->flag&SHAPE_FLAG_POINTSCC) conv = points_from_vector_cc;
	else if (cont->flag&SHAPE_FLAG_POINTSDP) conv = points_from_vector_dp;
	else conv = points_from_vector_cd;
	return conv(ppnt,&cont->buff);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPE_H__ */
/*----------------------------------------------------------------------------*/
