/*----------------------------------------------------------------------------*/
/**
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "vector_file.h"
#include "vector_dist.h"
/*----------------------------------------------------------------------------*/
#define FLAG_DO_VERBOSE 0x0001
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR ~((~0L)>>1)
#define STAT_ERROR_ARGS (STAT_ERROR|0x00000001)
#define STAT_ERROR_LOAD (STAT_ERROR|0x00000002)
#define STAT_ERROR_SAVE (STAT_ERROR|0x00000004)
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	int loop, test; /** general purpose vars */
	int flag, stat; /** control/status bits */
	int tmp1, tmp2; /** temp storage */
	int stop, offs;
	char *src1, *src2, *ptmp;
	double chk1, chk2, chk3, chkT;
	my1vector_t inp1, inp2;
} data_t;
/*----------------------------------------------------------------------------*/
int data_args(data_t *data, int argc, char* argv[]) {
	data->flag = 0; data->stat = 0;
	data->stop = 0; data->offs = 0; data->chkT = 0.0;
	if (argc<3) {
		fprintf(stderr,"** Must have at least 2 args! (%d)\n",(argc-1));
		data->stat |= STAT_ERROR_ARGS;
	}
	else {
		data->src1 = argv[1];
		data->src2 = argv[2];
		for (data->loop=3;data->loop<argc;data->loop++) {
			if (argv[data->loop][0]=='-'&&argv[data->loop][1]=='-') {
				data->ptmp = &argv[data->loop][2];
				if (!strncmp(data->ptmp,"skip",5)) {
					if (++data->loop<argc)
						data->offs = atoi(argv[data->loop]);
					else {
						fprintf(stderr,"** No value for '%s'!\n",data->ptmp);
						data->stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(data->ptmp,"stop",5)) {
					if (++data->loop<argc)
						data->stop = atoi(argv[data->loop]);
					else {
						fprintf(stderr,"** No value for '%s'!\n",data->ptmp);
						data->stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(data->ptmp,"threshold",10)) {
					if (++data->loop<argc)
						data->chkT = atof(argv[data->loop]);
					else {
						fprintf(stderr,"** No value for '%s'!\n",data->ptmp);
						data->stat |= STAT_ERROR_ARGS;
					}
				}
				else {
					fprintf(stderr,"** Unknown option '%s'!\n",data->ptmp);
					data->stat |= STAT_ERROR_ARGS;
				}
			}
			else {
				fprintf(stderr,"** Unknown parameter '%s'!\n",data->ptmp);
				data->stat |= STAT_ERROR_ARGS;
			}
		}
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_init(data_t *data) {
	/* initialize stuff */
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Initializing data...\n");
	vector_init(&data->inp1);
	vector_init(&data->inp2);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_free(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Cleaning up data...\n");
	vector_free(&data->inp1);
	vector_free(&data->inp2);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_load(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Loading vector...\n");
	if (vector_load(&data->inp1,data->src1)) {
		data->stat |= STAT_ERROR_LOAD;
		return data->stat;
	}
	if (vector_load(&data->inp2,data->src2)) {
		data->stat |= STAT_ERROR_LOAD;
		return data->stat;
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_proc(data_t *data) {
	if (data->flag&FLAG_DO_VERBOSE)
		printf("-- Calculating distance...\n");
	data->chk1 = vector_distance_avg(&data->inp1,&data->inp2,0,data->stop);
	data->chk2 = vector_distance_rms(&data->inp1,&data->inp2,0,data->stop);
	data->chk3 = vector_distance(&data->inp1,&data->inp2,0,data->stop);
	if (data->chk1!=data->chkT||data->chk2!=data->chkT||
			data->chk3!=data->chkT) {
		fprintf(stderr,"## [DIFF]");
		fprintf(stderr,"{DistanceD:%e}",data->chk1);
		fprintf(stderr,"{DistanceM:%e}",data->chk2);
		fprintf(stderr,"{DistanceC:%e}",data->chk3);
	}
	else fprintf(stderr,"## [OK] Same vector!");
	fprintf(stderr," [Offs:%d,Stop:%d,Threshold:%f]\n",
		data->offs,data->stop,data->chkT);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	data_args(&data,argc,argv);
	if (data.stat) return data.stat;
	do {
		if (data_init(&data)) break;
		if (data_load(&data)) break;
		if (data_proc(&data)) break;
	} while (0);
	data_free(&data);
	return data.stat;
}
/*----------------------------------------------------------------------------*/
