/*----------------------------------------------------------------------------*/
#ifndef __MY1CCL_HOL_H__
#define __MY1CCL_HOL_H__
/*----------------------------------------------------------------------------*/
#include "ccl_stat.h"
/*----------------------------------------------------------------------------*/
int mark_left(my1ccl_t* pccl, int* data, int curr, int mark) {
	int size = 0;
	while (data[curr]==1) {
		data[curr--] = mark;
		ccl_more(pccl,mark);
		size++;
	}
	return size;
}
/*----------------------------------------------------------------------------*/
int mark_right(my1ccl_t* pccl, int* data, int curr, int mark) {
	int size = 0;
	while (data[curr]==1) {
		data[curr++] = mark;
		ccl_more(pccl,mark);
		size++;
	}
	return size;
}
/*----------------------------------------------------------------------------*/
/* this will be recursive - needs separate data pointer */
int mark_label_HOL(my1ccl_t* pccl, int* data, int curr, int mark) {
	int *prev, *next, loop, left, stop, step = 0;
	left = mark_left(pccl,data,curr-1,mark);
	if (!left) left = curr;
	else {
		step += left;
		left = curr - left;
 	}
	stop = mark_right(pccl,data,curr,mark);
	if (!stop) stop = curr;
	else {
		step += stop;
		stop = curr + stop;
	}
	prev = data-pccl->cols;
	next = data+pccl->cols;
	for (loop=left-1;loop<=stop;loop++) {
		if (prev[loop]==1)
			step += mark_label_HOL(pccl,prev,loop,mark);
		if (next[loop]==1)
			step += mark_label_HOL(pccl,next,loop,mark);
	}
	return step;
}
/*----------------------------------------------------------------------------*/
void exec_HOL(my1ccl_t* pccl) {
	int irow, icol, size;
	int rows, cols, *that;
	/* border pixels are assumed cleared */
	rows = pccl->rows;
	cols = pccl->cols;
	that = pccl->data;
	for (irow=0;irow<rows;irow++) {
		for (icol=0;icol<cols;icol++) {
			if (that[icol]==1) {
				size = mark_label_HOL(pccl,that,icol,pccl->step);
				if (size>0) {
					ccl_mark(pccl,pccl->step);
					pccl->step++;
				}
			}
		}
		that += cols;
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
