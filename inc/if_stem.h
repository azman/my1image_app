/*----------------------------------------------------------------------------*/
/*
	if_stem.h
	- image_filter to suppress stem of harumanis mango
	- should be placed AFTER contour edge detection
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_STEM_H__
#define __MY1IF_STEM_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
#include "my1image_scan.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_STEM "if_stem"
/*----------------------------------------------------------------------------*/
#include "common.h"
/*----------------------------------------------------------------------------*/
#define STEM_MIN_CUT 3
/*----------------------------------------------------------------------------*/
#define STEM_FLAG_DRAW_STEM 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1if_stem_data_t {
	int flag, pend;
	int pair, step;
	int cur1, cur2, curd;
	int chk1, chk2, chkd;
} my1if_stem_data_t;
/*----------------------------------------------------------------------------*/
typedef my1if_stem_data_t my1ifsd_t;
/*----------------------------------------------------------------------------*/
void stem_data_init(my1if_stem_data_t* sdat, int icol) {
	sdat->flag = 0; sdat->pend = 0;
	sdat->pair = 0; sdat->step = icol;
	sdat->curd = 0; sdat->chkd = 0;
}
/*----------------------------------------------------------------------------*/
typedef struct _my1if_stem_t {
	int irow, icol;
	int rows, cols;
	int cur1, cur2, curd;
	int chk1, chk2, chkd;
	int prev, curr;
	int smax, ends;
	int lim1, lim2; /* hack: limit stem to middle area */
	int scut, flag;
	int *data;
	my1image_t *psrc, *pdst;
	my1if_stem_data_t *that;
} my1if_stem_t;
/*----------------------------------------------------------------------------*/
void* stem_prep_that(my1if_stem_t* stem, my1image_t* psrc, my1image_t* pdst) {
	void* temp = (void*)stem->that;
	if (psrc->cols>0&&psrc->cols!=stem->cols) {
		temp = realloc(temp,psrc->cols*sizeof(my1ifsd_t));
		if (temp) {
			stem->that = (my1ifsd_t*)temp;
			stem->cols = psrc->cols;
			stem->rows = psrc->rows;
			stem->psrc = psrc;
			stem->pdst = pdst;
			if (stem->flag&STEM_FLAG_DRAW_STEM) {
				image_make(pdst,psrc->rows,psrc->cols);
				image_fill(pdst,0);
			}
			else image_copy(pdst,psrc);
		}
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
void stem_mark_diff(my1if_stem_data_t* stem, my1image_t* psrc, int icol) {
	image_set_pixel(psrc,stem->cur1,icol,WHITE);
	image_set_pixel(psrc,stem->cur2,icol,WHITE);
}
/*----------------------------------------------------------------------------*/
void stem_mark_ends(my1if_stem_data_t* stem, my1image_t* psrc, int icol) {
	int irow;
	for (irow=stem->chk1;irow<=stem->chk2;irow++)
		image_set_pixel(psrc,irow,icol,WHITE);
}
/*----------------------------------------------------------------------------*/
void stem_proc_cuts(my1if_stem_data_t* stem, my1image_t* psrc, int icol) {
	int irow;
	if (stem->flag) {
		for (irow=stem->cur1;irow<=stem->cur2;irow++)
			image_set_pixel(psrc,irow,icol,0);
	}
	else /*if (stem->pend)*/ {
		for (irow=stem->chk1;irow<=stem->chk2;irow++)
			image_set_pixel(psrc,irow,icol,0);
	}
}
/*----------------------------------------------------------------------------*/
void stem_proc_endd(my1if_stem_data_t* stem, my1image_t* psrc, int icol) {
	int irow;
	for (irow=stem->cur1;irow<=stem->cur2;irow++)
		image_set_pixel(psrc,irow,icol,WHITE);
}
/*----------------------------------------------------------------------------*/
void stem_validate(my1ifsd_t* head, my1ifsd_t* tail, my1if_stem_t* stem) {
	if (tail->pair<=stem->scut||!stem->ends) {
		while (head->pair>0) {
#ifdef MY1DEBUG_STEM
			fprintf(stderr,"@@ Done:%d[%d/%d]\n",
				head->step,head->flag?head->cur1:head->chk1,
				head->flag?head->cur2:head->chk2);
#endif
			head->pair = 0;
			head->flag = 0;
			head->pend = 0;
			if (head==tail) break;
			head++;
		}
		return;
	}
	/* most probably a stem - update image! */
	if (!head->pend&&!tail->pend) {
		fprintf(stderr,"** Stem missing end? [@SAVE:%d][@PREV:%d]\n",
			head->step,tail->step);
	}
	/* option to draw just the stem */
	if (stem->flag&STEM_FLAG_DRAW_STEM) {
		while (head->pair) {
			if (head->flag)
				stem_mark_diff(head,stem->pdst,head->step);
			else if (head->pend)
				stem_mark_ends(head,stem->pdst,head->step);
			if (head==tail) break;
			head++;
		}
	}
	else {
		if (tail->pend) {
			if (tail->chk1<stem->lim1||tail->chk2<stem->lim2)
				return;
#ifdef MY1DEBUG_STEM
			fprintf(stderr,"@@ CUT1\n");
#endif
			while (head->pair<stem->scut) {
#ifdef MY1DEBUG_STEM
				fprintf(stderr,"@@ Skip:%d[%d/%d]\n",
					head->step,head->cur1,head->cur2);
#endif
				head++;
			}
#ifdef MY1DEBUG_STEM
			fprintf(stderr,"@@ CutS:%d[%d/%d]\n",
				head->step,head->cur1,head->cur2);
#endif
			stem_proc_endd(head,stem->pdst,head->step);
			head++;
			while (head->pair) {
#ifdef MY1DEBUG_STEM
				fprintf(stderr,"@@ CutD:%d[%d/%d]\n",
					head->step,head->flag?head->cur1:head->chk1,
					head->flag?head->cur2:head->chk2);
#endif
				stem_proc_cuts(head,stem->pdst,head->step);
				if (head==tail) break;
				head++;
			}
		}
		else /* if (head->pend) */ {
			if (head->chk1<stem->lim1||head->chk2<stem->lim2)
				return;
#ifdef MY1DEBUG_STEM
			fprintf(stderr,"@@ CUT2\n");
#endif
			/* stem first */
			tail->chkd = tail->step-stem->scut;
			while (head->pair<=tail->chkd) {
				stem_proc_cuts(head,stem->pdst,head->step);
				head++;
			}
			stem_proc_endd(head,stem->pdst,head->step);
		}
	}
}
/*----------------------------------------------------------------------------*/
int stem_look_flag(my1if_stem_data_t* curr, my1if_stem_data_t* prev) {
	int cur1, cur2;
	if (prev) {
		if (prev->flag) {
			cur1 = prev->cur1 - curr->cur1;
			if (cur1<0) cur1 = -cur1;
			cur2 = prev->cur2 - curr->cur2;
			if (cur2<0) cur2 = -cur2;
			if (cur1<2&&cur2<2) return 1;
		}
		else if (prev->pend) {
			cur1 = prev->chk1 - curr->cur1;
			if (cur1<0) cur1 = -cur1;
			cur2 = prev->chk2 - curr->cur2;
			if (cur2<0) cur2 = -cur2;
			if (cur1<2&&cur2<2) return 2;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int stem_look_pend(my1if_stem_data_t* curr, my1if_stem_data_t* prev) {
	int chk1, chk2;
	if (prev&&prev->flag) {
		chk1 = prev->cur1 - curr->chk1;
		if (chk1<0) chk1 = -chk1;
		chk2 = prev->cur2 - curr->chk2;
		if (chk2<0) chk2 = -chk2;
		if (chk1<2&&chk2<2) return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int filter_stem_flag_get(my1ipass_t* filter, int flag) {
	my1if_stem_t* stem = (my1if_stem_t*) filter->data;
	return stem->flag & flag;
}
/*----------------------------------------------------------------------------*/
int filter_stem_flag_set(my1ipass_t* filter, int flag) {
	my1if_stem_t* stem = (my1if_stem_t*) filter->data;
	int prev = stem->flag & flag;
	stem->flag |= flag;
	return prev;
}
/*----------------------------------------------------------------------------*/
int filter_stem_flag_clr(my1ipass_t* filter, int flag) {
	my1if_stem_t* stem = (my1if_stem_t*) filter->data;
	int prev = stem->flag & flag;
	stem->flag &= ~flag;
	return prev;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_stem(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1if_stem_data_t *curr, *prev, *save;
	my1if_stem_t* stem = (my1if_stem_t*) filter->data;
	if (img->mask==IMASK_COLOR||!stem_prep_that(stem,img,res))
		image_copy(res,img);
	else {
		/* process parameters */
		stem->smax = img->rows>>3; /* maxstem width: 1/8 image rows */
		stem->lim2 = img->rows>>1;
		stem->lim1 = stem->lim2-stem->smax;
		stem->lim2 = stem->lim2-stem->smax;
		stem->ends = 0;
		prev = 0x0; save = 0x0;
		/* processing column-by-column */
		for (stem->icol=0;stem->icol<stem->cols;stem->icol++) {
			curr = &stem->that[stem->icol]; /* data for current column */
			stem_data_init(curr,stem->icol);
			stem->data = &stem->psrc->data[stem->icol];
			stem->cur1 =-1; stem->chk1 =-1; stem->prev = 0;
			for (stem->irow=0;stem->irow<img->rows;stem->irow++) {
				stem->curr = stem->data[0];
				if (!stem->curr) {
					if (stem->prev) {
						/* look for initial point */
						if (stem->cur1<0)
							stem->cur1 = stem->irow-1;
						/* stem end? ends */
						if (stem->chk1>=0) {
							stem->chk2 = stem->irow-1;
							stem->chkd = stem->chk2-stem->chk1+1;
							if (stem->chkd>1&&stem->chkd<stem->smax) {
								if (!curr->pend||
										stem_look_pend((my1ifsd_t*)stem,prev)) {
									curr->pend = 1;
									curr->chk1 = stem->chk1;
									curr->chk2 = stem->chk2;
									curr->chkd = stem->chkd;
								}
							}
							stem->chk1 = -1;
						}
					}
				}
				else {
					if (!stem->prev) {
						/* look for end point */
						if (stem->cur1>=0) {
							stem->cur2 = stem->irow;
							stem->curd = stem->cur2-stem->cur1-1;
							if (stem->curd<stem->smax) {
								if (!curr->flag||
										stem_look_flag((my1ifsd_t*)stem,prev)) {
									curr->flag = 1;
									curr->cur1 = stem->cur1;
									curr->cur2 = stem->cur2;
									curr->curd = stem->curd;
								}
							}
							stem->cur1 = -1;
						}
						/* stem end? init */
						if (stem->chk1<0)
							stem->chk1 = stem->irow;
					}
				}
				stem->prev = stem->curr;
				stem->data += img->cols;
			}
			if (curr->flag) {
				switch (stem_look_flag(curr,prev)) {
					case 1:
						if (!prev->pair) {
							prev->pair = 1;
							save = prev;
#ifdef MY1DEBUG_STEM
							fprintf(stderr,"@@ PrvS:%d[%d/%d]\n",
								prev->step,prev->cur1,prev->cur2);
#endif
						}
						curr->pair = prev->pair+1;
						curr->pend = 0; /* ignore ends */
#ifdef MY1DEBUG_STEM
						fprintf(stderr,"@@ Stem:%d[%d/%d]\n",
							curr->step,curr->cur1,curr->cur2);
#endif
						break;
					case 2:
						if (stem->ends) break;
						if (!prev->pair) {
							prev->pair = 1;
							save = prev;
#ifdef MY1DEBUG_STEM
							fprintf(stderr,"@@ PrvE:%d[%d/%d]\n",
								prev->step,prev->chk1,prev->chk2);
#endif
						}
						curr->pair = prev->pair+1;
						curr->pend = 0; /* ignore ends */
						stem->ends = 1;
#ifdef MY1DEBUG_STEM
						fprintf(stderr,"@@ Stem:%d[%d/%d]\n",
							curr->step,curr->cur1,curr->cur2);
#endif
						break;
				}
			}
			if (curr->pend) {
				switch (stem_look_pend(curr,prev)) {
					case 1:
						if (stem->ends) break;
						if (!prev->pair) {
							prev->pair = 1;
							save = prev;
#ifdef MY1DEBUG_STEM
							fprintf(stderr,"@@ PrvS:%d[%d/%d]\n",
								prev->step,prev->cur1,prev->cur2);
#endif
						}
						curr->pair = prev->pair+1;
						curr->flag = 0; /* ignore stem */
						stem->ends = 1;
#ifdef MY1DEBUG_STEM
						fprintf(stderr,"@@ EndE:%d[%d/%d]\n",
							curr->step,curr->chk1,curr->chk2);
#endif
						break;
				}
			}
			if (prev&&(prev->flag||prev->pend)) {
				if (!prev->pair) {
					prev->flag = 0;
					prev->pend = 0;
				}
			}
			if (!curr->pair) {
				if (save) {
					stem_validate(save,prev,stem);
					stem->ends = 0;
					save = 0x0;
				}
			}
			prev = curr;
		}
		if (save) stem_validate(save,prev,stem);
	}
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_stem_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_stem_t* stem, *temp;
	filter->data = malloc(sizeof(my1if_stem_t));
	stem = (my1if_stem_t*) filter->data;
	if (pclone) {
		temp = (my1if_stem_t*) pclone->data;
		stem->rows = temp->rows;
		stem->cols = temp->cols;
		stem->scut = temp->scut;
		stem->flag = temp->flag;
	}
	else {
		stem->rows = 0;
		stem->cols = 0;
		stem->scut = STEM_MIN_CUT;
		stem->flag = 0;
	}
	/* this is always reset at run-time */
	stem->that = 0x0;
}
/*----------------------------------------------------------------------------*/
void filter_stem_free(my1ipass_t* filter) {
	my1if_stem_t* stem = (my1if_stem_t*) filter->data;
	if (stem->that) free((void*)stem->that);
	if (filter->data) free((void*)filter->data);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_STEM_H__ */
/*----------------------------------------------------------------------------*/
