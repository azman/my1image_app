/*----------------------------------------------------------------------------*/
/**
 * my1ipath_grab - by azman@my1matrix.net
 *   - conveyor simulator library
 *   - using codes from my1video_csim (from my own my1video_app project)
 *   - rewritten to be a grabber for my1image_grab
**/
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "my1path_list.h"
#include "my1ipath_grab.h"
#include "my1image_crgb.h"
#include "my1image_file.h"
/*----------------------------------------------------------------------------*/
void path_grab_load(my1ipath_grab_t* grab, my1image_t* that) {
	char *name;
	my1image_buffer_t *buff = &grab->work;
	my1image_t* temp = &buff->xtra;
	/* should ALWAYS get an image file from list - reset if done */
	if (!list_scan_item(&grab->full)) {
		if (grab->tchk&PGRAB_TCHK_1R)
			grab->tchk |= PGRAB_TCHK_ENDS;
		name = 0x0;
	}
	else name = (char*) grab->full.curr->data;
	/* load dummy if failed to load actual file */
	if (!name||image_load(temp,name)<0) {
		/* black screen */
		image_make(that,grab->maxrow,grab->maxcol);
		image_fill(that,BLACK);
	}
	else image_size_this(temp,that,grab->maxrow,grab->maxcol);
}
/*----------------------------------------------------------------------------*/
void* path_grab_load_thread(void *arg) {
	my1ipath_grab_t* grab = (my1ipath_grab_t*) arg;
	my1thread_t* code = &grab->loader;
	code->running = 1;
	while (!code->stopped) {
		if (!grab->wait) {
			path_grab_load(grab,grab->work.next);
			grab->wait = grab->work.next;
		}
	}
	code->running = 0;
	thread_done(code);
	return 0x0;
}
/*----------------------------------------------------------------------------*/
void path_grab_init(my1ipath_grab_t* grab) {
	grab->wait = 0x0;
	grab->next = 0x0;
	grab->show = &grab->view;
	image_init(&grab->view);
	buffer_init(&grab->work);
	list_init(&grab->full,list_free_item_data);
	path_init(&grab->path);
	thread_init(&grab->loader,path_grab_load_thread);
	grab->maxrow = MAX_HEIGHT;
	grab->maxcol = MAX_WIDTH;
	grab->donext = 0;
	grab->dostep = DEFAULT_COLSKIP;
	grab->flag = PGRAB_FLAG_INIT;
	grab->tchk = PGRAB_TCHK_NONE;
}
/*----------------------------------------------------------------------------*/
void path_grab_free(my1ipath_grab_t* grab) {
	image_free(&grab->view);
	buffer_free(&grab->work);
	list_free(&grab->full);
	path_free(&grab->path);
	if (grab->loader.running) {
		grab->wait = &grab->view; /* disable dummy wait */
		grab->loader.stopped = 1; /* stop loader thread */
		while (grab->loader.running);
		thread_wait(&grab->loader);
	}
	thread_free(&grab->loader);
}
/*----------------------------------------------------------------------------*/
void path_grab_next(my1ipath_grab_t* grab) {
	int irow, icol, rows, cols, tabs;
	int ichk, inxt, step, stop;
	int *prow, *nrow;
	rows = grab->show->rows;
	cols = grab->show->cols;
	tabs = grab->next->cols;
	step = grab->dostep;
	stop = cols - step;
	ichk = grab->donext;
	prow = grab->show->data;
	/* check end of slide for single cycle grab! */
	if (grab->tchk&PGRAB_TCHK_ENDS) {
		if (ichk>=(tabs-step)) {
			image_copy(grab->show,grab->next);
			return;
		}
	}
	/* start sliding, by row */
	for (irow=0;irow<rows;irow++) {
		/* slide step pixels to left */
		for (icol=0;icol<stop;icol++)
			prow[icol] = prow[icol+step];
		prow += cols;
	}
	/* get the rest from incoming - get column by column? */
	for (icol=stop,inxt=ichk;icol<cols;icol++,inxt++) {
		if (inxt==tabs) {
			/* need next image in wait! */
			while (!grab->wait);
			buffer_swap(&grab->work);
			grab->next = grab->work.curr;
			if (!(grab->tchk&PGRAB_TCHK_ENDS)) grab->wait = 0x0;
			inxt = 0;
			nrow = grab->next->data;
			tabs = grab->next->cols;
		}
		prow = &grab->show->data[icol];
		nrow = &grab->next->data[inxt];
		for (irow=0;irow<rows;irow++) {
			prow[0] = nrow[0];
			prow += cols;
			nrow += tabs;
		}
	}
	grab->donext = inxt;
}
/*----------------------------------------------------------------------------*/
void path_grab_path(my1ipath_grab_t* grab, char* path) {
	my1image_t that;
	my1list_t temp;
	my1item_t* item;
	char *name;
	path_access(&grab->path,path);
	if (grab->path.flag<0) {
		grab->flag |= PGRAB_FLAG_PATH_ERROR;
		return;
	}
	list_init(&temp,0x0);
	path_dolist(&grab->path,&temp,PATH_LIST_FILE);
	image_init(&that);
	while (temp.size>0) {
		item = list_pull_item(&temp);
		name = (char*) item->data;
		if (image_load(&that,name)<0) {
			free((void*)item);
			free((void*)name);
			continue;
		}
		/* transfer to actual list */
		list_push_item(&grab->full,item);
	}
	image_free(&that);
	list_free(&temp);
	/* should have number of supported image file(s) */
	if (grab->full.size<2) {
		grab->flag |= PGRAB_FLAG_SIZE_ERROR;
		return;
	}
	/* prepare to browse list */
	grab->full.curr = 0x0;
	grab->donext = 0; /* index - next */
	/** prepare buffer */
	buffer_size_all(&grab->work,grab->maxrow,grab->maxcol);
	/** use blank as first frame */
	image_make(grab->show,grab->maxrow,grab->maxcol);
	image_fill(grab->show,BLACK);
	grab->show->mask = IMASK_COLOR;
	grab->wait = 0x0;
	/* manually load first image */
	path_grab_load(grab,grab->work.curr);
	grab->next = grab->work.curr;
	/* start loader thread - wait until first load */
	thread_exec(&grab->loader,(void*)grab);
	/* remove init */
	grab->flag &= ~PGRAB_FLAG_INIT;
}
/*----------------------------------------------------------------------------*/
