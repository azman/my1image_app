/*----------------------------------------------------------------------------*/
/*
	if_shaped.h
	- image_filter to generate fourier descriptor for closed contour points
	- input should be output from if_points
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_SHAPED_H__
#define __MY1IF_SHAPED_H__
/*----------------------------------------------------------------------------*/
#define IFNAME_SHAPED "if_shaped"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "shape.h"
#include "shaped_item.h"
#include "shaped_file.h"
#include "vector_dist.h"
#include "my1list.h"
/*----------------------------------------------------------------------------*/
#define IFSHAPE_FDTYPE SHAPE_FLAG_BAND_DFT
#define IFSHAPE_FDSIZE 24
#define IFSHAPE_FDNEAR_THRESH 0.02
/*----------------------------------------------------------------------------*/
struct _my1if_shaped_t;
/*----------------------------------------------------------------------------*/
typedef void (*pshow_t)(void*,char*);
/*----------------------------------------------------------------------------*/
typedef struct _my1if_shaped_t {
	my1shape_t schk;
	my1vector_dist_t calc;
	my1list_t list; /* list of my1shaped_t */
	my1list_t clss; /* list of my1shaped_t - classess fd */
	pshow_t show;
	void *sobj;
	/* temporary stuffs */
	my1item_t* item;
	my1shaped_t *pick, *temp;
	my1cclist_t* pnts;
	my1points_t* cont;
	int stat, flag;
	double fdnear;
} my1if_shaped_t;
/*----------------------------------------------------------------------------*/
void filter_shaped_display(my1ipass_t* pass, pshow_t show, void* sobj) {
	my1if_shaped_t* pdat;
	pdat = (my1if_shaped_t*) pass->data;
	pdat->show = show;
	pdat->sobj = sobj;
}
/*----------------------------------------------------------------------------*/
void filter_shaped_redo_list(my1ipass_t* pass) {
	my1if_shaped_t* pdat;
	pdat = (my1if_shaped_t*) pass->data;
	list_redo(&pdat->list);
}
/*----------------------------------------------------------------------------*/
int filter_shaped_class_count(my1ipass_t* pass) {
	my1if_shaped_t* pdat;
	pdat = (my1if_shaped_t*) pass->data;
	return pdat->clss.size;
}
/*----------------------------------------------------------------------------*/
void* filter_shaped_load_class(my1ipass_t* pass, char* file, char* name) {
	my1if_shaped_t* pdat;
	my1item_t* item;
	my1shaped_t* temp;
	pdat = (my1if_shaped_t*) pass->data;
	item = make_shaped_item();
	if (item) {
		temp = (my1shaped_t*)item->data;
		if (shaped_load(temp,file,name)) {
			free_shaped_item(item);
			item = 0x0;
		}
		else list_push_item(&pdat->clss,item);
	}
	return item;
}
/*----------------------------------------------------------------------------*/
#define IFSHAPED_MESGSZ 80
/*----------------------------------------------------------------------------*/
my1image_t* filter_shaped(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	my1if_shaped_t* data;
	my1shaped_t *temp;
	my1ibuff_t* buff;
	char mesg[IFSHAPED_MESGSZ];
	/* pass->prev should be if_points */
	if (strcmp(pass->prev->name,IFNAME_POINTS)) return img;
	data = (my1if_shaped_t*) pass->data;
	buff = pass->buffer;
	/* only if points found - buff->flag&POINTS_FOUND ? */
	if (buff->flag&POINTS_NEW) {
		data->pnts = filter_points_list(pass->prev);
		data->cont = data->pnts->init;
		while (data->cont) {
			data->item = make_shaped_item();
			if (!data->item) {
				fprintf(stderr,"** Cannot create shaped item!\n");
				continue;
			}
			data->temp = (my1shaped_t*)data->item->data;
			shape_from_points(&data->schk,data->cont);
			vector_copy(&data->temp->desc,data->schk.desc);
			if (data->clss.size>0) {
				data->clss.curr = 0x0;
				data->pick = 0x0;
				while (list_scan_item(&data->clss)) {
					temp = (my1shaped_t*)data->clss.curr->data;
					if (vector_dist_calc(&data->calc,
							SVEC(data->temp),SVEC(temp))<data->fdnear) {
						data->pick = temp;
						list_push_item(&data->list,data->item);
						if (temp->name.fill>0)
							snprintf(mesg,IFSHAPED_MESGSZ,
								"Harumanis: '%s'",temp->name.buff);
						else
							snprintf(mesg,IFSHAPED_MESGSZ,
								"Shape class #%d",data->clss.step);
						printf("-- %s [%d]\n",mesg,data->list.size);
						if (data->show)
							data->show(data->sobj,mesg);
						break;
					}
				}
				if (!data->pick) {
					free_shaped_item(data->item);
					printf("-- Shape unknown\n");
				}
			}
			else {
				list_push_item(&data->list,data->item);
				printf("-- Shape found! (%d)\n",data->list.size);
			}
			data->cont = data->cont->next;
		}
	}
	else if (!(buff->flag&POINTS_FOUND)) {
		if (data->show)
			data->show(data->sobj,0x0);
	}
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_shaped_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1item_t* item;
	my1shaped_t *psrc, *pdst;
	my1if_shaped_t *curr, *temp;
	filter->data = (my1if_shaped_t*)malloc(sizeof(my1if_shaped_t));
	if (filter->data) {
		curr = (my1if_shaped_t*) filter->data;
		shape_init(&curr->schk,0x0);
		list_init(&curr->list,free_shaped_item);
		list_init(&curr->clss,free_shaped_item);
		curr->stat = 0;
		if (pclone) {
			temp = (my1if_shaped_t*) pclone->data;
			curr->schk.flag = temp->schk.flag;
			curr->schk.size = temp->schk.size;
			curr->calc.calc = temp->calc.calc;
			curr->calc.offs = temp->calc.offs;
			curr->calc.stop = temp->calc.stop;
			curr->show = temp->show;
			curr->sobj = temp->sobj;
			curr->flag = temp->flag;
			curr->fdnear = temp->fdnear;
			/* copy classes */
			temp->clss.curr = 0x0;
			while (list_scan_item(&temp->clss)) {
				item = make_shaped_item();
				if (item) {
					pdst = (my1shaped_t*)item->data;
					psrc = (my1shaped_t*)temp->clss.curr->data;
					if (!shaped_copy(pdst,psrc)) {
						free_shaped_item(item);
						printf("** Cannot copy shape for clone!\n");
					}
					else list_push_item(&curr->clss,item);
				}
				else printf("** Cannot create item for clone!\n");
			}
		}
		else {
			/* default fd settings */
			curr->schk.flag = IFSHAPE_FDTYPE;
			curr->schk.size = IFSHAPE_FDSIZE;
			/** vector_dist_prep */
			curr->calc.calc = vector_distance;
			curr->calc.offs = 1;
			curr->calc.stop = IFSHAPE_FDSIZE>>1;
			curr->show = 0x0; curr->sobj = 0x0;
			curr->flag = 0;
			curr->fdnear = IFSHAPE_FDNEAR_THRESH;
		}
	}
}
/*----------------------------------------------------------------------------*/
void filter_shaped_free(my1ipass_t* filter) {
	my1if_shaped_t* curr = (my1if_shaped_t*) filter->data;
	if (curr) {
		list_free(&curr->clss);
		list_free(&curr->list);
		shape_free(&curr->schk);
		free(filter->data);
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_SHAPED_H__ */
/*----------------------------------------------------------------------------*/
