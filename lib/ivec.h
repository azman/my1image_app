/*----------------------------------------------------------------------------*/
#ifndef __MY1IVEC_H__
#define __MY1IVEC_H__
/*----------------------------------------------------------------------------*/
/* replicate my1image_t - pointer interchangable */
typedef struct _my1ivec_t {
	int *data,size,flag,cols,rows;
} my1ivec_t;
/*----------------------------------------------------------------------------*/
void ivec_init(my1ivec_t* ivec);
void ivec_free(my1ivec_t* ivec);
int ivec_make(my1ivec_t* ivec,int rows,int cols);
int ivec_copy(my1ivec_t* ivec,my1ivec_t* psrc);
int ivec_fill(my1ivec_t* ivec,int fill);
int ivec_val1(my1ivec_t* ivec,int val1);
void ivec_mask(my1ivec_t* ivec,int skip);
/*----------------------------------------------------------------------------*/
#define ivec_prow(i,r) &((my1ivec_t*)i)->data[(int)(r)*(((my1ivec_t*)i)->cols)]
/*----------------------------------------------------------------------------*/
#ifndef __MY1_HEADER_ONLY__
/*----------------------------------------------------------------------------*/
#include <stdlib.h> /** realloc & free */
/*----------------------------------------------------------------------------*/
void ivec_init(my1ivec_t* ivec) {
	ivec->data = 0x0;
	ivec->size = 0;
	ivec->flag = 0; /* mask in my1image_t */
	ivec->cols = 0;
	ivec->rows = 0;
}
/*----------------------------------------------------------------------------*/
void ivec_free(my1ivec_t* ivec) {
	if (ivec->data) free((void*)ivec->data);
	ivec->data = 0x0;
	ivec->size = 0;
}
/*----------------------------------------------------------------------------*/
int ivec_make(my1ivec_t* ivec, int rows, int cols) {
	int *temp;
	int size, iret;
	iret = 0;
	if (rows>0&&cols>0) {
		size = rows*cols;
		if (size!=ivec->size) {
			temp = (int*)realloc(ivec->data,size*sizeof(int));
			if (temp) {
				ivec->data = temp;
				ivec->size = size;
				ivec->rows = rows;
				ivec->cols = cols;
				iret = size;
			}
		}
		else {
			iret = size;
			/* just in case different dimensions */
			ivec->rows = rows;
			ivec->cols = cols;
		}
	}
	return iret;
}
/*----------------------------------------------------------------------------*/
int ivec_copy(my1ivec_t* ivec, my1ivec_t* psrc) {
	int loop, test;
	test = ivec_make(ivec,psrc->rows,psrc->cols);
	if (!test) return test;
	for (loop=0;loop<ivec->size;loop++)
		ivec->data[loop] = psrc->data[loop];
	return test;
}
/*----------------------------------------------------------------------------*/
int ivec_fill(my1ivec_t* ivec, int fill) {
	int loop = 0;
	while (loop<ivec->size)
		ivec->data[loop++] = fill;
	return loop;
}
/*----------------------------------------------------------------------------*/
int ivec_val1(my1ivec_t* ivec, int val1) {
	int loop = 0;
	while (loop<ivec->size) {
		if (ivec->data[loop]) ivec->data[loop] = val1;
		else ivec->data[loop] = 0;
		loop++;
	}
	return loop;
}
/*----------------------------------------------------------------------------*/
void ivec_mask(my1ivec_t* ivec, int skip) {
	int loop, irow, icol, brow, bcol;
	brow = ivec->rows - skip - 1;
	bcol = ivec->rows - skip - 1;
	for (irow=0,loop=0;irow<ivec->rows;irow++) {
		for (icol=0;icol<ivec->cols;icol++,loop++) {
			/* mask 'border' values */
			if (irow<skip||irow>brow||icol<skip||icol>bcol)
				ivec->data[loop] = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1_HEADER_ONLY__ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1IVEC_H__ */
/*----------------------------------------------------------------------------*/
