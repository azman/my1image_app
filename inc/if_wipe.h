/*----------------------------------------------------------------------------*/
/*
	if_wipe.h
	- image_filter to suppress noise blob pixels
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_WIPE_H__
#define __MY1IF_WIPE_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_scan.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_WIPE "if_wipe"
/*----------------------------------------------------------------------------*/
#define MIN_CONNECT 4
/*----------------------------------------------------------------------------*/
my1image_t* filter_wipe(my1image_t* img, my1image_t* res, my1ipass_t* filter) {
	my1image_scan_t scan;
	int temp;
	image_make(res,img->rows,img->cols);
	res->mask = img->mask;
	iscan_init(&scan,img,BORDER_WIDTH);
	iscan_prep(&scan);
	while (iscan_next(&scan)) {
		res->data[scan.loop] = img->data[scan.loop];
		if (iscan_skip(&scan)) continue;
		temp = iscan_8connected_base(&scan);
		if (scan.curr[scan.icol]&&temp<MIN_CONNECT)
			res->data[scan.loop] = 0;
	}
	return res;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_WIPE_H__ */
/*----------------------------------------------------------------------------*/
