/*----------------------------------------------------------------------------*/
#include "morph.h"
#include "ivec_file.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
#define swap_buff() swap=ivec;ivec=next;next=swap
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, test;
	my1ivec_t load, buff, *ivec, *next, *swap;
	int elem[] = { 1,1,1, 1,1,1, 1,1,1 }; /* structuring element */
	ivec_init(&load);
	ivec_init(&buff);
	do {
		stat = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		test = ivec_load(&load,argv[1]);
		if (!test) {
			printf("** Cannot load '%s'!\n",argv[1]);
			stat = -2;
			break;
		}
		ivec = &load;
		printf("-- Load:\n");
		ivec_show(ivec);
		next = &buff;
		for (loop=2;loop<argc;loop++) {
			if (!strncmp(argv[loop],"--erode",8)) {
				morph_erode(ivec,next,elem);
				swap_buff();
			}
			else if (!strncmp(argv[loop],"--dilate",9)) {
				morph_dilate(ivec,next,elem);
				swap_buff();
			}
			else if (!strncmp(argv[loop],"--open",7)) {
				morph_opening(ivec,next,elem);
				swap_buff();
			}
			else if (!strncmp(argv[loop],"--close",8)) {
				morph_closing(ivec,next,elem);
				swap_buff();
			}
			else printf("** Unknown param '%s'!\n",argv[loop]);
		}
		printf("-- Data:\n");
		ivec_show(ivec);
	} while (0);
	ivec_free(&buff);
	ivec_free(&load);
	return stat;
}
/*----------------------------------------------------------------------------*/
