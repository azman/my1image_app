/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_DB_H__
#define __MY1SHAPED_DB_H__
/*----------------------------------------------------------------------------*/
/**
 * shaped_db.h
 * - basic database structure for list of my1shaped_t
 * - provides functions to load/save from/to file
 * - also a function to generate malloc'd distance matrix
 *   = requires external my1vector_dist_t
**/
/*----------------------------------------------------------------------------*/
#include "shaped_item.h"
#include "vector_dist.h"
/*----------------------------------------------------------------------------*/
/* for malloc/free */
#include <stdlib.h>
/* for strncmp */
#include <string.h>
/* for load/save */
#include <stdio.h>
/* for basename */
#include <libgen.h>
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_db_t {
	my1list_t list; /* list of my1shaped_t */
	my1cstr_t name;
	int tmin, imax;
} my1shaped_db_t;
/*----------------------------------------------------------------------------*/
#define MY1SHAPE_DBID "MY1SHAPEDB\0\0"
#define MY1SHAPE_DBID_SIZE 12
#define MY1SHAPE_NAME_SIZE 32
/*----------------------------------------------------------------------------*/
void shaped_db_init(my1shaped_db_t* data) {
	list_init(&data->list,free_shaped_item);
	cstr_init(&data->name);
	data->tmin = 0;
}
/*----------------------------------------------------------------------------*/
void shaped_db_free(my1shaped_db_t* data) {
	cstr_free(&data->name);
	list_free(&data->list);
}
/*----------------------------------------------------------------------------*/
my1shaped_t* shaped_db_append(my1shaped_db_t* data, my1shaped_t *that) {
	my1shaped_t* cont;
	my1item_t* item;
	cont = 0x0;
	item = make_shaped_item();
	if (item) {
		cont = (my1shaped_t*) item->data;
		shaped_copy(cont,that);
		list_push_item(&data->list,item);
		if (!data->tmin||data->tmin>cont->desc.fill)
			data->tmin = cont->desc.fill;
	}
	return cont;
}
/*----------------------------------------------------------------------------*/
my1shaped_t* shaped_db_search(my1shaped_db_t* data, char* name) {
	my1shaped_t *cont, *find;
	find = 0x0;
	data->list.curr = 0x0;
	while (list_scan_item(&data->list)) {
		cont = (my1shaped_t*)data->list.curr->data;
		if (!strncmp(cont->name.buff,name,cont->name.fill+1)) {
			find = cont;
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
void shaped_db_name(my1shaped_db_t* data, char* name) {
	/* assume name is a filename */
	char *buff, *base;
	buff = strdup(name);
	base = strdup(basename(buff));
	free((void*)buff);
	cstr_assign(&data->name,base);
	free((void*)base);
}
/*----------------------------------------------------------------------------*/
int shaped_db_save(my1shaped_db_t* data, char* name) {
	my1shaped_t *cont;
	FILE* save;
	int iret;
	iret = -1;
	save = fopen(name,"wb");
	if (save) {
		iret++;
		/* write header */
		fwrite((void*)MY1SHAPE_DBID,MY1SHAPE_DBID_SIZE,1,save);
		/* write number of shapes */
		fwrite((void*)&data->list.size,sizeof(int),1,save);
		/** loop through the shapes */
		data->list.curr = 0x0;
		while (list_scan_item(&data->list)) {
			cont = (my1shaped_t*)data->list.curr->data;
			/* write name of shape */
			fwrite((void*)cont->name.buff,MY1SHAPE_NAME_SIZE,1,save);
			/* write number of terms */
			fwrite((void*)&cont->desc.fill,sizeof(int),1,save);
			/* write all data */
			fwrite((void*)cont->desc.data,
				cont->desc.fill*sizeof(my1complex_t),1,save);
		}
		fclose(save);
	}
	return iret;
}
/*----------------------------------------------------------------------------*/
int shaped_db_load(my1shaped_db_t* data, char* name) {
	FILE* load;
	char* buff;
	my1shaped_t temp;
	int loop, size, step, test, iret;
	iret = -1;
	load = fopen(name,"rb");
	if (load) {
		iret++; /* iret = 0; */
		shaped_init(&temp);
		cstr_resize(&temp.name,MY1SHAPE_NAME_SIZE);
		buff = temp.name.buff;
		do {
			/* read/check header */
			test = fread((void*)buff,MY1SHAPE_DBID_SIZE,1,load);
			if (test!=1) {
				fprintf(stderr,"** Cannot read header!\n");
				iret = -2;
				break;
			}
			if (strncmp(buff,MY1SHAPE_DBID,MY1SHAPE_DBID_SIZE)) {
				buff[MY1SHAPE_DBID_SIZE+1] = 0x0;
				fprintf(stderr,"** Invalid header '%s'!\n",buff);
				iret = -3;
				break;
			}
			/* read number of shapes */
			test = fread((void*)&size,sizeof(int),1,load);
			if (test!=1) {
				fprintf(stderr,"** Cannot read shape count!\n");
				iret = -4;
				break;
			}
			/* read all shape info */
			for (loop=0;loop<size;loop++) {
				test = fread((void*)buff,MY1SHAPE_NAME_SIZE,1,load);
				if (test!=1) {
					fprintf(stderr,"** Cannot read shape name! (%d)\n",loop);
					iret = -5;
					break;
				}
				buff[MY1SHAPE_NAME_SIZE-1] = 0x0; /* make sure null is there */
				test = fread((void*)&step,sizeof(int),1,load);
				if (test!=1) {
					fprintf(stderr,"** Cannot read shape term count!\n");
					iret = -6;
					break;
				}
				vector_make(&temp.desc,step);
				test = fread((void*)temp.desc.data,
					sizeof(my1complex_t),step,load);
				if (test!=step) {
					fprintf(stderr,"** Cannot read shape terms!(%d/%d)\n",
						test,step);
					iret = -7;
					break;
				}
				/* manually set fill value! */
				temp.desc.fill = step;
				/* add to db */
				if (!shaped_db_append(data,&temp))
					fprintf(stderr,"** Failed to add shape [%s]!\n",buff);
			}
		} while (0);
		shaped_free(&temp);
		fclose(load);
		if (!iret) shaped_db_name(data,name);
	}
	return iret;
}
/*----------------------------------------------------------------------------*/
double* shaped_db_distance_matrix(my1shaped_db_t* data,
		my1vector_dist_t* calc) {
	int irow, size, curr, temp;
	double *dist, dmax;
	my1item_t *item;
	my1shaped_t *cont, *con2;
	/* prep */
	size = data->list.size;
	dist = (double*)malloc(size*size*sizeof(double));
	if (!dist) return 0x0;
	/* keep track of max distance */
	dmax = 0.0;
	data->imax = -1;
	/* calc */
	item = data->list.init;
	irow = 0;
	while (item) {
		cont = (my1shaped_t*)item->data;
		/* TODO: recalc index - use smaller storage! */
		/* index for current row */
		curr = irow*size;
		/* transpose index iterator */
		temp = irow;
		/* use previously calculated - lower-left triangle */
		data->list.curr = 0x0;
		while (list_scan_item(&data->list)) {
			if (data->list.curr==item) {
				dist[curr++] = 0.0;
				break;
			}
			dist[curr++] = dist[temp];
			temp += size;
		}
		/* calculate distance */
		while (list_scan_item(&data->list)) {
			con2 = (my1shaped_t*)data->list.curr->data;
			dist[curr] = vector_dist_calc(calc,SVEC(cont),SVEC(con2));
			if (dist[curr]>dmax) {
				dmax = dist[curr];
				data->imax = curr;
			}
			curr++;
		}
		item = item->next;
		irow++;
	}
	return dist;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_DB_H__ */
/*----------------------------------------------------------------------------*/
