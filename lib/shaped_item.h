/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_ITEM_H__
#define __MY1SHAPED_ITEM_H__
/*----------------------------------------------------------------------------*/
/**
 * shaped_item.h
 * - helper functions to use my1shaped_t in fifo
 **/
/*----------------------------------------------------------------------------*/
#include "shaped.h"
#include "list.h"
/*----------------------------------------------------------------------------*/
/* malloc/free */
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
my1item_t* make_shaped_item(void) {
	my1shaped_t* cont;
	my1item_t* item;
	item = make_item();
	if (item) {
		cont = (my1shaped_t*)malloc(sizeof(my1shaped_t));
		if (cont) {
			shaped_init(cont);
			item->data = (void*)cont;
		}
		else {
			free((void*)item);
			item = 0x0;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
void free_shaped_item(void* that) {
	my1item_t* item = (my1item_t*)that;
	shaped_free((my1shaped_t*)item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_ITEM_H__ */
/*----------------------------------------------------------------------------*/
