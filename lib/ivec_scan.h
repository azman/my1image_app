/*----------------------------------------------------------------------------*/
#ifndef __MY1IVEC_SCAN_H__
#define __MY1IVEC_SCAN_H__
/*----------------------------------------------------------------------------*/
/* replicate my1image_scan_t - pointer interchangable */
typedef struct _my1ivec_scan_t {
	int *data, *curr, *prev, *next;
	int cols, rows;
	int icol, irow, loop; /* running counters */
	int bcol, brow, skip; /* border mark */
} my1ivec_scan_t;
/*----------------------------------------------------------------------------*/
#define scan_skip_test(scan,x,y) (y<scan->skip||y>scan->brow|| \
	x<scan->skip||x>scan->bcol)?1:0
/*----------------------------------------------------------------------------*/
void scan_init(my1ivec_scan_t* scan, int* data, int rows, int cols, int skip);
void scan_prep(my1ivec_scan_t* scan);
int scan_next(my1ivec_scan_t* scan);
int scan_next_skip(my1ivec_scan_t* scan, int skip);
int scan_skip(my1ivec_scan_t* scan);
int scan_8connected_base(my1ivec_scan_t* scan);
int scan_8connected(my1ivec_scan_t* scan);
int scan_running(my1ivec_scan_t* scan);
/*----------------------------------------------------------------------------*/
#ifndef __MY1_HEADER_ONLY__
/*----------------------------------------------------------------------------*/
void scan_init(my1ivec_scan_t* scan, int* data, int rows, int cols, int skip) {
	scan->data = data;
	scan->rows = rows;
	scan->cols = cols;
	scan->skip = skip;
	scan->brow = rows - skip - 1;
	scan->bcol = cols - skip - 1;
}
/*----------------------------------------------------------------------------*/
void scan_prep(my1ivec_scan_t* scan) {
	scan->curr = scan->data;
	scan->prev = scan->curr;
	scan->next = scan->prev + scan->cols;
	scan->irow = 0;
	scan->icol = -1;
	scan->loop = -1;
}
/*----------------------------------------------------------------------------*/
int scan_next(my1ivec_scan_t* scan) {
	int next = 1;
	scan->loop++;
	scan->icol++;
	if (scan->icol==scan->cols) {
		scan->icol = 0;
		scan->irow++;
		scan->prev = scan->curr;
		scan->curr = scan->next;
		scan->next += scan->cols;
		if (scan->irow==scan->rows) {
			scan->irow = 0;
			scan->icol = -1;
			scan->loop = -1;
			next = 0;
		}
	}
	return next;
}
/*----------------------------------------------------------------------------*/
int scan_next_skip(my1ivec_scan_t* scan, int skip) {
	int next = 1;
	scan->loop += skip;
	scan->icol += skip; /* skip MUST BE < scan->cols */
	if (scan->icol>=scan->cols) {
		scan->icol -= scan->cols;
		scan->irow++;
		scan->prev = scan->curr;
		scan->curr = scan->next;
		scan->next += scan->cols;
		if (scan->irow==scan->rows) {
			scan->irow = 0;
			scan->icol = -1;
			scan->loop = -1;
			next = 0;
		}
	}
	return next;
}
/*----------------------------------------------------------------------------*/
int scan_skip(my1ivec_scan_t* scan) {
	return scan_skip_test(scan,scan->icol,scan->irow);
}
/*----------------------------------------------------------------------------*/
int scan_8connected_base(my1ivec_scan_t* scan) {
	int loop, size, curr, step;
	curr = scan->icol;
	step = curr-1;
	for (loop=0,size=0;loop<3;loop++) {
		if (scan->prev[step]) size++;
		if (scan->curr[step]) if (step!=curr) size++;
		if (scan->next[step]) size++;
		step++;
	}
	return size;
}
/*----------------------------------------------------------------------------*/
int scan_8connected(my1ivec_scan_t* scan) {
	if (!(scan->curr[scan->icol])) return 0;
	return scan_8connected_base(scan);
}
/*----------------------------------------------------------------------------*/
int scan_running(my1ivec_scan_t* scan) {
	int size = 0, loop = scan->icol;
	for (;loop<scan->cols;loop++,size++)
		if (!scan->curr[loop]) break;
	return size;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1_HEADER_ONLY__ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1IVEC_SCAN_H__ */
/*----------------------------------------------------------------------------*/
