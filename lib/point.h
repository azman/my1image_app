/*----------------------------------------------------------------------------*/
#ifndef __MY1POINT_H__
#define __MY1POINT_H__
/*----------------------------------------------------------------------------*/
/**
 * point module: finds contour points
 **/
/*----------------------------------------------------------------------------*/
#include "ivec.h"
#include "ivec_scan.h"
/*----------------------------------------------------------------------------*/
/* minimum value so that all scanned pixel has a 3x3 neighborhood! */
#define CONTOUR_BORDER_SKIP 1
#define CONTOUR_MIN_POINTS 16
/*----------------------------------------------------------------------------*/
/* direction codes is CCW (+ve degree angle) starting from right */
#define DIR_RR 0
#define DIR_TR 1
#define DIR_TT 2
#define DIR_TL 3
#define DIR_LL 4
#define DIR_BL 5
#define DIR_BB 6
#define DIR_BR 7
#define DIR_MAX 8
#define DIR_CHK (DIR_MAX-1)
/*----------------------------------------------------------------------------*/
#define DIR_RIGHT DIR_RR
#define DIR_BOTTOM_RIGHT DIR_BR
#define DIR_BOTTOM DIR_BB
/*----------------------------------------------------------------------------*/
/* 4-bit mask for direction codes */
#define DIR_BITS 4
#define DIR_MASK 0x0F
#define DIR_2MBITS (DIR_BITS<<1)
#define DIR_2MASKS 0xFF
/* 4-bit flag for contour finding */
#define DIR_FLAG_BITS 4
#define DIR_FLAG_OFFS (DIR_BITS+DIR_BITS)
#define DIR_FLAG_MASK (DIR_MASK<<DIR_FLAG_OFFS)
#define DIR_FLAG_MARK (0x01<<DIR_FLAG_OFFS)
/* contour code offset */
#define DIR_CODE_OFFS (DIR_FLAG_OFFS+DIR_FLAG_BITS)
/* helper macro */
#define DIR_ENCODE(odir,idir,code) \
	((odir&DIR_MASK)|((idir&DIR_MASK)<<DIR_BITS)|(code<<DIR_CODE_OFFS))
#define DIR_RECODE(dirs,code) ((dirs&DIR_2MASKS)|(code<<DIR_CODE_OFFS))
#define DIR_DECODE(code) (code>>DIR_CODE_OFFS)
#define DIR_DIRS(code) (code&DIR_2MASKS)
#define DIR_ODIR(code) (code&DIR_MASK)
#define DIR_IDIR(code) ((code>>DIR_BITS)&DIR_MASK)
#define DIR_FLAG(code) ((code>>DIR_FLAG_OFFS)&DIR_MASK)
/*----------------------------------------------------------------------------*/
#define DIR_REVS(dir) (dir>4?dir-4:dir+4)
/*----------------------------------------------------------------------------*/
#define point_test_comp(data,x,y) (x>=0&&x<data->cols&&y>=0&&y<data->rows)
#define point_test_that(data,x,y) (x>=0&&x<data->cols&&y>=0&&y<data->rows)? \
	y*data->cols+x:-1
/*----------------------------------------------------------------------------*/
typedef struct _my1point_t {
	int x,y;
} my1point_t;
/*----------------------------------------------------------------------------*/
void point_make(my1point_t* that, int x, int y) {
	that->x = x;
	that->y = y;
}
/*----------------------------------------------------------------------------*/
void point_copy(my1point_t* that, my1point_t* from) {
	that->x = from->x;
	that->y = from->y;
}
/*----------------------------------------------------------------------------*/
int point_diff(my1point_t* that, my1point_t* diff) {
	int xdif = that->x-diff->x;
	int ydif = that->y-diff->y;
	if (xdif<0) xdif = -xdif;
	if (ydif<0) ydif = -ydif;
	return xdif+ydif;
}
/*----------------------------------------------------------------------------*/
static my1point_t next_offs[DIR_MAX] = {
	/** DIR_RR,DIR_TR,DIR_TT,DIR_TL,DIR_LL,DIR_BL,DIR_BB,DIR_BR */
	{1,0}, {1,-1}, {0,-1}, {-1,-1}, {-1,0}, {-1,1}, {0,1}, {1,1}
	/**
		{-1,-1}    {0,-1}     {1,-1}
		{-1,0}     {0,0}      {1,0}
		{-1,1}     {0,1}      {1,1}
	 **/
};
/*----------------------------------------------------------------------------*/
void point_next(my1point_t* that, my1point_t* next, int path) {
	next->x = that->x + next_offs[path].x;
	next->y = that->y + next_offs[path].y;
}
/*----------------------------------------------------------------------------*/
void point_prev(my1point_t* that, my1point_t* prev, int path) {
	prev->x = that->x - next_offs[path].x;
	prev->y = that->y - next_offs[path].y;
}
/*----------------------------------------------------------------------------*/
int point_scan_null(my1point_t* that, my1point_t* test, my1ivec_t* data) {
	int loop, step, temp;
	for (loop=0,step=0;loop<DIR_MAX;loop++) {
		point_next(that,test,loop);
		temp = point_test_that(data,test->x,test->y);
		if (data->data[temp]<0) step++;
	}
	return step;
}
/*----------------------------------------------------------------------------*/
/** FF,FR,FL,SR,SL,VR,VL */
static int next_path[DIR_MAX][DIR_CHK] = {
	/* going RIGHT */
	{ DIR_RR,DIR_BR,DIR_TR,DIR_BB,DIR_TT,DIR_BL,DIR_TL },
	/* going TOP_RIGHT */
	{ DIR_TR,DIR_RR,DIR_TT,DIR_BR,DIR_TL,DIR_BB,DIR_LL },
	/* going TOP */
	{ DIR_TT,DIR_TR,DIR_TL,DIR_RR,DIR_LL,DIR_BR,DIR_BL },
	/* going TOP_LEFT */
	{ DIR_TL,DIR_TT,DIR_LL,DIR_TR,DIR_BL,DIR_RR,DIR_BB },
	/* going LEFT */
	{ DIR_LL,DIR_TL,DIR_BL,DIR_TT,DIR_BB,DIR_TR,DIR_BR },
	/* going BOTTOM_LEFT */
	{ DIR_BL,DIR_LL,DIR_BB,DIR_TL,DIR_BR,DIR_TT,DIR_RR },
	/* going BOTTOM */
	{ DIR_BB,DIR_BL,DIR_BR,DIR_LL,DIR_RR,DIR_TL,DIR_TR },
	/* going BOTTOM_RIGHT */
	{ DIR_BR,DIR_BB,DIR_RR,DIR_BL,DIR_TR,DIR_LL,DIR_TT }
};
/*----------------------------------------------------------------------------*/
typedef struct _my1point_test_t {
	int odir, idir, find, pick, loop, prev;
	int xdir, temp, *path;
	my1ivec_t *ivec, *dest;
	my1point_t *curr, *next;
	my1point_t keep[DIR_CHK];
} my1point_test_t;
/*----------------------------------------------------------------------------*/
void point_test_init(my1point_test_t* ptest, my1ivec_t* ivec, my1ivec_t* dest) {
	ptest->ivec = ivec; /* idata vector */
	ptest->dest = dest; /* odata vector */
}
/*----------------------------------------------------------------------------*/
void point_test_prep(my1point_test_t* ptest, int init) {
	ptest->odir = init;
	ptest->xdir = -1; /* used by point_test_contour to skip dir */
}
/*----------------------------------------------------------------------------*/
int point_test_triangle_raw(int dir1, int dir2, int dir3) {
#ifdef MY1DEBUG_POINT
	printf("@@{%d|%d|%d}@@",dir1,dir2,dir3);
#endif
	switch (dir3) {
		case DIR_RR:
			if (dir2==DIR_BL&&dir1==DIR_TT) return 1;
			if (dir2==DIR_TL&&dir1==DIR_BB) return 1;
			if (dir2==DIR_TT&&dir1==DIR_BL) return 1;
			if (dir2==DIR_BB&&dir1==DIR_TL) return 1;
			break;
		case DIR_TR:
			if (dir2==DIR_LL&&dir1==DIR_BB) return 1;
			if (dir2==DIR_BB&&dir1==DIR_LL) return 1;
			break;
		case DIR_TT:
			if (dir2==DIR_BL&&dir1==DIR_RR) return 1;
			if (dir2==DIR_BR&&dir1==DIR_LL) return 1;
			if (dir2==DIR_RR&&dir1==DIR_BL) return 1;
			if (dir2==DIR_LL&&dir1==DIR_BR) return 1;
			break;
		case DIR_TL:
			if (dir2==DIR_RR&&dir1==DIR_BB) return 1;
			if (dir2==DIR_BB&&dir1==DIR_RR) return 1;
			break;
		case DIR_LL:
			if (dir2==DIR_BR&&dir1==DIR_TT) return 1;
			if (dir2==DIR_TR&&dir1==DIR_BB) return 1;
			if (dir2==DIR_TT&&dir1==DIR_BR) return 1;
			if (dir2==DIR_BB&&dir1==DIR_TR) return 1;
			break;
		case DIR_BL:
			if (dir2==DIR_RR&&dir1==DIR_TT) return 1;
			if (dir2==DIR_TT&&dir1==DIR_RR) return 1;
			break;
		case DIR_BB:
			if (dir2==DIR_TL&&dir1==DIR_RR) return 1;
			if (dir2==DIR_TR&&dir1==DIR_LL) return 1;
			if (dir2==DIR_RR&&dir1==DIR_TL) return 1;
			if (dir2==DIR_LL&&dir1==DIR_TR) return 1;
			break;
		case DIR_BR:
			if (dir2==DIR_LL&&dir1==DIR_TT) return 1;
			if (dir2==DIR_TT&&dir1==DIR_LL) return 1;
			break;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int point_test_triangle(my1point_test_t* ptest, int tmask) {
	return point_test_triangle_raw(DIR_IDIR(tmask),ptest->idir,ptest->odir);
}
/*----------------------------------------------------------------------------*/
int point_test_next(my1point_test_t* test, my1point_t* next) {
	int temp = point_test_that(test->ivec,next->x,next->y);
	if (temp<0) return 0;
	if (!test->dest->data[temp]) return 0;
	return test->ivec->data[temp];
}
/*----------------------------------------------------------------------------*/
int point_test_contour(my1point_test_t* ptest, my1point_t* point) {
	ptest->idir = ptest->odir; /* for reverse tracking */
	ptest->curr = point;
	ptest->path = next_path[ptest->odir];
	ptest->next = ptest->keep;
	ptest->find = 0; ptest->pick = -1;
	/* identify first 3 points */
	for (ptest->loop=0;ptest->loop<3;ptest->loop++,ptest->next++) {
		ptest->temp = ptest->path[ptest->loop];
		point_next(ptest->curr,ptest->next,ptest->temp);
		if (point_test_next(ptest,ptest->next)) {
			if (ptest->temp!=ptest->xdir) {
				if (ptest->pick<0) /* pick first found */
					ptest->pick = ptest->loop;
				ptest->find++;
			}
		}
	}
	if (ptest->find&&ptest->find<3) {
		/* we got a pick? done! */
		point_copy(ptest->curr,&ptest->keep[ptest->pick]);
		ptest->odir = ptest->path[ptest->pick];
		return ptest->odir;
	}
	ptest->prev = ptest->find;
	ptest->find = 0; ptest->pick = -1; /* check left and right */
	for (;ptest->loop<5;ptest->loop++,ptest->next++) {
		ptest->temp = ptest->path[ptest->loop];
		point_next(ptest->curr,ptest->next,ptest->temp);
		if (point_test_next(ptest,ptest->next)) {
			if (ptest->temp!=ptest->xdir) {
				if (ptest->pick<0) ptest->pick = ptest->loop;
				ptest->find++;
			}
		}
	}
	if (ptest->find==1) {
		if (ptest->prev==3) { /* find 0! override pick! */
			if (ptest->pick==3) ptest->pick = 2;
			else ptest->pick = 1;
		}
		point_copy(ptest->curr,&ptest->keep[ptest->pick]);
		ptest->odir = ptest->path[ptest->pick];
		return ptest->odir;
	}
	else if (ptest->find==2) {
		if (ptest->prev==0) { /* pick first found! */
			point_copy(ptest->curr,&ptest->keep[ptest->pick]);
			ptest->odir = ptest->path[ptest->pick];
			return ptest->odir;
		}
	}
	else {
		if (ptest->prev==3) {
			/* pick based on direction (0 is always 'front') */
			ptest->pick = 0;
			point_copy(ptest->curr,&ptest->keep[ptest->pick]);
			ptest->odir = ptest->path[ptest->pick];
			return ptest->odir;
		}
	}
	ptest->prev = ptest->find;
	ptest->find = 0; ptest->pick = -1; /* check final 2 */
	for (;ptest->loop<DIR_CHK;ptest->loop++,ptest->next++) {
		ptest->temp = ptest->path[ptest->loop];
		point_next(ptest->curr,ptest->next,ptest->temp);
		if (point_test_next(ptest,ptest->next)) {
			if (ptest->temp!=ptest->xdir) {
				if (ptest->pick<0) ptest->pick = ptest->loop;
				ptest->find++;
			}
		}
	}
	if (ptest->find==1) {
		if (ptest->prev==2) { /* find 0! override pick! */
			if (ptest->pick==5) ptest->pick = 4;
			else ptest->pick = 3;
		}
		point_copy(ptest->curr,&ptest->keep[ptest->pick]);
		ptest->odir = ptest->path[ptest->pick];
		return ptest->odir;
	}
	else if (ptest->find==2) {
		if (ptest->prev==0) { /* pick first found! */
			point_copy(ptest->curr,&ptest->keep[ptest->pick]);
			ptest->odir = ptest->path[ptest->pick];
			return ptest->odir;
		}
		/* prev should be 2 here! full house! ignore! */
	}
	else {
		if (ptest->prev) { /* either 2 or 0! */
			/* pick based on direction (3 is always 'right') */
			ptest->pick = 3; /* first found! */
			point_copy(ptest->curr,&ptest->keep[ptest->pick]);
			ptest->odir = ptest->path[ptest->pick];
			return ptest->odir;
		}
	}
	ptest->odir = -1;
	return ptest->odir;
}
/*----------------------------------------------------------------------------*/
int point_remove_contour(my1point_t* init, my1ivec_t* test) {
	int step, that = 0;
	while ((step=point_test_that(test,init->x,init->y))>=0) {
		if (test->data[step]<=0) break;
		/* should break if different contour id! */
		if (!that) that = DIR_DECODE(test->data[step]);
		else if (DIR_DECODE(test->data[step])!=that) break;
#ifdef MY1DEBUG_POINT
		printf("@@ [REMOVE] (%d,%d)[%d:%d@%d] ",init->x,init->y,
			step,test->data[step],(test->data[step]&DIR_MASK));
#endif
		point_next(init,init,(test->data[step]&DIR_MASK));
#ifdef MY1DEBUG_POINT
		printf("=> (%d,%d)\n",init->x,init->y);
#endif
		test->data[step] = 0;
	}
	return step;
}
/*----------------------------------------------------------------------------*/
typedef struct _my1point_full_t {
	int xcol,yrow;
	int ichk,cols;
} my1point_full_t;
/*----------------------------------------------------------------------------*/
void point_full_make(my1point_full_t* that, int x, int y) {
	that->xcol = x;
	that->yrow = y;
}
/*----------------------------------------------------------------------------*/
void point_full_ical(my1point_full_t* that) {
	that->ichk = (that->yrow*that->cols)+that->xcol;
}
/*----------------------------------------------------------------------------*/
void point_full_copy(my1point_full_t* that, my1point_full_t* from) {
	that->xcol = from->xcol;
	that->yrow = from->yrow;
	that->ichk = from->ichk;
	that->cols = from->cols;
}
/*----------------------------------------------------------------------------*/
void point_make_full(my1point_full_t* that, int x, int y) {
	point_full_make(that,x,y);
	point_full_ical(that);
}
/*----------------------------------------------------------------------------*/
typedef void (*psave_t)(my1ivec_t*,void*,int,int,int,int);
/*----------------------------------------------------------------------------*/
typedef struct _my1point_find_t {
	int step, size, path, temp, find;
	int tchk; /* temp check value */
	int pmin; /* minimum point */
	int offs; /* offset for step (connected multiple contour loops) */
	int flag; /* general purpose bitwise flag */
	int st_size, st_rows, st_cols; /* used by point_find_steptrace */
	my1ivec_scan_t scan;
	my1point_full_t curr, prev, keep, xtra;
	my1point_t *pcurr, *pprev, *pkeep, *pxtra;
	my1point_test_t test;
	psave_t save;
	void* data;
} my1point_find_t;
/*----------------------------------------------------------------------------*/
typedef my1point_find_t my1pfind_t;
/*----------------------------------------------------------------------------*/
void point_find_init(my1point_find_t* find, psave_t save, void* data) {
	find->save = save;
	find->data = data;
	find->pcurr = (my1point_t*)&find->curr;
	find->pprev = (my1point_t*)&find->prev;
	find->pkeep = (my1point_t*)&find->keep;
	find->pxtra = (my1point_t*)&find->xtra;
	find->pmin = CONTOUR_MIN_POINTS;
	find->flag = 0;
}
/*----------------------------------------------------------------------------*/
void point_find_prep(my1point_find_t* find,
		my1ivec_t* ivec, my1ivec_t* dest) {
	find->curr.cols = ivec->cols;
	find->prev.cols = ivec->cols;
	find->keep.cols = ivec->cols;
	find->xtra.cols = ivec->cols;
	point_test_init(&find->test,ivec,dest);
}
/*----------------------------------------------------------------------------*/
int point_find_steptrace(my1pfind_t* find, my1point_t* init) {
	int step, loop, xsav, ysav, xmin, ymin, xmax, ymax;
	my1ivec_t* dest = find->test.dest;
	step = 0; xsav = init->x; ysav = init->y;
	xmin = init->x;
	ymin = init->y;
	xmax = init->x;
	ymax = init->y;
	find->st_size = 0;
	find->st_rows = 0;
	find->st_cols = 0;
	while ((loop=point_test_that(dest,init->x,init->y))>=0) {
		if (dest->data[loop]<=0)
			break;
		if (init->x>xmax) xmax = init->x;
		else if (init->x<xmin) xmin = init->x;
		if (init->y>ymax) ymax = init->y;
		else if (init->y<ymin) ymin = init->y;
		point_next(init,init,(dest->data[loop]&DIR_MASK));
		step++;
		if (init->x==xsav&&init->y==ysav)
			break;
	}
	init->x = xsav;
	init->y = ysav;
	if (step>0) {
		find->st_size = step;
		find->st_rows = ymax-ymin+1;
		find->st_cols = xmax-xmin+1;
	}
	return step;
}
/*----------------------------------------------------------------------------*/
#ifdef MY1DEBUG_POINT
/*----------------------------------------------------------------------------*/
/*
 * NOTE: inline functions need to be static so that it will not be removed
 * by compiler optimization stage
**/
static inline void print_find_pick(my1pfind_t* find) {
	printf("##doPICK[%d:%d]",find->test.pick,find->test.path[find->test.pick]);
	printf("@(%d,%d)##",find->test.keep[find->test.pick].x,
		find->test.keep[find->test.pick].y);
}
/*----------------------------------------------------------------------------*/
static inline void print_find_hood(my1pfind_t* find, my1ivec_t *iref) {
	printf("##opHOOD(%d,%d)[f:%d,p:%d,x:%d]{",
		find->test.curr->x,find->test.curr->y,
		find->test.find,find->test.pick,find->test.xdir);
	for (find->test.loop=0;find->test.loop<DIR_CHK;find->test.loop++) {
		find->test.temp = point_test_that(iref,
			find->test.keep[find->test.loop].x,
			find->test.keep[find->test.loop].y);
		if (find->test.loop) putchar(',');
		find->test.temp =
			find->test.temp<0?-2:iref->data[find->test.temp];
		printf("[%d]%d,%d:%d",find->test.path[find->test.loop],
			find->test.keep[find->test.loop].x,
			find->test.keep[find->test.loop].y,find->test.temp);
		if (find->test.loop==find->test.pick)
			break;
	}
	printf("}##");
}
/*----------------------------------------------------------------------------*/
static inline void print_find_status(my1pfind_t* find) {
	printf("{Step:%d,Size:%d}",find->step,find->size);
}
/*----------------------------------------------------------------------------*/
static inline void print_test_status(my1pfind_t* find) {
	printf("{x:%d,i:%d,o:%d}",find->test.xdir,find->test.idir,find->test.odir);
}
/*----------------------------------------------------------------------------*/
static inline void print_full_chkdir(int cdir) {
	printf("{%x:%d|%x|%d|%d}",cdir,DIR_DECODE(cdir),DIR_FLAG(cdir),
		DIR_IDIR(cdir),DIR_ODIR(cdir));
}
/*----------------------------------------------------------------------------*/
static inline void print_full_point(my1point_full_t *pick) {
	printf("(%d,%d)",pick->xcol,pick->yrow);
	printf("[%d]",pick->ichk);
#if 0
#endif
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
int point_find_marktrace(my1pfind_t* find, my1point_t* init) {
	int size, that, mark, loop, xsav, ysav;
	my1ivec_t* dest = find->test.dest;
#ifdef MY1DEBUG_POINT
	my1point_full_t* test = (my1point_full_t*) init;
#endif
	size = 0; that = 0; mark = ++find->offs;
	xsav = init->x; ysav = init->y;
	while ((loop=point_test_that(dest,init->x,init->y))>=0) {
		if (dest->data[loop]<=0) {
			/* not possible? */
#ifdef MY1DEBUG_POINT
			printf("** Lost? (%d,%d)->(%d,%d)\n",init->x,init->y,xsav,ysav);
#endif
			point_make(init,xsav,ysav);
			point_remove_contour(init,dest);
			mark = 0;
			break;
		}
#ifdef MY1DEBUG_POINT
		test->ichk = loop;
		printf("-- [MARKCON] %s ",size?"Next":"Init");
		print_full_point(test);
		print_full_chkdir(dest->data[loop]);
		printf("{Size:%d}\n",size);
#endif
		point_next(init,init,(dest->data[loop]&DIR_MASK));
		if (!that) {
			that = DIR_DECODE(dest->data[loop]);
			mark += that;
		}
		else if (DIR_DECODE(dest->data[loop])!=that) {
#ifdef MY1DEBUG_POINT
			printf("** Diff? (%d<=>%d)\n",DIR_DECODE(dest->data[loop]),that);
#endif
			point_make(init,xsav,ysav);
			point_remove_contour(init,dest);
			mark = 0;
			break;
		}
		/* update new mark! */
		dest->data[loop] = DIR_RECODE(dest->data[loop],mark);
		size++; find->size--;
		if (init->x==xsav&&init->y==ysav) {
#ifdef MY1DEBUG_POINT
			printf("-- [MARKCON] Done (%d,%d)[%d(%d+%d):%d]\n",
				init->x,init->y,mark,find->step,find->offs,size);
#endif
			if (find->save)
				find->save(dest,find->data,init->y,init->x,mark,size);
			break;
		}
	}
	return mark;
}
/*----------------------------------------------------------------------------*/
int point_find_backtrace(my1pfind_t* find) {
	int done, temp;
	my1ivec_t *dest = find->test.dest;
	done = find->size>0&&dest->data[find->prev.ichk]>0?0:1;
	while (!done) {
		/* backtrace to initial point / find a branch */
		find->size--;
		find->temp = dest->data[find->prev.ichk];
		find->test.xdir = DIR_ODIR(find->temp);
		find->test.odir = DIR_IDIR(find->temp);
		point_full_copy(&find->curr,&find->prev);
#ifdef MY1DEBUG_POINT
		printf("@@ [BKTRACE] Back ");
		print_full_point(&find->prev);
#endif
		if (find->size>1&& /* if only 1 left, we are done! */
				point_test_contour(&find->test,find->pcurr)>=0) {
#ifdef MY1DEBUG_POINT
			print_test_status(find);
#if 0
			print_find_hood(find,dest);
#endif
#endif
			/* get prev-prev point */
			point_prev(find->pprev,find->pkeep,DIR_IDIR(find->temp));
			point_full_ical(&find->keep);
			find->tchk = dest->data[find->keep.ichk];
#ifdef MY1DEBUG_POINT
			if (find->tchk<0) {
				/* should NOT happen! prev point guranteed? find->size>1 */
				printf("**[ERROR]prev@");
				print_full_point(&find->keep);
				print_full_chkdir(find->tchk);
				printf("**");
			}
#endif
			temp = find->tchk;
			/* get new next point */
			point_next(find->pprev,find->pkeep,find->test.odir);
			point_full_ical(&find->keep);
			find->tchk = dest->data[find->keep.ichk];
#ifdef MY1DEBUG_POINT
			printf("{next@");
			print_full_point(&find->keep);
			print_full_chkdir(find->tchk);
			printf("(temp:%d)(mark:%d)(ttri:%d)}",temp,
				find->tchk&DIR_FLAG_MARK?1:0,
				point_test_triangle(&find->test,temp));
#if 0
#endif
#endif
			/* must be unmarked pixel & avoid triangle! */
			if ((find->tchk<0||!(find->tchk&DIR_FLAG_MARK))&&
					!point_test_triangle(&find->test,temp)) {
#ifdef MY1DEBUG_POINT
				print_test_status(find);
				print_find_status(find);
				printf("\n");
#endif

				point_full_copy(&find->keep,&find->prev);
				break;
			}
		}
		/* remove point */
		dest->data[find->prev.ichk] = 0;
#ifdef MY1DEBUG_POINT
		printf("@@REMOVED!@@");
#endif
		if (!find->size) done = 1;
		else {
			point_prev(find->pprev,find->pprev,DIR_IDIR(find->temp));
			point_full_ical(&find->prev);
#ifdef MY1DEBUG_POINT
			print_full_chkdir(find->temp);
			printf("=>");
			print_full_point(&find->prev);
#endif
			if (dest->data[find->prev.ichk]<=0) {
				/* should NOT happen! */
#ifdef MY1DEBUG_POINT
				printf(" => HUH??? (%d)",dest->data[find->prev.ichk]);
#endif
				done = 1;
			}
		}
#ifdef MY1DEBUG_POINT
		printf("\n");
#endif
	}
	return find->size;
}
/*----------------------------------------------------------------------------*/
int point_find_undo(my1pfind_t* find) {
	if (point_find_backtrace(find)>0) {
		point_full_copy(&find->curr,&find->prev);
		find->temp = find->test.dest->data[find->prev.ichk];
		find->test.odir = DIR_IDIR(find->temp);
#ifdef MY1DEBUG_POINT
		if (find->test.odir<0||find->test.odir>DIR_CHK) {
			printf("** [ERROR] Invalid DIR_ODIR (%d)<-[%x]\n",
				find->test.odir,find->temp);
		}
#endif
		point_prev(find->pprev,find->pprev,find->test.odir);
		point_full_ical(&find->prev);
#ifdef MY1DEBUG_POINT
		printf("@@ [CHKUNDO] ");
		print_full_point(&find->curr);
		print_full_chkdir(find->test.dest->data[find->curr.ichk]);
		printf(">[dir:%d]>",find->test.odir);
		print_full_point(&find->prev);
		print_full_chkdir(find->test.dest->data[find->prev.ichk]);
		printf("\n");
#endif
	}
	return find->size;
}
/*----------------------------------------------------------------------------*/
int point_find_done(my1pfind_t* find) {
#ifdef MY1DEBUG_POINT
	printf("-- [CONTOUR] Done ");
	print_full_point(&find->curr);
	print_find_status(find);
	printf("\n");
#endif
	if (find->save)
		find->save(find->test.dest,find->data,
			find->scan.irow,find->scan.icol,find->step,find->size);
	find->find = 1;
	find->step++;
	if (find->offs) {
		find->step += find->offs;
		find->offs = 0;
	}
	return find->step;
}
/*----------------------------------------------------------------------------*/
int point_find_contour(my1pfind_t* find, my1ivec_t *ivec, my1ivec_t *dest) {
	my1ivec_scan_t *scan = &find->scan;
	scan_init(scan,ivec->data,ivec->rows,ivec->cols,CONTOUR_BORDER_SKIP);
	scan_prep(scan);
	ivec_make(dest,ivec->rows,ivec->cols);
	ivec_fill(dest,-1);
	point_find_prep(find,ivec,dest);
	find->step = 1; /* 1-based index */
	find->offs = 0;
	while (scan_next(scan)) {
		/* skip if already scanned */
		if (dest->data[scan->loop]>=0) continue;
		/* skip frame borders, background and non-edge pixels */
		if (scan_skip(scan)||!ivec->data[scan->loop]||
				scan_8connected(scan)==8) {
			dest->data[scan->loop] = 0;
			continue;
		}
		/* scan contour */
		find->find = 0; find->size = 0;
		/* prepare test */
		find->curr.ichk = scan->loop;
		find->curr.xcol = scan->icol;
		find->curr.yrow = scan->irow;
		point_full_copy(&find->prev,&find->curr);
		point_test_prep(&find->test,DIR_RIGHT);
		do { /* INIT: scan contour loop */
			/* keep a copy for prev to take */
			point_full_copy(&find->keep,&find->curr);
			/* this will update curr! */
			point_test_contour(&find->test,find->pcurr);
			find->test.xdir = -1; /* just in case! */
			if (find->test.odir<0) {
				/* mark output */
				dest->data[find->curr.ichk] = 0;
				/* ignore single point noise! */
				if (!find->size) break;
#ifdef MY1DEBUG_POINT
				printf("@@ [CHKHOOD] None ");
				print_full_point(&find->curr);
				print_find_hood(find,ivec);
				printf("\n");
#endif
				/* backtrace to initial point / find a branch */
				point_find_backtrace(find);
				/* quit if we cannot find a new branch */
				if (!find->size) {
#ifdef MY1DEBUG_POINT
					printf("@@ BREAK (ZERO!)\n");
#endif
					break;
				}
			}
			/* check for invalid path (should not happen!) */
			if (find->test.odir>DIR_CHK||find->test.idir>DIR_CHK||
					find->test.odir<0||find->test.idir<0) {
#ifdef MY1DEBUG_POINT
				printf("** [ERROR] InvalidDir@");
				print_full_point(&find->curr);
				print_test_status(find);
				print_find_status(find);
				printf("\n");
#endif
				/* backtrace */
				point_find_backtrace(find);
#ifdef MY1DEBUG_POINT
				if (find->size)
					printf("** BREAK (ERROR!)\n");
#endif
				break;
			}
#ifdef MY1DEBUG_POINT
			printf("%s-- [CONTOUR] ",find->size?"":"@@ START\n");
			printf("%s ",find->size?"Next":"Init");
			print_full_point(&find->keep);
			print_find_pick(find);
#if 0
			print_find_hood(find,dest);
#endif
			print_test_status(find);
			print_find_status(find);
			printf("\n");
#endif
			/* save info in current pixel */
			dest->data[find->keep.ichk] = DIR_ENCODE(find->test.odir,
				find->test.idir,find->step);
			find->size++;
			/* update prev */
			point_full_copy(&find->prev,&find->keep);
			/* update curr index */
			point_full_ical(&find->curr);
			/* check this next point */
			if (scan_skip_test(scan,find->curr.xcol,find->curr.yrow)) {
				/* should not consider skipped scan border! */
#ifdef MY1DEBUG_POINT
				printf("@@ [CONTOUR] Border@");
				print_full_point(&find->curr);
				print_full_chkdir(dest->data[find->curr.ichk]);
				printf("\n");
#endif
				/* mark border pixels as 0 */
				dest->data[find->curr.ichk] = 0;
				/* quit if we cannot find a new branch */
				if (!point_find_undo(find)) {
#ifdef MY1DEBUG_POINT
					printf("@@ BREAK (BORDER!)\n");
#endif
					break;
				}
			}
			find->temp = dest->data[find->curr.ichk];
			if (find->temp>0) { /* process if dest is already marked */
				if (find->curr.ichk==scan->loop) {
					/* back to init -> closed contour! */
					if (find->size<find->pmin) {
#ifdef MY1DEBUG_POINT
						printf("-- [CONTOUR] Undo ");
						print_full_point(&find->curr);
						print_find_status(find);
						printf("\n");
#endif
						if (point_find_undo(find)) continue;
					}
					else point_find_done(find);
				}
				else if (DIR_DECODE(find->temp)!=find->step) {
					/* this belongs to another countour! */
#ifdef MY1DEBUG_POINT
					printf("** [CONTOUR] Overlap ");
					print_full_point(&find->curr);
					print_full_chkdir(find->temp);
					print_find_status(find);
					printf("\n");
#endif
					/* mark this pixel */
					dest->data[find->curr.ichk] |= DIR_FLAG_MARK;
					if (point_find_undo(find)) continue;
				}
				else if (point_find_steptrace(find,find->pcurr)>find->pmin) {
					/* found own contour, but not at initial point */
					point_prev(find->pcurr,find->pprev,DIR_IDIR(find->temp));
					point_full_ical(&find->prev);
#ifdef MY1DEBUG_POINT
					printf("@@ [DOSPLIT] ");
					print_full_point(&find->curr);
					print_full_chkdir(dest->data[find->curr.ichk]);
					printf(">[dir:%d]>",DIR_IDIR(find->temp));
					print_full_point(&find->prev);
					print_full_chkdir(dest->data[find->prev.ichk]);
					printf("\n");
#endif
					/* mark this pixel */
					dest->data[find->curr.ichk] |= DIR_FLAG_MARK;
					/* mark this loop! */
					point_full_copy(&find->keep,&find->curr);
					point_find_marktrace(find,find->pkeep);
					/* backtrace */
					point_full_copy(&find->keep,&find->curr);
					if (point_find_undo(find)) continue;
				}
				else {
					/* too small to consider a loop! */
#ifdef MY1DEBUG_POINT
					printf("@@ [DOBREAK] ");
					print_full_point(&find->curr);
					printf("{%d<%d}\n",find->st_size,find->pmin);
#endif
					/* mark this pixel */
					dest->data[find->curr.ichk] |= DIR_FLAG_MARK;
					/* save current point - just in case... */
					point_full_copy(&find->keep,&find->curr);
					/* backtrace */
					if (point_find_undo(find)) continue;
				}
#ifdef MY1DEBUG_POINT
				printf("@@ BREAK (DONE!)\n");
#endif
				/* whatever the case, we are done */
				break;
			}
		} while (!find->find); /* ENDS: scan contour loop */
	}
	return --find->step; /* because find->step is a 1-based index */
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
