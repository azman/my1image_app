/*----------------------------------------------------------------------------*/
/**
 * shaped.c
 * - tool to manage a shaped db file
 * - default: prints magnitude values of shaped items in columns to stderr
 *   = first column is index, first row is shape name
 *   = also shows basic min/max/avg info on stdout
 * - opt{pick}:
 *   = exract single shape binary file (base-1 index)
 * - opt{average}:
 *   = calculate average and write to a vector file
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "shaped_db.h"
#include "shaped_file.h"
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR (~(~0U>>1))
#define STAT_ERROR_ARGS (STAT_ERROR|0x01)
#define STAT_ERROR_LOAD (STAT_ERROR|0x02)
#define STAT_ERROR_SAVE (STAT_ERROR|0x04)
#define STAT_ERROR_WORK (STAT_ERROR|0x08)
/*----------------------------------------------------------------------------*/
#define REAL_FORMAT ",%.12lf"
/*----------------------------------------------------------------------------*/
#define FLAG_DO_VERBOSE  0x0001
#define FLAG_DO_DISTANCE 0x0002
#define FLAG_DO_BANDSIZE 0x0004
/*----------------------------------------------------------------------------*/
int file_exists(char* name) {
	FILE* test;
	int iret;
	iret = 0;
	test = fopen(name,"r");
	if (test) {
		iret++;
		fclose(test);
	}
	return iret;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	unsigned int flag, stat;
	int loop, test, size, skip;
	int rmax, rmin, ravg, pick;
	char* ptmp;
	char *pdat, *padd, *name, *nsum;
	my1list_t *list;
	my1item_t *item;
	my1shaped_t *con1, *con2;
	double *dval;
	int chk1, chk2;
	my1vector_dist_t dist;
	my1shaped_db_t that;
	my1shaped_t cont, buff, *pcon;
	/* setup */
	shaped_db_init(&that);
	shaped_init(&cont);
	shaped_init(&buff);
	vector_dist_default(&dist);
	do {
		flag = 0; stat = STAT_OK;
		size = 0; skip = 0; pick = 0;
		pdat = 0x0; padd = 0x0; name = 0x0; nsum = 0x0;
		/* get options */
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
				ptmp = &argv[loop][2];
				if (!strncmp(ptmp,"add",4)) {
					if (++loop<argc) padd = argv[loop];
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"name",5)) {
					if (++loop<argc) name = argv[loop];
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"average",8)) {
					if (++loop<argc) nsum = argv[loop];
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"skip",5)) {
					if (++loop<argc) {
						test = atoi(argv[loop]);
						if (test>0) skip = test;
					}
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"size",5)) {
					if (++loop<argc) {
						test = atoi(argv[loop]);
						if (test>0) size = test;
					}
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"band",5))
					flag |= FLAG_DO_BANDSIZE;
				else if (!strncmp(ptmp,"pick",5)) {
					if (++loop<argc) {
						test = atoi(argv[loop]);
						if (test>0) pick = test;
					}
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"dist-offs",10)) {
					if (++loop<argc) dist.offs = atoi(argv[loop]);
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"dist-stop",10)) {
					if (++loop<argc) dist.stop = atoi(argv[loop]);
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"dist-avg",9)) {
					dist.calc = vector_distance_avg;
					flag |= FLAG_DO_DISTANCE;
				}
				else if (!strncmp(ptmp,"dist-rms",9)) {
					dist.calc = vector_distance_rms;
					flag |= FLAG_DO_DISTANCE;
				}
				else if (!strncmp(ptmp,"verbose",8))
					flag |= FLAG_DO_VERBOSE;
				else {
					fprintf(stderr,"** Invalid option '%s'!\n",argv[loop]);
					stat |= STAT_ERROR_ARGS;
				}
			}
			else {
				/* non-parameter */
				if (!pdat) pdat = argv[loop];
				else {
					fprintf(stderr,"** Invalid argument '%s'!\n",argv[loop]);
					stat |= STAT_ERROR_ARGS;
				}
			}
		}
		if (stat) break;
		/* check input */
		if (!pdat) {
			fprintf(stderr,"** No shape database file given!\n");
			stat |= STAT_ERROR_ARGS;
			break;
		}
		/* load db or create new */
		if (file_exists(pdat)) {
			if (flag&FLAG_DO_VERBOSE)
				printf("-- Loading data...\n");
			if (shaped_db_load(&that,pdat)<0) {
				fprintf(stderr,"** Error loading shape db '%s'!\n",pdat);
				stat |= STAT_ERROR_LOAD;
				break;
			}
		}
		else shaped_db_name(&that,pdat);
		if (padd) {
			if (flag&FLAG_DO_VERBOSE)
				printf("-- Loading shape...\n");
			if (shaped_load(&cont,padd,name)) {
				fprintf(stderr,"** Error loading shape '%s'!\n",padd);
				stat |= STAT_ERROR_LOAD;
				break;
			}
			pcon = &cont;
			if (size>0||skip>0) {
				test = pcon->desc.fill;
				if (size>0) test = size;
				if (flag&FLAG_DO_VERBOSE) {
					if (flag&FLAG_DO_BANDSIZE)
						printf("@@ {BandSize:%d,Skip:%d%s}\n",size,skip,
							skip?"Ignored":"");
					else
						printf("@@ {Skip:%d,Size:%d}\n",skip,size);
					printf("-- Trim descriptor (%d->",pcon->desc.fill);
				}
				if (flag&FLAG_DO_BANDSIZE)
					shaped_copy_band(&buff,pcon,test);
				else
					shaped_copy_some(&buff,pcon,skip,test);
				pcon = &buff;
				if (flag&FLAG_DO_VERBOSE)
					printf("%d)\n",pcon->desc.fill);
			}
			name = pcon->name.buff;
			if ((con1=shaped_db_search(&that,name))) {
				fprintf(stderr,"** Error shape '%s' already in db! (%s)\n",
					name,con1->name.buff);
				stat |= STAT_ERROR_WORK;
			}
			else if (shaped_db_append(&that,pcon)) {
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Shape '%s' added to DB\n",name);
				if (shaped_db_save(&that,pdat)) {
					fprintf(stderr,"** Error saving shape db '%s'!\n",pdat);
					stat |= STAT_ERROR_SAVE;
				}
				else if (flag&FLAG_DO_VERBOSE)
					printf("-- Data saved to '%s'!\n",pdat);
			}
			else {
				fprintf(stderr,"** Error adding shape '%s'!\n",name);
				stat |= STAT_ERROR_WORK;
			}
			break; /* when adding, not doing anything else */
		}
		/* make sure data is available */
		list = &that.list;
		if (list->size>0) {
			if (flag&FLAG_DO_DISTANCE) {
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Calculating distance...\n");
				size = list->size; /* size can be reused */
				/* display header line */
				fprintf(stderr,"D-MAT");
				list->curr = 0x0;
				while (list_scan_item(list)) {
					con1 = (my1shaped_t*)list->curr->data;
					fprintf(stderr,",%s",con1->name.buff);
				}
				fprintf(stderr,"\n");
				/* calculate distance and display */
				dval = shaped_db_distance_matrix(&that,&dist);
				if (dval) {
					item = list->init;
					for (loop=0,chk1=0;chk1<size;chk1++) {
						/* column0 - name */
						con1 = (my1shaped_t*)item->data;
						fprintf(stderr,"%s",con1->name.buff);
						for (chk2=0;chk2<size;chk2++,loop++)
							fprintf(stderr,REAL_FORMAT,dval[loop]);
						fprintf(stderr,"\n");
						item = item->next;
					}
					free((void*)dval);
				}
				else fprintf(stderr,"** Cannot calc distance?!\n");
			}
			else { /* by default, print descriptor magnitude to stderr */
				/* print header line and get stat */
				fprintf(stderr,"Index");
				rmax = 0; rmin = 0; ravg = 0;
				that.list.curr = 0x0;
				while (list_scan_item(&that.list)) {
					con1 = (my1shaped_t*)that.list.curr->data;
					chk1 = con1->desc.fill;
					if (!rmax||chk1>rmax)
						rmax = chk1;
					if (!rmin||chk1<rmin)
						rmin = chk1;
					ravg += chk1;
					fprintf(stderr,",%s",con1->name.buff);
				}
				fprintf(stderr,"\n");
				ravg /= that.list.step; /* get average */
				/* print data per index - print magnitude only*/
				for (loop=0;loop<rmax;loop++) {
					fprintf(stderr,"%d",loop);
					that.list.curr = 0x0;
					while (list_scan_item(&that.list)) {
						con1 = (my1shaped_t*)that.list.curr->data;
						if (loop<con1->desc.fill) {
							fprintf(stderr,REAL_FORMAT,
								complex_magnitude(&con1->desc.data[loop]));
						}
						else fprintf(stderr,",");
					}
					fprintf(stderr,"\n");
				}
				printf("-- DB Size:%d ",that.list.size);
				printf("(Stat:{min=%d,max=%d,avg=%d})\n",rmin,rmax,ravg);
			}
			if (nsum) {
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Calculating descriptor average...\n");
				con1 = &cont;
				con2 = (my1shaped_t*)list->init->data;
				size = con2->desc.fill;
				shaped_make(con1,that.name.buff,size);
				con1->desc.fill = 0;
				vector_fill(SVEC(con1),0.0,0.0);
				/* desriptor average */
				list->curr = 0x0;
				while (list_scan_item(list)) {
					con2 = (my1shaped_t*)list->curr->data;
					if (con2->desc.fill!=size) {
						fprintf(stderr,"** Invalid size! (%d/%d)\n",
							con2->desc.fill,size);
						stat |= STAT_ERROR_WORK;
						break;
					}
					vector_summation(SVEC(con1),SVEC(con2));
				}
				if (stat) break;
				vector_scale(SVEC(con1),(double)1/list->size);
				if (vector_save_bin(SVEC(con1),nsum)) {
					fprintf(stderr,"** Error saving vector '%s'!\n",nsum);
					stat |= STAT_ERROR_SAVE;
					break;
				}
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Saved averaged data to '%s'\n",nsum);
			}
			if (pick>0) {
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Picked shaped #%d\n",pick);
				pick--; /* base-1 index */
				that.list.curr = 0x0; con1 = 0x0;
				while (list_scan_item(&that.list)) {
					if (that.list.step==pick) {
						con1 = (my1shaped_t*)that.list.curr->data;
						break;
					}
				}
				if (!con1) {
					fprintf(stderr,"** Invalid pick! (%d/%d)\n",++pick,
						that.list.size);
					stat |= STAT_ERROR_WORK;
				}
				else {
					cstr_append(&con1->name,".bin");
					name = con1->name.buff;
					if (vector_save_bin(SVEC(con1),name)) {
						fprintf(stderr,"** Error saving shape '%s'!\n",name);
						stat |= STAT_ERROR_WORK;
					}
					else if (flag&FLAG_DO_VERBOSE)
						printf("-- Saved shape to '%s'\n",name);
				}
			}
		}
	}
	while (0);
	/* cleanup */
	shaped_free(&buff);
	shaped_free(&cont);
	shaped_db_free(&that);
	return stat;
}
/*----------------------------------------------------------------------------*/
