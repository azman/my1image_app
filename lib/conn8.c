/*----------------------------------------------------------------------------*/
#include "conn8.h"
#include "ivec_file.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, test, mask;
	my1ivec_t load, buff, *ivec;
	ivec_init(&load);
	ivec_init(&buff);
	do {
		stat = 0; mask = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		for (loop=2;loop<argc;loop++) {
			if (!strncmp(argv[loop],"--mask",7))
				mask = atoi(argv[++loop]);
			else
				printf("** Unknown param '%s'!\n",argv[loop]);
		}
		test = ivec_load(&load,argv[1]);
		if (!test) {
			printf("** Cannot load '%s'!\n",argv[1]);
			stat = -2;
			break;
		}
		ivec = &load;
		printf("-- Load:\n");
		ivec_show(ivec);
		conn8_scan(&buff,ivec,mask);
		ivec = &buff;
		printf("-- Data:\n");
		ivec_show(ivec);
	} while (0);
	ivec_free(&buff);
	ivec_free(&load);
	return stat;
}
/*----------------------------------------------------------------------------*/
