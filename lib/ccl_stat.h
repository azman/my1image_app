/*----------------------------------------------------------------------------*/
#ifndef __MY1CCL_STAT_H__
#define __MY1CCL_STAT_H__
/*----------------------------------------------------------------------------*/
#define LABEL_SIZE 256
/*----------------------------------------------------------------------------*/
#define CCLFLAG_ONBORDER 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1ccl_stat_t {
	int mark, size;
	unsigned int flag, stat;
} my1ccl_stat_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1ccl_t {
	my1ccl_stat_t stat[LABEL_SIZE];
	int step, size, ilab, imax, ichk, rsv0;
	/** my1ccl_t predates my1ivec_t! */
	int *temp, *curr, *data, rows, cols;
} my1ccl_t;
/*----------------------------------------------------------------------------*/
void ccl_init(my1ccl_t* pccl, int* data, int rows, int cols) {
	int loop;
	for (loop=0;loop<LABEL_SIZE;loop++) {
		pccl->stat[loop].mark = 0;
		pccl->stat[loop].size = 0;
		pccl->stat[loop].flag = 0;
		pccl->stat[loop].stat = 0; /* stat as 'next' => linked-list! */
	}
	pccl->step = 2; /* initial label! */
	pccl->size = 0;
	pccl->ilab = -1;
	pccl->imax = 0;
	pccl->ichk = -1;
	pccl->data = data;
	pccl->rows = rows;
	pccl->cols = cols;
}
/*----------------------------------------------------------------------------*/
void ccl_mark(my1ccl_t* pccl, int mark) {
	pccl->size++; /** mark > 1 */
	pccl->stat[mark].mark = mark;
}
/*----------------------------------------------------------------------------*/
void ccl_more(my1ccl_t* pccl, int mark) {
	pccl->stat[mark].size++;
}
/*----------------------------------------------------------------------------*/
void ccl_less(my1ccl_t* pccl, int mark) {
	pccl->stat[mark].size--;
}
/*----------------------------------------------------------------------------*/
void ccl_stat(my1ccl_t* pccl) {
	int loop, test, prev, curr;
	for (loop=2,test=0;loop<LABEL_SIZE;loop++) {
		if (pccl->stat[loop].mark) {
			/* sorted list */
			curr = pccl->stat[0].stat; prev = 0;
			while (curr) {
				if (pccl->stat[curr].size<pccl->stat[loop].size)
					break;
				/* look next in list if smaller or equal */
				prev = curr;
				curr = pccl->stat[curr].stat;
			}
			/* take this spot */
			pccl->stat[prev].stat = loop;
			if (curr) pccl->stat[loop].stat = curr;
			test++; if (test>=pccl->size) break;
		}
	}
	/* stat max */
	curr = pccl->stat[0].stat;
	if (curr) {
		pccl->imax = pccl->stat[curr].size;
		pccl->ilab = pccl->stat[curr].mark;
		pccl->ichk = curr;
	}
}
/*----------------------------------------------------------------------------*/
void ccl_flag_border(my1ccl_t* pccl, int x1, int x2, int y1, int y2) {
	int *vec1, *vec2;
	int loop, mark;
	y1 *= pccl->cols;
	y2 *= pccl->cols;
	vec1 = &pccl->data[y1];
	vec2 = &pccl->data[y2];
	for (loop=x1;loop<=x2;loop++) {
		mark = vec1[loop];
		if (mark>1) pccl->stat[mark].flag |= CCLFLAG_ONBORDER;
		mark = vec2[loop];
		if (mark>1) pccl->stat[mark].flag |= CCLFLAG_ONBORDER;
	}
	for (loop=y1;loop<=y2;loop+=pccl->cols) {
		mark = vec1[x1];
		if (mark>1) pccl->stat[mark].flag |= CCLFLAG_ONBORDER;
		mark = vec1[x2];
		if (mark>1) pccl->stat[mark].flag |= CCLFLAG_ONBORDER;
		vec1 += pccl->cols;
	}
}
/*----------------------------------------------------------------------------*/
void ccl_wipe_edge(my1ccl_t* pccl) {
	int curr, prev;
	curr = pccl->stat[0].stat; prev = 0;
	while (curr&&pccl->stat[curr].flag&CCLFLAG_ONBORDER) {
		prev = curr;
		curr = pccl->stat[curr].stat;
	}
	if (!curr) curr = prev;
	if (curr&&curr!=pccl->ilab) {
		pccl->imax = pccl->stat[curr].size;
		pccl->ilab = pccl->stat[curr].mark;
		pccl->ichk = curr;
	}
}
/*----------------------------------------------------------------------------*/
void ccl_min_suppress(my1ccl_t* pccl, int pmin) {
	int loop, test, irow, icol;
	if (pmin<=0) return;
	test = pccl->stat[0].stat; loop = 0;
	while (test) {
		if (pccl->stat[test].size<pmin) {
			pccl->stat[test].size = 0;
			pccl->stat[loop].stat = 0; /* remove from list */
		}
		loop = test;
		test = pccl->stat[test].stat;
	}
	for (irow=0,loop=0;irow<pccl->rows;irow++) {
		for (icol=0;icol<pccl->cols;icol++,loop++) {
			if (pccl->data[loop]) {
				test = pccl->data[loop];
				if (!pccl->stat[test].size)
					pccl->data[loop] = 0;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void ccl_max_suppress(my1ccl_t* pccl) {
	int irow, icol,loop;
	for (irow=0,loop=0;irow<pccl->rows;irow++) {
		for (icol=0;icol<pccl->cols;icol++,loop++) {
			if (pccl->data[loop]!=pccl->ilab)
				pccl->data[loop] = 0;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
