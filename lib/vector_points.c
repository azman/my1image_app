/*----------------------------------------------------------------------------*/
/**
 * vector_points.c
 * - simple program to test functions in vector_points.h
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "points_file.h"
#include "vector_file.h"
#include "vector_points.h"
/*----------------------------------------------------------------------------*/
#define FLAG_VECTORDATA 0x01
#define FLAG_DOSAVE_BIN 0x02
#define FLAG_SAMPLE_PNT 0x04
/*----------------------------------------------------------------------------*/
#define STAT_ERROR_ARGS 0x0001
#define STAT_ERROR_LOAD 0x0002
#define STAT_ERROR_SAVE 0x0004
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, test;
	unsigned int flag, stat;
	char* ptmp, *psrc, *pdst;
	pmakev_t makevec;
	pfromv_t fromvec;
	my1points_t inpp, prop, *usep;
	my1vector_t inpv, prov;
	/* initialize stuff */
	points_init(&inpp);
	points_init(&prop);
	vector_init(&inpv);
	vector_init(&prov);
	do { /* dummy loop to enable break */
		flag = 0; stat = 0; psrc = 0x0; pdst = 0x0;
		makevec = points_make_vector_cc;
		fromvec = points_from_vector_cc;
		/* check args */
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
				ptmp = &argv[loop][2];
				if (!strncmp(ptmp,"save",5)) {
					if (++loop<argc) pdst = argv[loop];
					else {
						fprintf(stderr,"** No value for '%s'!\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"cd",3)) {
					printf("@@ Using centroid-distance!\n");
					makevec = points_make_vector_cd;
					fromvec = points_from_vector_cd;
				}
				else if (!strncmp(ptmp,"dp",3)) {
					printf("@@ Using distance/phase!\n");
					makevec = points_make_vector_dp;
					fromvec = points_from_vector_dp;
				}
				else if (!strncmp(ptmp,"sample",7))
					flag |= FLAG_SAMPLE_PNT;
				else if (!strncmp(ptmp,"bin",4))
					flag |= FLAG_DOSAVE_BIN;
				else {
					fprintf(stderr,"** Invalid option '%s'!\n",argv[loop]);
					stat |= STAT_ERROR_ARGS;
				}
			}
			else {
				if (!psrc) psrc = argv[loop];
				else {
					fprintf(stderr,"** Invalid argument '%s'!\n",argv[loop]);
					stat |= STAT_ERROR_ARGS;
				}
			}
		}
		if (!psrc) {
			fprintf(stderr,"** No input file given!\n");
			stat |= STAT_ERROR_ARGS;
		}
		if (stat) break;
		/* assume points */
		printf("-- Loading... ");
		if ((test=points_load(&inpp,psrc))) {
			loop = test;
			if ((test=vector_load(&inpv,psrc))) {
				stat |= STAT_ERROR_LOAD;
				printf("failed. [P:%d|V:%d]\n",loop,test);
				break;
			}
			flag |= FLAG_VECTORDATA;
			printf("done. [V:%d/%d]\n",inpv.fill,inpv.size);
		}
		else {
			printf("done. [P:%d/%d](%08x)\n",
				inpp.fill,inpp.size,inpp.flag);
			printf("   Xmin:%d, Xmax:%d\n",inpp.xmin,inpp.xmax);
			printf("   Ymin:%d, Ymax:%d\n",inpp.ymin,inpp.ymax);
		}
		/* convert data */
		if (flag&FLAG_VECTORDATA) {
			printf("-- Convert to points... ");
			fromvec(&prop,&inpv);
			printf("done. [%d/%d]\n",prop.fill,prop.size);
			points_prep4image(&prop,1); /* border:1px */
			if (pdst) {
				printf("-- Saving points... ");
				if ((test=points_save(&prop,pdst))) {
					stat |= STAT_ERROR_SAVE;
					printf("failed. [%d]\n",test);
					break;
				}
				printf("done.\n");
			}
			else {
				printf("-- Showing points on stderr\n");
				points_ostr(&prop,stderr);
			}
		}
		else {
			if (pdst) {
				usep = &inpp;
				if (flag&FLAG_SAMPLE_PNT) {
					/* if NOT power of two, sample! */
					test = inpp.fill;
					if (test&(test-1)) {
						points_sample(&inpp,&prop,-1);
						printf("@@ Points sampled! (%d/%d)\n",
							prop.fill,inpp.fill);
						usep = &prop;
					}
				}
				printf("-- Convert to vector... ");
				points_moment(usep);
				makevec(usep,&prov);
				printf("done. [%d/%d]\n",prov.fill,prov.size);
				printf("-- Saving vector... ");
				if (flag&FLAG_DOSAVE_BIN) {
					printf("[bin] ");
					test = vector_save_bin(&prov,pdst);
				}
				else test = vector_save(&prov,pdst);
				if (test) {
					stat |= STAT_ERROR_SAVE;
					printf("failed. [%d]\n",test);
				}
				else printf("done.\n");
			}
			else {
				printf("-- Convert to vector... ");
				points_moment(&inpp);
				makevec(&inpp,&prov);
				printf("done. [%d/%d]\n",prov.fill,prov.size);
				printf("-- Recovering points... ");
				fromvec(&prop,&prov);
				printf("done. [%d/%d]\n",prop.fill,prop.size);
				points_translate(&prop,inpp.orig.x,inpp.orig.y);
				printf("-- Showing points on stderr\n");
				points_ostr(&prop,stderr);
			}
		}
	} while (0);
	/* cleanup stuff */
	vector_free(&prov);
	vector_free(&inpv);
	points_free(&prop);
	points_free(&inpp);
	return stat;
}
/*----------------------------------------------------------------------------*/
