/*----------------------------------------------------------------------------*/
/**
 * shape.c
 * - tool to make/conv shape descriptors
 *   = using shape.h (can use fft)
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
#include "points_file.h"
#include "vector_file.h"
#include "cstr.h"
#include "keys.h"
#include "shape.h"
/*----------------------------------------------------------------------------*/
#define FLAG_TASK_MASK 0x7000
#define FLAG_TASK_MAKE 0x1000
#define FLAG_TASK_CONV 0x2000
#define FLAG_TASK_OSTR 0x4000
#define FLAG_DO_VERBOSE 0x0001
#define FLAG_CALC_TIME 0x0002
#define FLAG_SKIP_SAVE 0x0004
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR (~(~0U>>1))
#define STAT_ERROR_ARGS (STAT_ERROR|0x00000001)
#define STAT_ERROR_LOAD (STAT_ERROR|0x00000002)
#define STAT_ERROR_SAVE (STAT_ERROR|0x00000004)
#define STAT_ERROR_WORK (STAT_ERROR|0x00000008)
/*----------------------------------------------------------------------------*/
void file_basename(my1cstr_t* pstr, char* name, char* extn) {
	char *base, *buff;
	int loop;
	buff = strdup(name);
	base = basename(buff);
	cstr_assign(pstr,base);
	free((void*)buff);
	loop = pstr->fill-1;
	while (loop>0) {
		if (pstr->buff[loop]=='.') {
			pstr->buff[loop] = 0x0;
			pstr->fill = loop;
			break;
		}
		loop--;
	}
	if (extn) cstr_append(pstr,extn);
}
/*----------------------------------------------------------------------------*/
int wait_key(void) {
	my1key_t read;
	printf("-- Press any key... ");
	read = get_key();
	printf("\n\n");
	return (read==KEY_ESCAPE)?1:0;
}
/*----------------------------------------------------------------------------*/
void vector_text(my1vector_t* vec, char* str) {
	int loop, size = vec->fill;
	printf("%s{%p}[%d/%d]:\n",str,vec,size,vec->size);
	for (loop=0;loop<size;loop++) {
		printf("REAL[%d]=%10lf\t",loop,vec->data[loop].real);
		printf("IMAG[%d]=%10lf\t",loop,vec->data[loop].imag);
		printf("ABS[%d]=%10lf\t",loop,complex_magnitude(&vec->data[loop]));
		printf("ARG[%d]=%10lf\n",loop,complex_phase(&vec->data[loop]));
	}
}
/*----------------------------------------------------------------------------*/
#define OFFSET_ORIGIN 11
my1point_t basic_square[8] =  {
	{ -10 ,  10 },
	{   0 ,  10 },
	{  10 ,  10 },
	{  10 ,   0 },
	{  10 , -10 },
	{   0 , -10 },
	{ -10 , -10 },
	{ -10 ,   0 }
};
my1point_t basic_circle[8] =  {
	{  -7 ,   7 },
	{   0 ,  10 },
	{   7 ,   7 },
	{  10 ,   0 },
	{   7 ,  -7 },
	{   0 , -10 },
	{  -7 ,  -7 },
	{ -10 ,   0 }
};
/*----------------------------------------------------------------------------*/
void points_basic8(my1points_t* pnts, my1point_t* list) {
	int loop;
	points_make(pnts,8);
	points_zero(pnts);
	for (loop=0;loop<8;loop++,list++)
		points_append(pnts,list->x+OFFSET_ORIGIN,list->y+OFFSET_ORIGIN);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, test, size;
	unsigned int flag, stat;
	double tval;
	char *ptmp, *name;
	struct timeval init, stop, diff;
	my1points_t con;
	my1vector_t vec;
	my1cstr_t str1;
	my1shape_t pro;
	points_init(&con);
	vector_init(&vec);
	cstr_init(&str1);
	shape_init(&pro,&vec);
	{
		/* build shape */
		my1points_t chk1;
		my1shape_t pro1;
		points_init(&chk1);
		shape_init(&pro1,0x0);
		printf("-- Using basic square\n\n");
		points_basic8(&chk1,basic_square);
		points_ostr(&chk1,stdout);
		points_moment(&chk1);
		shape_use_cd(&pro1);
		shape_from_points(&pro1,&chk1);
		vector_text(&pro1.buff,"PointCD");
		vector_text(&pro1.keep,"ShapeCD");
		printf("\n");
		wait_key();
		shape_use_cc(&pro1);
		shape_from_points(&pro1,&chk1);
		vector_text(&pro1.buff,"PointCC");
		vector_text(&pro1.keep,"ShapeCC");
		printf("\n");
		wait_key();
		shape_use_dp(&pro1);
		shape_from_points(&pro1,&chk1);
		vector_text(&pro1.buff,"PointDP");
		vector_text(&pro1.keep,"ShapeDP");
		printf("\n");
		wait_key();
		shape_free(&pro1);
		points_free(&chk1);
	}
	do {
		flag = 0; stat = 0; size = 0;
		if (argc<2) {
			fprintf(stderr,"** No task!\n");
			stat |= STAT_ERROR_ARGS;
			break;
		}
		if (!strncmp(argv[1],"make",5))
			flag |= FLAG_TASK_MAKE;
		else if (!strncmp(argv[1],"conv",5))
			flag |= FLAG_TASK_CONV;
		else if (!strncmp(argv[1],"ostr",5))
			flag |= FLAG_TASK_OSTR;
		else {
			fprintf(stderr,"** Invalid task '%s'!\n",argv[1]);
			stat |= STAT_ERROR_ARGS;
			break;
		}
		if (argc<3) {
			fprintf(stderr,"** No input file!\n");
			stat |= STAT_ERROR_ARGS;
			break;
		}
		name = 0x0;
		for (loop=3;loop<argc;loop++) {
			if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
				ptmp = &argv[loop][2];
				if (!strncmp(ptmp,"cc",3))
					pro.flag |= SHAPE_FLAG_POINTSCC;
				else if (!strncmp(ptmp,"dp",3))
					pro.flag |= SHAPE_FLAG_POINTSDP;
				else if (!strncmp(ptmp,"fft",4))
					pro.flag |= SHAPE_FLAG_PICK_FFT;
				else if (!strncmp(ptmp,"dftx",5))
					pro.flag |= SHAPE_FLAG_BAND_DFT;
				else if (!strncmp(ptmp,"sample",7))
					pro.flag |= SHAPE_FLAG_DOSAMPLE;
				else if (!strncmp(ptmp,"nosave",7))
					flag |= FLAG_SKIP_SAVE;
				else if (!strncmp(ptmp,"time",5))
					flag |= FLAG_CALC_TIME;
				else if (!strncmp(ptmp,"verbose",8))
					flag |= FLAG_DO_VERBOSE;
				else if (!strncmp(ptmp,"name",5)) {
					if (++loop<argc) name = argv[loop];
					else {
						fprintf(stderr,"** No value for '%s'\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"cut",4)) {
					if (++loop<argc) {
						test = atoi(argv[loop]);
						if (test>0) size = test;
						else {
							fprintf(stderr,"** Invalid value for '%s' (%s)\n",
								argv[loop-1],argv[loop]);
							stat |= STAT_ERROR_ARGS;
						}
					}
					else {
						fprintf(stderr,"** No value for '%s'\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else if (!strncmp(ptmp,"size",5)) {
					if (++loop<argc) {
						test = atoi(argv[loop]);
						if (test>0) pro.size = test;
						else {
							fprintf(stderr,"** Invalid value for '%s' (%s)\n",
								argv[loop-1],argv[loop]);
							stat |= STAT_ERROR_ARGS;
						}
					}
					else {
						fprintf(stderr,"** No value for '%s'\n",argv[loop-1]);
						stat |= STAT_ERROR_ARGS;
					}
				}
				else {
					fprintf(stderr,"** Invalid option '%s'\n",argv[loop]);
					stat |= STAT_ERROR_ARGS;
				}
			}
			else {
				fprintf(stderr,"** Invalid argument '%s'\n",argv[loop]);
				stat |= STAT_ERROR_ARGS;
			}
		}
		if (stat) break;
		if (flag&FLAG_DO_VERBOSE) {
			/* show settings */
			printf("@@ Settings:");
			if (pro.flag&SHAPE_FLAG_POINTSCC) printf(" [CC]");
			if (pro.flag&SHAPE_FLAG_POINTSDP) printf(" [DP]");
			if (pro.flag&SHAPE_FLAG_PICK_FFT) printf(" [FFT]");
			if (pro.flag&SHAPE_FLAG_BAND_DFT) printf(" [band-DFT]");
			if (pro.flag&SHAPE_FLAG_DOSAMPLE) printf(" [SAMPLING]");
			if (flag&FLAG_SKIP_SAVE) printf(" [NOT-SAVING]");
			if (flag&FLAG_CALC_TIME) printf(" [CALC TIME]");
			if (size>0) printf(" [CutTrim:%d]",size);
			if (pro.size>0) printf(" [BandTrim:%d]",pro.size);
			printf("\n");
		}
		if (flag&FLAG_TASK_MAKE) {
			if (points_load(&con,argv[2])) {
				fprintf(stderr,"** Cannot load '%s'\n",argv[2]);
				stat |= STAT_ERROR_LOAD;
				break;
			}
			if (flag&FLAG_DO_VERBOSE)
				printf("-- [MAKE] Points loaded ('%s':%d)",argv[2],con.fill);
			if (con.fill<=0) {
				fprintf(stderr,"** No points loaded from '%s'\n",argv[2]);
				stat |= STAT_ERROR_LOAD;
				break;
			}
			printf("-- Making shape... ");
			gettimeofday(&init,0x0);
			shape_from_points(&pro,&con);
			gettimeofday(&stop,0x0);
			timersub(&stop,&init,&diff);
			tval = (double)diff.tv_sec+(double)diff.tv_usec/1000000.0;
			printf("done. (%d|%d)=>[%es]\n",pro.buff.fill,vec.fill,tval);
			if (size>0) {
				if (flag&FLAG_DO_VERBOSE)
					printf("-- Truncating (%d->%d)\n",vec.fill,size);
				vec.fill = size;
			}
			if (!(flag&FLAG_SKIP_SAVE)) {
				/* save to file */
				if (!name) {
					file_basename(&str1,argv[2],".bin");
					name = str1.buff;
				}
				printf("-- Saving shape to '%s'... ",name);
				if (vector_save_bin(&vec,name)) {
					printf("failed!\n");
					stat |= STAT_ERROR_SAVE;
				}
				else printf("done.\n");
			}
			if (flag&FLAG_CALC_TIME) {
				printf("-- Measuring processing time... ");
				fflush(stdout);
				gettimeofday(&init,0x0);
				for (loop=0;loop<1024;loop++)
					shape_from_vector(&pro,&pro.buff);
				gettimeofday(&stop,0x0);
				timersub(&stop,&init,&diff);
				tval = (double)diff.tv_sec+(double)diff.tv_usec/1000000.0;
				printf("done. <%ld.%06lds>{%.6lfs|%.6lfs}\n",
					(long int)(diff.tv_sec),(long int)(diff.tv_usec),
					tval,tval/1024.0);
			}
		}
		else /*if (flag&FLAG_TASK_CONV)*/ {
			if (vector_load_bin(&vec,argv[2])) {
				fprintf(stderr,"** Cannot load '%s'\n",argv[2]);
				stat |= STAT_ERROR_LOAD;
				break;
			}
			if (flag&FLAG_DO_VERBOSE)
				printf("-- [CONV] Vector loaded ('%s':%d)",argv[2],vec.fill);
			if (vec.fill<=0) {
				fprintf(stderr,"** No vector loaded from '%s'\n",argv[2]);
				stat |= STAT_ERROR_LOAD;
				break;
			}
			if (flag&FLAG_TASK_OSTR)
				vector_ostr(&vec,stderr);
			else {
				printf("-- Converting shape... ");
				test = shape_make_points(&pro,&con);
				printf("done. (%d:%d)\n",test,con.fill);
				/* save to file */
				if (!name) {
					file_basename(&str1,argv[2],".txt");
					name = str1.buff;
				}
				printf("-- Saving points to '%s'... ",name);
				if (points_save(&con,name)) {
					printf("failed!\n");
					stat |= STAT_ERROR_SAVE;
				}
				else printf("done.\n");
			}
		}
	} while (0);
	shape_free(&pro);
	cstr_free(&str1);
	vector_free(&vec);
	points_free(&con);
	return stat;
}
/*----------------------------------------------------------------------------*/
