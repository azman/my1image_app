/*----------------------------------------------------------------------------*/
/*
	if_keepshow.h
	- image_filter to insert image copied by keepthis
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_KEEPSHOW_H__
#define __MY1IF_KEEPSHOW_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_KEEPSHOW "if_keepshow"
/*----------------------------------------------------------------------------*/
typedef struct _my1if_keepshow_t {
	my1image_t keep;
} my1if_keepshow_t;
/*----------------------------------------------------------------------------*/
void filter_keepshow_keep(my1ipass_t* pass, my1image_t* from) {
	my1if_keepshow_t *curr;
	curr = (my1if_keepshow_t*) pass->data;
	image_copy(&curr->keep,from);
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_keepshow(my1image_t* img, my1image_t* res,
		my1ipass_t* pass) {
	my1if_keepshow_t *curr;
	curr = (my1if_keepshow_t*) pass->data;
	if (img->size==curr->keep.size&&
			img->rows==curr->keep.rows&&img->cols==curr->keep.cols) {
		/* should be same size? */
		image_copy(res,&curr->keep);
	}
	else res = img;
	return res;
}
/*----------------------------------------------------------------------------*/
void filter_keepshow_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_keepshow_t *curr;
	filter->data = malloc(sizeof(my1if_keepshow_t));
	curr = (my1if_keepshow_t*) filter->data;
	image_init(&curr->keep);
	/* ignore pclone! */
}
/*----------------------------------------------------------------------------*/
void filter_keepshow_free(my1ipass_t* filter) {
	my1if_keepshow_t *curr;
	if (filter->data) {
		curr = (my1if_keepshow_t*) filter->data;
		image_free(&curr->keep);
		free(filter->data);
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_KEEPSHOW_H__ */
/*----------------------------------------------------------------------------*/
