/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_FILE_H__
#define __MY1SHAPED_FILE_H__
/*----------------------------------------------------------------------------*/
/**
 * shaped_file.h
 * - extension for shaped.h
 * - provides function to load a single descriptor from file
 *   = will use filename as name of the shape
 * - no save function required
**/
/*----------------------------------------------------------------------------*/
#include "shaped.h"
#include "vector_file.h"
/*----------------------------------------------------------------------------*/
/* strdup */
#include <string.h>
/* basename */
#include <libgen.h>
/* free */
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
int shaped_load(my1shaped_t* cont, char* file, char* name) {
	char *base, *buff;
	if (vector_load(&cont->desc,file)&&
			vector_load_bin(&cont->desc,file))
		return -1;
	base = 0x0;
	if (!name) {
		buff = strdup(file);
		base = strdup(basename(buff));
		free((void*)buff);
		name = base;
	}
	cstr_assign(&cont->name,name);
	if (base) free((void*)base);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_FILE_H__ */
/*----------------------------------------------------------------------------*/
