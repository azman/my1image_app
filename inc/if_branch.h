/*----------------------------------------------------------------------------*/
/*
	if_branch.h
	- conditional image_filter
	  = used to 'control' the next filter
	  = will disable next filter if cond function returns non-zero
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1IF_BRANCH_H__
#define __MY1IF_BRANCH_H__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
#define IFNAME_BRANCH "if_branch"
/*----------------------------------------------------------------------------*/
#define IFBRANCH_FLAG_INVLOGIC 0x01
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
/* return non-zero to disable next filter */
typedef int (*pcond_t)(my1image_t*, my1ipass_t*);
/*----------------------------------------------------------------------------*/
typedef struct _my1if_branch_t {
	unsigned int flag, rsv0;
	pcond_t cond;
} my1if_branch_t;
/*----------------------------------------------------------------------------*/
void filter_branch_doinvert(my1ipass_t* pass, int invert) {
	my1if_branch_t* pick = (my1if_branch_t*) pass->data;
	if (invert) pick->flag |= IFBRANCH_FLAG_INVLOGIC;
	else pick->flag &= ~IFBRANCH_FLAG_INVLOGIC;
}
/*----------------------------------------------------------------------------*/
void filter_branch_condition(my1ipass_t* pass, pcond_t cond) {
	my1if_branch_t* pick = (my1if_branch_t*) pass->data;
	pick->cond = cond;
}
/*----------------------------------------------------------------------------*/
my1image_t* filter_branch(my1image_t* img, my1image_t* res, my1ipass_t* pass) {
	my1if_branch_t* pick = (my1if_branch_t*) pass->data;
	int exec = 1;
	if (pass->next) {
		if (pick->cond&&pick->cond(img,pass))
			exec = 0;
		if (pick->flag&IFBRANCH_FLAG_INVLOGIC)
			exec = !exec;
		if (exec) filter_doexec(pass->next);
		else filter_noexec(pass->next);
	}
	return img;
}
/*----------------------------------------------------------------------------*/
void filter_branch_init(my1ipass_t* filter, my1ipass_t* pclone) {
	my1if_branch_t *curr, *pchk;
	filter->data = malloc(sizeof(my1if_branch_t));
	curr = (my1if_branch_t*) filter->data;
	curr->flag = 0;
	if (!pclone) curr->cond = 0x0;
	else {
		pchk = (my1if_branch_t*) pclone->data;
		curr->cond = pchk->cond;
	}
}
/*----------------------------------------------------------------------------*/
void filter_branch_free(my1ipass_t* filter) {
	if (filter->data) free(filter->data);
}
/*----------------------------------------------------------------------------*/
void filter_branch_conf(my1ipass_t* filter, char *pkey, char *pval) {
	my1if_branch_t* data;
	data = (my1if_branch_t*) filter->data;
	if (!strncmp(pkey,"inv",4)) {
		if ((pval[0]==0x31||pval[0]==0x30)&&pval[1]==0x0) {
			if (pval[0]==0x31) data->flag |= IFBRANCH_FLAG_INVLOGIC;
			else data->flag &= ~IFBRANCH_FLAG_INVLOGIC;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IF_BRANCH_H__ */
/*----------------------------------------------------------------------------*/
