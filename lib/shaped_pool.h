/*----------------------------------------------------------------------------*/
#ifndef __MY1SHAPED_POOL_H__
#define __MY1SHAPED_POOL_H__
/*----------------------------------------------------------------------------*/
#include "shaped_dist.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1shaped_pool_t { /* shaped cluster */
	my1shaped_t davg; /* average descriptor for this pool */
	my1list_t pool; /* list of my1shaped_dist_t* (pure pointers) */
} my1shaped_pool_t;
/*----------------------------------------------------------------------------*/
void shaped_pool_init(my1shaped_pool_t* that) {
	shaped_init(&that->davg);
	list_init(&that->pool,0x0); /* by default items are moved around */
}
/*----------------------------------------------------------------------------*/
void shaped_pool_free(my1shaped_pool_t* that) {
	that->pool.done = list_free_item; /* free item if still available */
	list_free(&that->pool);
	shaped_free(&that->davg);
}
/*----------------------------------------------------------------------------*/
void shaped_pool_sort(my1shaped_pool_t* that) {
	/* use simple bubble sort for now */
	my1item_t *item, *work, *last;
	my1shaped_t *curr, *next;
	void *swap; /* just swap data */
	if (that->pool.size<2) return; /* nothing to sort */
	item = that->pool.init; last = that->pool.last;
	while (item->next) { /* skip last item */
		work = that->pool.init;
		while (work!=last) {
			curr = (my1shaped_t*) work->data;
			next = (my1shaped_t*) work->next->data;
			if (strcmp(curr->name.buff,next->name.buff)>0) {
				swap = work->data;
				work->data = work->next->data;
				work->next->data = swap;
			}
			work = work->next;
		}
		item = item->next;
		last = last->prev;
	}
}
/*----------------------------------------------------------------------------*/
my1shaped_t* shaped_pool_average(my1shaped_pool_t* that) {
	my1item_t* item;
	my1vector_t *chk1, *chk2;
	chk1 = SVEC(&that->davg);
	item = that->pool.init;
	if (!item) return 0x0;
	/* copy first item */
	chk2 = SVEC(item->data);
	vector_copy(chk1,chk2);
	item = item->next;
	while (item) {
		/* accumulate the rest */
		chk2 = SVEC(item->data);
		vector_summation(chk1,chk2);
		item = item->next;
	}
	vector_scale(chk1,(double)1/that->pool.size);
	return &that->davg;
}
/*----------------------------------------------------------------------------*/
my1item_t* make_shaped_pool_item(void) {
	my1shaped_pool_t* temp;
	my1item_t* item;
	item = make_item();
	if (item) {
		temp = (my1shaped_pool_t*)malloc(sizeof(my1shaped_pool_t));
		if (temp) {
			shaped_pool_init(temp);
			item->data = (void*)temp;
		}
		else {
			free((void*)item);
			item = 0x0;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
void free_shaped_pool_item(void* that) {
	my1item_t* item = (my1item_t*)that;
	shaped_pool_free((my1shaped_pool_t*)item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHAPED_POOL_H__ */
/*----------------------------------------------------------------------------*/
