/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_FILE_H__
#define __MY1VECTOR_FILE_H__
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "vector.h"
/*----------------------------------------------------------------------------*/
#define VECTOR_FILE_HEADER "MY1VECTOR:"
#define VECTOR_FILE_HEADER_SIZE 10
#define VECTOR_FILE_HEADER_BIN VECTOR_FILE_HEADER"\0\0"
#define VECTOR_FILE_HEADER_BIN_SIZE 12
/*----------------------------------------------------------------------------*/
#define VECTOR_FILE_OK 0
#define VECTOR_FILE_ERROR_OPEN 1
#define VECTOR_FILE_ERROR_FMT0 2
#define VECTOR_FILE_ERROR_FMT1 3
#define VECTOR_FILE_ERROR_FMT2 4
#define VECTOR_FILE_ERROR_FMT3 5
#define VECTOR_FILE_ERROR_MAKE 6
#define VECTOR_FILE_ERROR_READ 7
#define VECTOR_FILE_ERROR_FMT4 8
/*----------------------------------------------------------------------------*/
#define BUFF_SIZE 80
/*----------------------------------------------------------------------------*/
static char vector_ostr_res[16] = "%.10lf";
/*----------------------------------------------------------------------------*/
void vector_ostr_resolution(int res) {
	if (res>0) snprintf(vector_ostr_res,16,"%%.%dlf",res);
}
/*----------------------------------------------------------------------------*/
int vector_ostr(my1vector_t* data,FILE* ostr) {
	int loop,stop;
	fprintf(ostr,"%s%d\n",VECTOR_FILE_HEADER,data->fill);
	stop = 0;
	while (vector_ostr_res[stop]) stop++;
	vector_ostr_res[stop+1] = 0x0;
	for (loop=0;loop<data->fill;loop++) {
		vector_ostr_res[stop] = ',';
		fprintf(ostr,vector_ostr_res,data->data[loop].real);
		fprintf(ostr,vector_ostr_res,data->data[loop].imag);
		vector_ostr_res[stop] = '\n';
		fprintf(ostr,vector_ostr_res,complex_magnitude(&data->data[loop]));
	}
	vector_ostr_res[stop] = 0x0;
	return loop;
}
/*----------------------------------------------------------------------------*/
int vector_save(my1vector_t* data,char* name) {
	FILE *save = fopen(name,"wt");
	if (!save) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return VECTOR_FILE_ERROR_OPEN;
	}
	vector_ostr(data,save);
	fclose(save);
	return VECTOR_FILE_OK;
}
/*----------------------------------------------------------------------------*/
int vector_load(my1vector_t* data,char* name) {
	int test, size, fill, done;
	double real, imag;
	char buff[BUFF_SIZE];
	FILE* read = fopen(name,"rt");
	if (!read) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return VECTOR_FILE_ERROR_OPEN;
	}
	do {
		done = VECTOR_FILE_OK;
		if (!fgets(buff,BUFF_SIZE,read)) {
			done = VECTOR_FILE_ERROR_FMT0;
			break;
		}
		test = strlen(buff);
		/* assume size is no more than 4 digit */
		if (buff[test-1]!='\n') {
			done = VECTOR_FILE_ERROR_FMT1;
			break;
		}
		buff[test-1] = 0x0;
		if (strncmp(buff,VECTOR_FILE_HEADER,VECTOR_FILE_HEADER_SIZE)) {
			done = VECTOR_FILE_ERROR_FMT2;
			break;
		}
		size = atoi(&buff[10]);
		if (size<=0) {
			done = VECTOR_FILE_ERROR_FMT2;
			break;
		}
		vector_make(data,size);
		if (data->size!=size) {
			done = VECTOR_FILE_ERROR_MAKE;
			break;
		}
		data->fill = 0;
		fill = 0;
		while (fill<size) {
			fill++;
			if (!fgets(buff,BUFF_SIZE,read)) {
				fprintf(stderr,"** Cannot read data %d/%d!\n",fill,size);
				done = VECTOR_FILE_ERROR_READ;
				break;
			}
			if (sscanf(buff,"%lf,%lf",&real,&imag)!=2) {
				fprintf(stderr,"** Cannot scan data (%d/%d)!\n",fill,size);
				done = VECTOR_FILE_ERROR_FMT4;
				break;
			}
			vector_append(data,real,imag);
		}
	} while (0);
	fclose(read);
	return done;
}
/*----------------------------------------------------------------------------*/
int vector_save_bin(my1vector_t* data,char* name) {
	FILE *save;
	int loop;
	save = fopen(name,"wb");
	if (!save) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return VECTOR_FILE_ERROR_OPEN;
	}
	/* use same header, zero padded */
	fwrite((void*)VECTOR_FILE_HEADER_BIN,VECTOR_FILE_HEADER_BIN_SIZE,1,save);
	/* write number of complex data */
	fwrite((const void*)&data->fill,sizeof(int),1,save);
	for (loop=0;loop<data->fill;loop++)
		fwrite((const void*)&data->data[loop],sizeof(my1complex_t),1,save);
	fclose(save);
	return VECTOR_FILE_OK;
}
/*----------------------------------------------------------------------------*/
int vector_load_bin(my1vector_t* data,char* name) {
	int test, done;
	long save, tell;
	char buff[BUFF_SIZE];
	my1complex_t* curr;
	FILE* read = fopen(name,"rb");
	if (!read) {
		fprintf(stderr,"** Cannot open file '%s'!\n",name);
		return VECTOR_FILE_ERROR_OPEN;
	}
	do {
		done = VECTOR_FILE_OK;
		if (fread((void*)buff,VECTOR_FILE_HEADER_BIN_SIZE,1,read)!=1) {
			done = VECTOR_FILE_ERROR_FMT0;
			break;
		}
		if (strncmp(buff,VECTOR_FILE_HEADER_BIN,VECTOR_FILE_HEADER_BIN_SIZE)) {
			done = VECTOR_FILE_ERROR_FMT2;
			break;
		}
		if (fread((void*)&test,sizeof(int),1,read)!=1) {
			done = VECTOR_FILE_ERROR_FMT1;
			break;
		}
		if (test<=0) {
			done = VECTOR_FILE_ERROR_FMT2;
			break;
		}
		vector_make(data,test);
		if (data->size!=test) {
			done = VECTOR_FILE_ERROR_MAKE;
			break;
		}
		save = ftell(read); /* save current position */
		tell = save + (test*sizeof(my1complex_t)); /* expected file size */
		fseek(read,0,SEEK_END);
		if (ftell(read)!=tell) { /* check file size */
			done = VECTOR_FILE_ERROR_FMT4;
			break;
		}
		fseek(read,save,SEEK_SET); /* back to saved position */
		curr = data->data;
		data->fill = 0;
		while (data->fill<data->size) {
			test = fread((void*)curr,sizeof(my1complex_t),1,read);
			if (test!=1) {
				fprintf(stderr,"** Cannot read data %d/%d!\n",
					data->fill,data->size);
				done = VECTOR_FILE_ERROR_READ;
				break;
			}
			data->fill++;
			curr++;
		}
	} while (0);
	fclose(read);
	return done;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_FILE_H__ */
/*----------------------------------------------------------------------------*/
