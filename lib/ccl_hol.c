/*----------------------------------------------------------------------------*/
#include "ccl_hol.h"
#include "ivec_file.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, loop, test, rows, cols;
	my1ivec_t load, buff, *ivec;
	my1ccl_t _ccl;
	ivec_init(&load);
	ivec_init(&buff);
	do {
		stat = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		test = ivec_load(&load,argv[1]);
		if (!test) {
			printf("** Cannot load '%s'!\n",argv[1]);
			stat = -2;
			break;
		}
		ivec = &load;
		printf("-- Load:\n");
		ivec_show(ivec);
		ivec_copy(&buff,&load);
		ivec = &buff;
		rows = ivec->rows;
		cols = ivec->cols;
		ccl_init(&_ccl,ivec->data,rows,cols);
		printf("-- Processing... ");
		exec_HOL(&_ccl);
		printf("done.\n-- List (Step:%d,Size:%d):\n",_ccl.step,_ccl.size);
		for (loop=2,test=0;loop<LABEL_SIZE;loop++) {
			if (_ccl.stat[loop].mark) {
				printf("-- [Loop:%03d] => Mark:%d (Size:%d)\n",loop,
					_ccl.stat[loop].mark,_ccl.stat[loop].size);
				test++; if (test>=_ccl.size) break;
			}
		}
		printf("-- Data:\n");
		ivec_show(ivec);
	} while (0);
	ivec_free(&buff);
	ivec_free(&load);
	return stat;
}
/*----------------------------------------------------------------------------*/
