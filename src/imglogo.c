/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
/** requires -lm for sizing process */
/*----------------------------------------------------------------------------*/
#include "my1image_01full.c"
/*----------------------------------------------------------------------------*/
/* target 10% of image size */
#define LOGO_SIZE_PCT 0.1
/* 16x16 min size */
#define LOGO_SIZE_MIN 16
#define LOGO_BORDER_PAD 8
#define LOGO_POS_BR 0x01
#define LOGO_POS_BL 0x02
#define LOGO_POS_TR 0x04
#define LOGO_POS_TL 0x08
#define LOGO_POS_MASK 0x0F
/*----------------------------------------------------------------------------*/
#define STAT_ERROR_ARGS -1
#define STAT_ERROR_LOAD -2
#define STAT_ERROR_PICK -3
#define STAT_ERROR_SAVE -4
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	unsigned int stat, flag;
	int test, loop;
	int chkw, chkh;
	char *save, *pick, *load;
	my1image_t buff, logo, lbuf;
	my1image_area_t area;
	if (argc<2) {
		fprintf(stderr,"** Need at least 1 parameter!\n");
		return STAT_ERROR_ARGS;
	}
	load = argv[1]; pick = 0x0; save = 0x0; stat = 0; flag = 0;
	for (loop=2;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--logo",7)) {
			if (++loop<argc) pick = argv[loop];
			else {
				fprintf(stderr,"** No param for '%s'!\n",argv[loop-1]);
				stat++;
			}
		}
		else if (!strncmp(argv[loop],"--save",7)) {
			if (++loop<argc) save = argv[loop];
			else {
				fprintf(stderr,"** No param for '%s'!\n",argv[loop-1]);
				stat++;
			}
		}
		else if (!strncmp(argv[loop],"--bottom-right",15)) {
			flag &= LOGO_POS_MASK;
			flag |= LOGO_POS_BR;
		}
		else if (!strncmp(argv[loop],"--bottom-left",14)) {
			flag &= LOGO_POS_MASK;
			flag |= LOGO_POS_BL;
		}
		else if (!strncmp(argv[loop],"--top-right",12)) {
			flag &= LOGO_POS_MASK;
			flag |= LOGO_POS_TR;
		}
		else if (!strncmp(argv[loop],"--top-left",11)) {
			flag &= LOGO_POS_MASK;
			flag |= LOGO_POS_TL;
		}
		else {
			fprintf(stderr,"** Unknown param '%s'!\n",argv[loop]);
			stat++;
		}
	}
	if (!pick) {
		fprintf(stderr,"** Logo not provided! (use '--logo' option)\n");
		return STAT_ERROR_ARGS;
	}
	if (stat) return STAT_ERROR_ARGS;
	if (!save) save = load; /* override mode */
	if (!(flag&LOGO_POS_MASK)) flag |= LOGO_POS_BR;
	/* ready to go */
	image_file_init();
	image_init(&buff);
	image_init(&logo);
	image_init(&lbuf);
	do {
		fprintf(stderr,"-- Loading logo '%s'... ",pick);
		if ((test=image_load(&logo,pick))) {
			fprintf(stderr,"**error** [0x%08x]\n",(unsigned)test);
			stat = STAT_ERROR_PICK;
			break;
		}
		fprintf(stderr,"done.\n");
		fprintf(stderr,"-- File: %s\n",pick);
		fprintf(stderr,"-- Size: %d x %d (Size:%d) {Mask:%08X}\n\n",
			logo.cols,logo.rows,logo.size,logo.mask);
		fprintf(stderr,"-- Loading image '%s'... ",load);
		if ((test=image_load(&buff,load))) {
			fprintf(stderr,"**error** [0x%08x]\n",(unsigned)test);
			stat = STAT_ERROR_LOAD;
			break;
		}
		fprintf(stderr,"done.\n");
		fprintf(stderr,"-- File: %s\n",load);
		fprintf(stderr,"-- Size: %d x %d (Size:%d) {Mask:%08X}\n\n",
			buff.cols,buff.rows,buff.size,buff.mask);
		chkw = buff.cols*LOGO_SIZE_PCT;
		chkh = buff.rows*LOGO_SIZE_PCT;
		if (chkw>chkh) chkw = chkh;
		if (chkw<LOGO_SIZE_MIN) chkw = LOGO_SIZE_MIN;
		/* resize logo */
		image_size_this(&logo,&lbuf,chkw,chkw);
		/* positioning */
		chkh = chkw+LOGO_BORDER_PAD;
		switch (flag&LOGO_POS_MASK) {
			case LOGO_POS_BL:
				area.yset = buff.rows-chkh;
				area.xset = LOGO_BORDER_PAD;
				break;
			case LOGO_POS_TR:
				area.yset = LOGO_BORDER_PAD;
				area.xset = buff.cols-chkh;
				break;
			case LOGO_POS_TL:
				area.yset = LOGO_BORDER_PAD;
				area.xset = LOGO_BORDER_PAD;
				break;
			case LOGO_POS_BR: default:
				area.yset = buff.rows-chkh;
				area.xset = buff.cols-chkh;
				break;
		}
		area.hval = chkw;
		area.wval = chkw;
		image_set_area(&buff,&lbuf,&area);
		fprintf(stderr,"-- Saving image to %s... ",save);
		if ((test=image_save(&buff,save))) {
			fprintf(stderr,"**error** [0x%08x]\n",(unsigned)test);
			stat = STAT_ERROR_SAVE;
		}
		else fprintf(stderr,"done.\n");
	} while (0);
	image_free(&lbuf);
	image_free(&logo);
	image_free(&buff);
	image_file_free();
	return stat;
}
/*----------------------------------------------------------------------------*/
